import React from 'react'
import {
  View,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  ActivityIndicator
} from 'react-native'
import styles from './style'
import {Text,CartItem,Button} from '@components'

import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'

class Carts extends React.Component {
  render(){
    let {carts,isRequesting} = this.props
    let total = this.getPriceTotal()
    return (
      <SafeAreaView style={styles.container}>
      {carts.length == 0 && this.renderEmptyList()}
      {carts.length > 0 && (
        <FlatList
          contentContainerStyle={styles.list}
          keyExtractor={(item,index)=>`${index}`}
          data={carts}
          renderItem={({item})=><CartItem item={item} onRemove={this.removeToCart}/>}
          ItemSeparatorComponent={()=><View style={styles.separator}/>}
        />
      )}
      {total > 0 && (
        <View>
          <Text style={styles.total}>{__.t('Total')}: ${total}</Text>
          <Button title={__.t('Checkout')} loading={isRequesting} style={styles.btnCheckout} onPress={this.checkout}/>
        </View>
      )}

      </SafeAreaView>
    )
  }

  renderEmptyList = () => {
    return (
      <View style={styles.wrapper}>
        <Text style={styles.message}>{__.t('Empty List')}</Text>
      </View>
    )
  }

  removeToCart = (item)=>{
    this.props.removeToCart(item)
  }

  getPriceTotal = ()=>{
    var total = 0
    this.props.carts.forEach((item)=>{
      total += item.price*item.qty
    })
    return total
  }

  checkout = ()=>{
    this.isGetUserInfo = true
    this.props.getCustomerInfo(this.props.userToken)
  }

  componentWillReceiveProps(props){
    if (props.type == ActionTypes.GET_CUSTOMER_INFO_FAIL && this.isGetUserInfo == true) {
      this.isLogin = true
      this.props.signIn()
    }

    if (props.type == ActionTypes.SIGN_IN_SUCCESS && this.isLogin == true) {
      this.isLogin = false
      this.isGetUserInfo = true
      setTimeout(()=>{
        this.props.getCustomerInfo(props.userToken)
      },300)
    }

    if (props.type == ActionTypes.GET_CUSTOMER_INFO_SUCCESS && this.isGetUserInfo == true) {
      this.isGetUserInfo = false
      this.props.showShippingAddress()
    }
  }
}

Carts.defaultProps = {
  carts:[],
  type:false
}

function mapStateToProps({cartsReducers,authReducers}){
  return {
    carts:cartsReducers.carts,
    reload:cartsReducers.reload,
    type:authReducers.type,
    userToken:authReducers.userToken,
    cartType:cartsReducers.type,
    cartMessage:cartsReducers.message,
    quoteId:cartsReducers.quoteId,
    isRequesting:authReducers.type == ActionTypes.GET_CUSTOMER_INFO_PENDING
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators(ActionCreators,dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Carts)
