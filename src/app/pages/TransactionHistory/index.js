import React, { Component } from 'react';
import { Alert,View,ImageBackground,ToastAndroid,StatusBar,Image,Text, ScrollView,SafeAreaView,TouchableOpacity } from 'react-native';
import { Container, Header, Content, List, ListItem, Body,Left,Right,Icon,Button, ScrollableTab,Col,Accordion,Tab,Tabs,TabHeading,Thumbnail, Row, CardItem} from 'native-base';
import {Icons,Constants,Colors} from '@common'
import GridView from 'react-native-gridview';
import styles from './style'
import { Card } from 'react-native-elements';
import Swiper from 'react-native-swiper';
import CalendarStrip from 'react-native-calendar-strip';
import moment, { calendarFormat } from 'moment';
const itemsPerRow = 4;


class TransactionHistory extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            activeDate:false,
            activeDataList:[],
            activeNumDate:1,
            activedate:" ",
            activefee:'',
            activeBodySub:"",
            activeBodyTitle:"",
            Dates:"",
            activeicon:Icons.Home,
            activeFees:"",
            newData:[],
            dates:moment(),
            tab: {
                active: 'Category'
            },
            sort:{
                active:'down'
            },
            status:true,
            status1:true,
            status2:true,
            date:{
                active:'sat,1-december-2018'
            },
            Prolik:[],
            menu:[
            {
            id:1,
            title:'Transport',
            image:Icons.tsTransport,
            },
            {
            id:1,
            title:'Bills',
            image:Icons.tsBill,
            },
            {
            id:1,
            title:'Education',
            image:Icons.tsEducation,
            },
            {
            id:1,
            title:'Entertainment',
            image:Icons.tsEntertainment,
            },
            {
            id:1,
            title:'Food',
            image:Icons.tsFood,
            },
            {
            id:1,
            title:'HealthCare',
            image:Icons.tsHealth,
            },
            {
            id:1,
            title:'Kids',
            image:Icons.tsKids,
            },
            {
            id:1,
            title:'Pets',
            image:Icons.tsPets,
            },
            {
            id:1,
            title:'Travel',
            image:Icons.tsTravel,
            },
            {
            id:1,
            title:'Sport',
            image:Icons.tsSport,
            },
            {
            id:1,
            title:'Transport',
            image:Icons.tsBeauty,
            },
            {
            id:1,
            title:'Home',
            image:Icons.Home,
            },   
            ],
            datasheet:[
                {
                    id:1,
                    header:'Saturday,1 december 2018',
                    costfee:'-201'
                },
            ],
            
        };
    }
    
    setTabActive (active) {
        this.setState({ tab: {
            ...this.state.tab,
            active
        }})
        }
        setSortActive (active) {
            this.setState({ sort: {
                ...this.state.sort,
                active
            }})
            }
            setTimeActive (item, index) {
                //copy data dari state ke variable baru
                let newData = [...this.state.data]

                //set semua data active = false
                newData.map((item, index) => {
                    newData[index] = {...newData[index], active:false}
                })

                //mencari index berdasarkan id
                let newIndex = newData.findIndex(val => val.id === item.id)

                //update data active = true berdasarkan index
                newData[newIndex] = {...newData[newIndex], active:true}

                //update state
                this.setState({
                    data:newData,
                    activeDate:newData[newIndex].active,
                    activeNumDate:newData[newIndex].numdate,
                    newData:newData[newIndex],
                    activedate:newData[newIndex].date,
                    activefee:newData[newIndex].fee,
                    activeicon:newData[newIndex].dataList.Iconsot,
                    activeBodyTitle:newData[newIndex].dataList.BodyTitle,
                    activeBodySub:newData[newIndex].dataList.BodySub,
                    activeFees:newData[newIndex].dataList.Fees,
                })
                
            }
    
        ShowHideTextComponentView = () =>{

            if(this.state.status == true)
            {
                this.setState({status: false})
            }
            else
            {
                this.setState({status: true})
            }
            }
            ShowHideTextComponentView1 = () =>{

                if(this.state.status1 == true)
                {
                    this.setState({status1: false})
                }
                else
                {
                    this.setState({status1: true})
                }
                }
                _TimeBySort(){
                    var time = moment($(e).attr('datetime'));
                    var now = moment();
                    if(now.diff(time, 'days') <= 1) {
                        Alert.alert(moment().diff(1,'days'))
                    }
                }
                _renderHome(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>02 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>03 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
                _renderBeauty(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>02 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>03 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
                _renderSport(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>02 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>03 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
                _renderTravel(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>02 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>03 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
                _renderPets(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>02 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>03 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
                _renderKids(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>02 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>03 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
                _renderHealthCare(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>02 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>03 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
                _renderFood(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>02 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>03 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
                _renderEntertainment(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>02 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>03 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
                _renderEducation(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>02 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>03 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
                _renderTransport(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>02 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>03 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>-$250</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Uber Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$60</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Grab Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$50</Text>
                        </Right>
                        </ListItem>
                        
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsTransport} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Go Taxi</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Transport</Text>
                        </Body>
                        <Right>
                            <Text note>-$40</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
                _renderSalary(){
                    return(
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>01 january 2019</Text>
                        </Body>
                        <Right>
                        <Text>$5100</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={Icons.tsBill} style={{width:30,height:30,tintColor:'yellow'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Salary</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Bills</Text>
                        </Body>
                        <Right>
                            <Text note>$2050</Text>
                        </Right>
                        </ListItem>
                        <ListItem style={{marginVertical:10}} icon>
                        <Left>
                            <Image source={Icons.tsBill} style={{width:30,height:30,tintColor:'yellow'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>Salary</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >Bills</Text>
                        </Body>
                        <Right>
                            <Text note>$3050</Text>
                        </Right>
                        </ListItem>
                    </List>
                    </Content>
                    )
                }
            _renderDate(){
                let {sort,dates,Dates}=this.state
                let styleTabActive = {backgroundColor:Colors.Green, paddingHorizontal:5,paddingVertical:5,borderColor:'white',borderWidth:1}
                let styleTabInActive = {paddingHorizontal:5,paddingVertical:5,backgroundColor:'grey',borderColor:'green',borderWidth:1}
                let datesWhitelist = [{
                    start: moment().diff(1000,'days'),
                    today:dates,
                    end: moment().add(1000, 'days')  // total 4 days enabled
                }];
                
                // let datesBlacklist = [ moment().add(1, 'days') ];
                console.log(dates)
                Alert.alert(dates)
                console.log(Dates._d)
                return(
                        
                        <View>
                        <CalendarStrip
                            calendarAnimation={{type: 'sequence', duration: 30}}
                            daySelectionAnimation={{type: 'border', duration: 200, borderWidth: 1, borderHighlightColor: 'white'}}
                            style={{height: 100, paddingTop: 20, paddingBottom: 10}}
                            calendarHeaderStyle={{color: 'white'}}
                            calendarColor={'#1c1c1c'}
                            dateNumberStyle={{color: 'white'}}
                            dateNameStyle={{color: 'white'}}
                            highlightDateNumberStyle={{color: 'yellow'}}
                            highlightDateNameStyle={{color: 'yellow'}}
                            disabledDateNameStyle={{color: 'grey'}}
                            disabledDateNumberStyle={{color: 'grey'}}
                            datesWhitelist={datesWhitelist}
                            // datesBlacklist={datesBlacklist}
                            iconLeft={Icons.Left}
                            iconRight={Icons.Right}
                            iconLeftStyle={{tintColor:"white"}}
                            iconRightStyle={{tintColor:"white"}}
                            iconContainer={{flex: 0.1}}
                            onDateSelected={(date)=>this.setState({Dates:date})}
                        />
                    </View>
                        // ToastAndroid.show(date,ToastAndroid.SHORT)
                    )
            };
            _renderMenu(){
                return(
                    <View style={{height:40,backgroundColor:'#1c1c1c'}}>
                    <TouchableOpacity onPress={this.ShowHideTextComponentView1}>
                    {this.state.status1?<Image
                    source={Icons.ArrDown}
                    style={{width:15,height:15,tintColor:'grey',alignSelf:'center'}}
                    />:<Image
                    source={Icons.UpArrow}
                    style={{width:15,height:15,tintColor:'grey',alignSelf:'center'}}
                    />}
                </TouchableOpacity>
                </View>
                )
            }
            _renderTs(){
                let{tab}=this.state
                return(
                    <Container>
                        <Tabs renderTabBar={()=> <ScrollableTab  /> }>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.tsTransport} style={{tintColor:'white',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>Transport</Text>
                            <Text style={{color:'red',fontSize:10}}>$5000,99</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 256 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderTransport():null}
                        </Tab>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.tsBill} style={{tintColor:'yellow',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>Salary</Text>
                            <Text style={{color:'red',fontSize:10}}>$3990,59</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 156 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderSalary():null}
                        </Tab>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.tsEducation} style={{tintColor:'white',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>Education</Text>
                            <Text style={{color:'red',fontSize:10}}>$5000,99</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 256 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderEducation():null}
                        </Tab>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.tsEntertainment} style={{tintColor:'white',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>Entertainment</Text>
                            <Text style={{color:'red',fontSize:10}}>$8000,99</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 556 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderEntertainment():null}
                        </Tab>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.tsFood} style={{tintColor:'white',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>Food</Text>
                            <Text style={{color:'red',fontSize:10}}>$3400,00</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 400 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderFood():null}
                        </Tab>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.tsHealth} style={{tintColor:'white',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>HealthCare</Text>
                            <Text style={{color:'red',fontSize:10}}>$2000,99</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 126 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderHealthCare():null}
                        </Tab>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.tsKids} style={{tintColor:'white',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>Kids</Text>
                            <Text style={{color:'red',fontSize:10}}>$3200,29</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 356 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderKids():null}
                        </Tab>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.tsPets} style={{tintColor:'white',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>Pets</Text>
                            <Text style={{color:'red',fontSize:10}}>$2999.80</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 356 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderPets():null}
                        </Tab>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.tsTravel} style={{tintColor:'white',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>Travel</Text>
                            <Text style={{color:'red',fontSize:10}}>$4056,99</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 290 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderTravel():null}
                        </Tab>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.tsSport} style={{tintColor:'white',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>Sport</Text>
                            <Text style={{color:'red',fontSize:10}}>$7000,99</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 456 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderSport():null}
                        </Tab>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.tsBeauty} style={{tintColor:'white',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>Beauty</Text>
                            <Text style={{color:'red',fontSize:10}}>$5556,99</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 399 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderBeauty():null}
                        </Tab>
                        <Tab  tabStyle={{backgroundColor:'#1c1c1c'}} textStyle={{color: '#fff'}} activeTabStyle={{backgroundColor: '#1c1c1c'}} activeTextStyle={{color: '#fff', fontWeight: 'normal'}}  heading={ <TabHeading style={{height:100,width:130}}><View style={{flexDirection:'row',height:100,backgroundColor:'#1c1c1c',marginRight:5}}>
                        <View >
                            <Thumbnail small borderRadius={50} source={Icons.Home} style={{tintColor:'white',borderColor:'green',borderWidth:1,marginTop:10,marginLeft:30}}/>
                        </View>
                        <View style={{flexDirection:'column',backgroundColor:'#1c1c1c',opacity:0.70,paddingHorizontal:5,paddingVertical:5,width:120}}>
                            <Text style={{color:'white',fontSize:12}}>Home</Text>
                            <Text style={{color:'red',fontSize:10}}>$5234,99</Text>
                            <Text  style={{fontSize:8,color:'white'}}> 390 Transaction</Text>
                        </View>
                    </View></TabHeading>}>
                    {tab.active==='Category'?this._renderMenu():null}
                    {tab.active==='Category'?this._renderHome():null}
                        </Tab>
                        </Tabs>
                        </Container>
                )
            }
            
    render() {
        
        let {tab,sort,status}=this.state
        let styleTabActive = {backgroundColor:'grey',opacity:0.60,borderRadius:50, paddingHorizontal:20,paddingVertical:5,borderColor:Colors.Green,borderWidth:1}
        let styleTabInActive = {paddingHorizontal:20,paddingVertical:5,backgroundColor:'#1c1c1c',borderRadius:50,borderColor:'green',borderWidth:1}
        let styleMenuActive={flex:1,flexWrap: 'wrap',}
        let styleMenuInActive={flex:0.21}
        let styleFontActive={color:'white'}
        let styleFontInActive={color:'white'}
        let dataRow1 = this.state.menu.slice(0, 4)
        let dataRow2 = this.state.menu.slice(4, 8)
        let dataRow3 = this.state.menu.slice(8, 12)
        
        return (
        <SafeAreaView style={{flex:1,backgroundColor:'#1c1c1c'}}>
            <StatusBar
                        translucent
                        backgroundColor="transparent"
                        barStyle="light-content"
                    />
                    
                    <Header androidStatusBarColor="black"  style={{backgroundColor:'#1c1c1c',marginTop:20}}>
                    <Left style={{marginLeft:10,marginTop:10}}>
                    <TouchableOpacity onPress={this.props.goBack}>
                    <Image  source={Icons.Left} style={{width:18,height:18,tintColor:'white'}}/>
                    </TouchableOpacity>
                    </Left>
                    <Body style={{marginLeft:50,marginTop:10}}>
                        <Text style={styles.headerTitle}>Sort by {this.state.tab.active}</Text>
                    </Body>
                    <Right style={{marginTop:10,marginRight:10}}>
                        <TouchableOpacity onPress={this.ShowHideTextComponentView}>
                            {this.state.status?<Image
                            source={Icons.ArrDown}
                            style={styles.headerNotif}
                            />:<Image
                            source={Icons.UpArrow}
                            style={styles.headerNotif}
                            />}
                        </TouchableOpacity>
                    </Right>
                    </Header>
                {
                    this.state.status?null:<View style={{flexDirection:'row'}}>
                    <TouchableOpacity onPress={()=>this.setTabActive('Time')} style={tab.active==='Time'?styleTabActive:styleTabInActive}>
                        <Text style={tab.active==='Time'?styleFontActive:styleFontInActive}> By Time</Text>
                    </TouchableOpacity>
                    <View style={{width:10,backgroundColor:'transparent'}}></View>
                    <TouchableOpacity onPress={()=>this.setTabActive('Category')} style={tab.active==='Category'?styleTabActive:styleTabInActive}>
                        <Text style={tab.active==='Category'?styleFontActive:styleFontInActive}> By Category</Text>
                    </TouchableOpacity>
                    <View style={{width:10,backgroundColor:'transparent'}}></View>
                    <TouchableOpacity onPress={()=>this.setTabActive('Amount')} style={tab.active==='Amount'?styleTabActive:styleTabInActive}>
                        <Text style={tab.active==='Amount'?styleFontActive:styleFontInActive}> By Amount</Text>
                    </TouchableOpacity>
                </View>
                }
                {this.state.status1?null:<Swiper style={styles.wrapper} showsButtons={true}>
                    <View style={styles.slide1}>
                    <View style={{flexDirection:'row',marginTop:6,marginBottom:3}}>
                    {
                        
                        dataRow1.map((val,index) =>(
                            <View style={{flexDirection:'column'}}>
                            <TouchableOpacity style={{backgroundColor:'transparent',width:65,height:65}}>
                                <Image source={val.image} style={{height:30,width:30,marginVertical:2,alignSelf:'center'}}/>
                                <Text style={{fontSize:10 ,alignSelf:'center'}}>{val.title}</Text>
                            </TouchableOpacity>
                            </View>
                        ) 
                        )
                    
                    }
                    </View>
                    <View style={{flexDirection:'row',marginVertical:3}}>
                    {
                        
                        dataRow2.map((val,index) =>(
                            <View style={{flexDirection:'column'}}>
                            <TouchableOpacity style={{backgroundColor:'transparent',width:65,height:65}}>
                                <Image source={val.image} style={{height:30,width:30,marginVertical:2,alignSelf:'center'}}/>
                                <Text style={{fontSize:10 ,alignSelf:'center'}}>{val.title}</Text>
                            </TouchableOpacity>
                            </View>
                        ) 
                        )
                    
                    }
                    </View>
                    <View style={{flexDirection:'row',marginVertical:3}}>
                    {
                        
                        dataRow3.map((val,index) =>(
                            <View style={{flexDirection:'column'}}>
                            <TouchableOpacity style={{backgroundColor:'transparent',width:65,height:65}}>
                                <Image source={val.image} style={{height:30,width:30,marginVertical:2,alignSelf:'center'}}/>
                                <Text style={{fontSize:10 ,alignSelf:'center'}}>{val.title}</Text>
                            </TouchableOpacity>
                            </View>
                        ) 
                        )
                    
                    }
                    
                    </View>
                    </View>
                    <View style={styles.slide2}>
                    <Text style={styles.text}>Beautiful</Text>
                    </View>
                    <View style={styles.slide3}>
                    <Text style={styles.text}>And simple</Text>
                    </View>
                </Swiper> }
                {this.state.status1?null:tab.active==='Category'?this._renderMenu():null }
                <View style={this.state.status1?styleMenuInActive:styleMenuActive}>
                
                {tab.active==='Time'?this._renderDate():null}
                
                </View>
                {
                    this.state.activeDate ?
                        <Content style={{backgroundColor:'black'}}>
                    <List>
                        <ListItem style={{backgroundColor:'grey'}} itemDivider>
                        <Body>
                        <Text>{this.state.activedate}</Text>
                        </Body>
                        <Right>
                        <Text>-{this.state.activefee}</Text>
                        </Right>
                        </ListItem>                    
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={this.state.activeicon} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>{this.state.activeBodyTitle}</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >{this.state.activeBodySub}</Text>
                        </Body>
                        <Right>
                            <Text note>{this.state.activeFees}</Text>
                        </Right>
                        </ListItem>
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={this.state.activeicon} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>{this.state.activeBodyTitle}</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >{this.state.activeBodySub}</Text>
                        </Body>
                        <Right>
                            <Text note>{this.state.activeFees}</Text>
                        </Right>
                        </ListItem>
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={this.state.activeicon} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>{this.state.activeBodyTitle}</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >{this.state.activeBodySub}</Text>
                        </Body>
                        <Right>
                            <Text note>{this.state.activeFees}</Text>
                        </Right>
                        </ListItem>
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={this.state.activeicon} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>{this.state.activeBodyTitle}</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >{this.state.activeBodySub}</Text>
                        </Body>
                        <Right>
                            <Text note>{this.state.activeFees}</Text>
                        </Right>
                        </ListItem>
                        <ListItem icon style={{marginVertical:10}}>
                        <Left>
                            <Image source={this.state.activeicon} style={{width:30,height:30,tintColor:'white'}}/>
                        </Left>
                        <Body>
                        <Text style={{color:'white'}}>{this.state.activeBodyTitle}</Text>
                        <Text note style={{color:'grey',opacity:0.80}} >{this.state.activeBodySub}</Text>
                        </Body>
                        <Right>
                            <Text note>{this.state.activeFees}</Text>
                        </Right>
                        </ListItem>
                        
                    </List>
                    </Content>
                
                
                :
                null
                }
                {this.state.status1 && tab.active==='Category'?this._renderTs():null}
        </SafeAreaView>
        );
    }
}

export default TransactionHistory;
