import {StyleSheet, Dimensions} from 'react-native'
import {Config,Constants, Icons, Colors} from '@common'
export default StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex:1,
    flexDirection:'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  slide2: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#97CAE5',
  },
  slide3: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#92BBD9',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: 'bold',
  },
  container:{
    flex:1,
    backgroundColor:'#1c1c1c',
    paddingTop: 10,
  },
  headerContainer:{
    flexDirection:'row', justifyContent:'center', padding:16, backgroundColor:'#1c1c1c', alignItems:'center',marginTop:20
  },
  headerMenu:{
    height:25, width:25, tintColor:'white',marginRight:10
  },
  headerTitle:{
    fontWeight:'500', color:'white', fontFamily:Constants.FontFamily
  },
  headerNotif:{
    height:25, width:21,tintColor:'white',marginLeft:10,
  },
  headerNotif1:{
    height:15, width:15,tintColor:'white'
  },
  content:{
    marginHorizontal:16, marginBottom:16
  },
  contentBalanceInfo:{
    fontSize:25, fontWeight:'500', textAlign:'center', color:'white', fontFamily:Constants.FontFamily
  },
  contentSliderContainer:{
    marginVertical:20, alignItems:'center',
  },
  contentWTContainer:{
    flexDirection:'row'
  },
  contentWTButtonContainer:{
    flex:1, marginRight:5
  },
  contentWTButtonSubContainer:{
    padding:10, borderRadius:5
  },
  contentWTButtonMainContainer:{
    flexDirection:'row', justifyContent:'center', padding:10, alignItems:'center'
  },
  contentWTButtonIcon:{
    height:30, width:30
  },
  contentWTButtonText:{
    fontFamily:Constants.FontFamily, fontWeight:'500', fontSize:14
  },
  contentOPContainer:{
    marginTop:10
  },
  contentOPTitle:{
    fontWeight:'500', color:'white', fontFamily:Constants.FontFamily, marginBottom:5
  },
  contentOPSubContainer:{
    flex:1
  },
  contentOPMainContainer:{
    flexDirection:'row', justifyContent:'space-between'
  },
  contentOPMainSubContainer:{
    flex:1, alignItems:'center'
  },
  contentOPButton:{
    padding:10, borderRadius:5, alignItems:'center'
  },
  contentOPButtonIcon:{
    height:50, width:50
  },
  contentOPButtonText:{
    fontSize:12, fontWeight:'500'
  },
  contentOPArrow:{
    height:20, width:45, tintColor:'white'
  },
  logoStyle: {
    width: Dimensions.get('window').width,
    height: 200,
    
  },
  item: {
    alignItems: "center",
    backgroundColor: "#dcda48",
    flexBasis: 0,
    flexGrow: 1,
    margin: 4,
    padding: 20
  },
  itemEmpty: {
    backgroundColor: "transparent"
  },
  // text: {
  //   color: "#333333"
  // },
  image: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  modal: {
    alignItems: 'center'
  },
  modal4: {
    height: Dimensions.get('window').height
  },
})
