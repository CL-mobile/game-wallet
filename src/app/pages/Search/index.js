import React from 'react'
import {
  View,
  SafeAreaView,
  ScrollView
} from 'react-native'
import styles from './style'
import {Products,Filter,CategoriesModal} from '@components'

import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'

import {Global,Constants} from '@common'

class Search extends React.Component {
  state = {
    searchText : '',
    filter:{},
    showCategories:false,
    index:1
  }

  render(){
    let {searchedProducts,showDetail,categories,total_count} = this.props
    if (this.state.searchText.length == 0) {
      return <View />
    }

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.content}>
          {searchedProducts.length > 0 && <Products hideSection={true} products={searchedProducts} seeAll={false} horizontal={false} onPress={showDetail} onLoadMore={this.onLoadMore}/>}
        </View>
      </SafeAreaView>
    )
  }

  onShowCategories = ()=>{
    this.setState({showCategories:true})
  }

  selectCategory = (item)=>{
    var filter = this.state.filter
    filter.category_id = item.id
    this.setState({showCategories:false,filter},()=>{
      this.props.searchProducts(this.state.searchText,filter,1)
    })
  }

  onLoadMore = () => {
    if (this.props.isMore) {
      let index = this.state.index+1
      var filter = this.state.filter
      this.setState({index},()=>{
        this.props.searchProducts(this.state.searchText,filter,index)
      })
    }
  }

  componentDidMount(){
    this.onSearchListener = Global.EventEmitter.addListener(Constants.EventEmitterName.onSearch,this.onSearch)
  }

  componentWillUnmount(){
    this.onSearchListener.remove()
  }

  onSearch = (searchText)=>{
    this.setState({searchText})

    if (searchText.length == 0) {
      return
    }
    typeof this.timer != "undefined" && clearTimeout(this.timer)
    this.timer = setTimeout(()=>{
      this.props.searchProducts(searchText,this.state.filter,1)
    },500)
  }
}

Search.defaultProps = {
  searchedProducts:[],
  categories:[]
}

function mapStateToProps({productsReducers,categoriesReducers}){
  return {
    searchedProducts:productsReducers.searchedProducts,
    isMore:productsReducers.isMore,
    total_count:productsReducers.total_count,
    categories:categoriesReducers.categories
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators(ActionCreators,dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Search)
