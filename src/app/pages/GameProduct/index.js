import React, { Component } from 'react';
import {
  View,
  Dimensions,
  Image,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  LayoutAnimation,
  ActivityIndicator,
  Linking
} from 'react-native'


import styles from './style'
import {SearchBar,Promotions,BrowserByCategory,Brands,Products, Layout, HeaderBar, NavBack} from '@components'
import GridView from 'react-native-gridview';
import { Container, Header, Content, Card, CardItem, Text, BodyTab, Tabs, Tab, ScrollableTab } from 'native-base';
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'

import {Config,Constants, Icons} from '@common'

const screenWidth = Dimensions.get('window').width

const itemsPerRow = 2;
 
const data = [
  {
    title:"PUBG",
    image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
  },
  {
    title:"Fortnite",
    image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
  },
  {
    title:"Free Fire",
    image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
  },
  {
    title:"ROS",
    image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
  },
  {
    title:"Free Fire",
    image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
  },
  {
    title:"ROS",
    image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
  },
  {
    title:"PUBG",
    image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
  },
  {
    title:"Fortnite",
    image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
  },
  
]



class GameProduct extends Component {

  state = {
    data_gameProduct:this.props.navigation.state.params.data.gameProduct.game,
    dataSource:[]
  }

  componentDidMount(){
    
    const dataSource = new GridView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2,
    }).cloneWithRows(this.state.data_gameProduct);

    this.setState({dataSource:dataSource})
  }

  showGame = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }
  
  render() {
    let {goBack} = this.props
    let {subCategory, vendor} = this.props.navigation.state.params.data

    return (
      <Layout >
        <HeaderBar left={<NavBack onPress={goBack}/>} center={<Text style={styles.topHeader }>{vendor.toUpperCase()}</Text>}/>

        <Tabs style={{backgroundColor: 'transparent'}} locked={false}  renderTabBar={()=> <ScrollableTab  style={{backgroundColor: 'rgba(0,0,0,.3)', borderWidth:0 }} underlineStyle={{backgroundColor:'#6196cb', height:2, borderRadius:50}}/>}>
          {
            subCategory.map(val =>

              <Tab heading={val.sub_name}  style={{backgroundColor:'transparent'}} tabStyle={{backgroundColor: 'transparent'}}   textStyle={styles.TabsTitle} activeTabStyle={{backgroundColor: 'rgba(0,0,0,.3)' }} activeTextStyle={styles.activeTabsText}>

              <GridView
                data={val.game}
                style={{backgroundColor:'rgba(0,0,0,.15)'}}
                dataSource={this.props.randomizeRows ? this.state.dataSource : null}
                itemsPerRow={itemsPerRow}
                renderItem={(item, sectionID, rowID, itemIndex, itemID) => {
                  return (
                    <TouchableOpacity style={{margin:10}} onPress={() => this.showGame(item.link_url)}>
                      <Card  transparent={true}  style={{ flex: 1, alignItems:'center', borderRadius:5, borderWidh:0 , backgroundColor:'rgba(0,0,0,.2)'}}>
                        <Image
                          source={{uri:item.cover_image}}
                          style={{height:200, width:200, borderTopLeftRadius:5, borderTopRightRadius:5}}
                        />
                        <Text style={{fontWeight:'500', color:'black', textAlign:'center', padding:10}}>{item.name}</Text>
                      </Card>
                    </TouchableOpacity>
                  );
                }}
              />

              </Tab>
            )
          }
        </Tabs>

    </Layout>
    );
    
  }
}

export default GameProduct;
