import {StyleSheet, Dimensions} from 'react-native'
import {robotoWeights,iOSColors} from 'react-native-typography'

export default StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#1c1c1c',
    paddingTop: 10,
  },
  loading:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'white'
  },
  content:{
    paddingBottom:10
  },
  topHeader :{
      ...robotoWeights.light,
      marginRight:18,
      color:'white',
      textAlign:'center',
      flex:1
  },
   TabsTitle:{
       ...robotoWeights.light,
       color:iOSColors.lightGray
   },
    activeTabsText:{
        ...robotoWeights.medium,
        color:'#4acaff'
    }
})
