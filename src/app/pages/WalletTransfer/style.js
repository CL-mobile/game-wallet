import {StyleSheet,Dimensions} from 'react-native'
import {robotoWeights, iOSColors} from 'react-native-typography'

export default StyleSheet.create({
    topContainer:{
        paddingTop:20,
        paddingHorizontal: 10
    },
    textTitle:{
        ...robotoWeights.bold,
        color: iOSColors.lightGray,
        fontSize:24,
    },
    topBalanceTitle:{
        ...robotoWeights.condensed,
        color: iOSColors.gray,
        fontSize: 12,
        marginTop:10
    },
    balanceTop:{
        ...robotoWeights.bold,
        color: iOSColors.lightGray,
        fontSize:18,
    },
    formSection:{
        paddingVertical: 10,
        paddingHorizontal:16,

    },
    formSectionTitle:{
        ...robotoWeights.light,
        color:iOSColors.lightGray,
        fontSize:13.5,
        marginBottom: 7
    },
    formContainer:{
        borderWidth: .5,
        borderColor:'rgba(60,122,191,.8)',
        borderRadius:3,
        height:45,
        backgroundColor:'rgba(60,122,191,.1)'
    },
    formControl:{
        ...robotoWeights.light,
        paddingHorizontal:10,
        color:iOSColors.lightGray,
        fontSize:16.5 ,
        paddingTop:5
    },
    buttoBottom:{
        alignSelf:'flex-end',
        borderRadius:0,
        paddingHorizontal: 0,
        width:Dimensions.get('window').width,
    },
    modal: {
        flex:1,
        alignItems: 'center'
    },
    modal4: {
        height: Dimensions.get('window').height, 
        width:Dimensions.get('window').width, 
        backgroundColor:'rgba(0,0,0,.9)'
    },
    modal2:{
        height: Dimensions.get('window').height, 
        width:Dimensions.get('window').width, 
    },
    btn: {
        margin: 10,
        color: '#fff',
        padding: 10
      },
    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
      },

})