import React, {Component} from 'react'
import {View, Text, TextInput, TouchableOpacity, ScrollView,ToastAndroid,RefreshControl, BackHandler} from 'react-native'
import {Layout, HeaderBar,GradientButton, NavBack} from '@components'
import {Icons,Constants,Utils} from '@common'
import { ActionSheetCustom as ActionSheet } from 'react-native-custom-actionsheet'
import Modal from 'react-native-modalbox';
import LinearGradient from "react-native-linear-gradient";

import styles from './style'
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'
import axios from 'axios'

import {NavigationActions, StackActions} from 'react-navigation'

class WalletTransfer extends Component{


    constructor(props){
        super(props)
        var selectedFrom = 0
        var selectedTo = 1
        if(props.navigation.state.params == undefined){
            selectedFrom = 0
            selectedTo = 1
        }else{
            selectedFrom = props.navigation.state.params.index
            selectedTo = 0
        }

        this.state = {
            selectedFrom: selectedFrom,
            selectedTo: selectedTo,
            refreshing: false,
            loadingFirebase:true,
            isOpen:false,
            isErrorFund:false,
            options:[],
            CANCEL_INDEX:0,
            tranferFun:"",
            dataBalanceTransfer:props.dataBalance.balance_type.map((x, i) => {

                x.balance = Object.values(props.dataBalance.balance)[i]
    
                return x
            }),
            currency:this.props.dataBalance.currency,
        }
    }

    showFromAction = () => this.actionSheet.show()
    showToAction = () => this.actionTo.show()

    getActionFromRef = ref => (this.actionSheet = ref)
    getActionToRef = ref => (this.actionTo = ref)

    changeFrom = (index) => {
        const {selectedTo, selectedFrom} = this.state
        console.log(this.state.dataBalanceTransfer)
        index == selectedTo && this.setState({selectedTo:selectedFrom})
        index != this.getCancelIndex() && this.setState({selectedFrom:index})
        
    }

    changeTo = (index) => {
        const {selectedTo,selectedFrom} = this.state
        console.log(this.state.dataBalanceTransfer)
        index == selectedFrom && this.setState({selectedFrom:selectedTo})
        index != this.getCancelIndex() && this.setState({selectedTo:index})
    }

    getCancelIndex(){
        return this.state.options.length-1
    }
    pushWallet=()=>{
        const {dataBalanceTransfer, selectedFrom, selectedTo, tranferFun} = this.state

        if(tranferFun == ''){
            this.setState({isErrorFund:true})
            ToastAndroid.show("Please fill Total Amount", ToastAndroid.SHORT);
        }else{
            this.setState({isErrorFund:false})

            

            const data={
                "from_wallet":dataBalanceTransfer[selectedFrom].key,
                "transfer_fund":this.state.tranferFun,
                "target_wallet":dataBalanceTransfer[selectedTo].key
            }
    
            
            this.props.postTranfer(data, this.props.dataUser.token)
        }
    }
    componentDidMount(){
        
        this.props.getDataFirebase(this.props.dataUser.data.id)
        const options = Array.from(this.state.dataBalanceTransfer, x => x.name)
        options.push('Cancel')
        this.setState({options:options,CANCEL_INDEX:options.length-1, isOpen:false})
    }
    
    render() {
        // const currency = 'IDR'
    
   // let CANCEL_INDEX = options.length-1

    const title = <Text style={{ color: 'crimson', fontSize: 18 }}>Select Wallet</Text>
        // console.log(this.props.dataBalance.balance.main_wallet)
        const { selectedFrom, selectedTo,dataBalanceTransfer,options } = this.state
        const {goBack} =this.props
       // const selectedText = options[selected].component || options[selected]

        return (
            <Layout>
                <HeaderBar left={<NavBack onPress={goBack}/>}/>
                <ScrollView style={{marginBottom:30}} 
                    refreshControl={
                    <RefreshControl
                        refreshing={this.state.refreshing}
                        onRefresh={this._onRefresh}
                    />
                    }>
                    <View>
                        <View style={styles.topContainer}>
                            <Text style={styles.textTitle}>
                                Transfer Balance
                            </Text>
                            <Text style={styles.topBalanceTitle}>
                                {dataBalanceTransfer[selectedFrom].name}
                            </Text>
                            
                            <Text style={styles.balanceTop}>
                                {Utils.convertCurrency(this.props.dataBalance.currency) + " " + (this.state.loadingFirebase ? "" : Utils.convertFormatAmount(dataBalanceTransfer[selectedFrom].balance, this.props.dataBalance.currency))}
                            </Text>
                            
                        </View>
                        <View style={{ marginTop:20}}>
                            <TouchableOpacity onPress={this.showFromAction}>
                                <Form label={'Transfer From'} editable={false} value={options[this.state.selectedFrom]}/>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.showToAction}>
                                <Form label={'Transfer To'}
                                      editable={false}
                                      value={options[this.state.selectedTo]}
                                      bottomLabel={true}
                                      bottomLabelText={"Balance : "+Utils.convertCurrency(this.props.dataBalance.currency) + " " + (this.state.loadingFirebase ? "" : Utils.convertFormatAmount(dataBalanceTransfer[selectedTo].balance, this.props.dataBalance.currency))}
                                />
                            </TouchableOpacity>
                            <Form keyboardType="numeric" label={'Total Amount'} isError={this.state.isErrorFund} value={this.state.tranferFun} onChangeText={(text)=>this.setState({tranferFun:text})}/>
                        </View>
                        <ActionSheet
                            ref={this.getActionFromRef}
                            title={"Transfer From?"}
                            message={"Select your wallet"}
                            cancelButtonIndex={this.state.CANCEL_INDEX}
                            options={options}
                            onPress={this.changeFrom}
                        />
                        <ActionSheet
                            ref={this.getActionToRef}
                            title={"Transfer From?"}
                            cancelButtonIndex={this.state.CANCEL_INDEX}
                            options={options}
                            message={"Select your wallet"}
                            onPress={this.changeTo}
                        />
                    </View>
                </ScrollView>
                <GradientButton  colors={['rgba(63,154,255,.4)', 'rgba(60,122,191,.3)', 'rgba(32,65,138,.1)']} style={styles.buttoBottom} title={"SUBMIT"} onPress={this.pushWallet} loading={this.props.isRequesting} disabled={this.props.isRequesting}/>

                <Modal isOpen={this.state.isOpen} 
                        onClosed={() => this.setState({isOpen: false})} 
                        style={[styles.modal4, styles.modal]} 
                        swipeToClose={false}
                        backdrop={false}
                        backdropPressToClose={true}
                        position={"bottom"} >
                    
                    <LinearGradient colors={['rgba(63,154,255,.4)', 'rgba(60,122,191,.3)', 'rgba(32,65,138,.1)']} start={{ x: 0, y: -1}} end={{ x: 1, y: 3 }}   style={[styles.modal2,{flex:1, marginHorizontal:5, alignItems:'center', justifyContent:'center'}]}>
                        <Text style={{color:'white', fontSize:20, textAlign:'center'}}>Transfer Berhasil</Text>

                    </LinearGradient>
                    <TouchableOpacity 
                                onPress={this.onRefresh} 
                                style={[styles.btn, styles.btnModal]} 
                            >
                            <Text style={{color:'white', fontSize:30}}>X</Text>
                    </TouchableOpacity>
                </Modal>
            </Layout>
        );
    }

    onRefresh = () => {
        this.setState({isOpen: false})
        
        
        setTimeout(()=>{
            this._onRefresh()
        },500)

        
    }

    _onRefresh = () => {
        this.setState({refreshing: true,isOpen: false});
        this.props.getHomeBalanceInfo(this.props.dataUser.token)
        this.props.getDataFirebase(this.props.dataUser.data.id)
        this.setState({
            dataBalanceTransfer:this.props.dataBalance.balance_type.map((x, i) => {

                x.balance = Object.values(this.props.dataFirebase.wallet_info)[i]
    
                return x
            }),
        })
      }

    componentWillReceiveProps(nextProps){
        const {navigation } = this.props


        if (nextProps.balanceType==ActionTypes.GET_HOME_BALANCE_INFO_SUCCESS){
            setTimeout(()=>{
              this.setState({
                  refreshing: false
              })
          },250)
          }
 
        if(nextProps.isOpen){
            if(nextProps.type === ActionTypes.POST_TRANFER_SUCCESS){
                this.setState({isOpen: true,tranferFun:''})
                nextProps.setIsOpenModalTransfer(false)
            }
        }else{
            if(nextProps.type === ActionTypes.POST_TRANFER_PENDING){

            }else{
                if(nextProps.type === ActionTypes.POST_TRANFER_SUCCESS){
                    this.setState({isOpen: true,tranferFun:''})
                    nextProps.setIsOpenModalTransfer(false)
                }
            }
        }
        if(nextProps.firebaseType == ActionTypes.GET_FIREBASE_SUCCESS){
            this.setState({loadingFirebase:false})
            
          }
        
    }
}


function mapStateToProps({othersReducers,authReducers,menusReducers,bannerReducers,balanceInfoReducers,tranferReducers,firebaseReducers}){

    return {
      isProductMessage:othersReducers.isProductMessage,
      dataUser:authReducers.dataUser,
      balanceType:balanceInfoReducers.type,
      dataBalance:balanceInfoReducers.dataBalance,
      isLoadingB:balanceInfoReducers.isLoadingB,
      type:tranferReducers.type,
      message:tranferReducers.message,
      isOpen:tranferReducers.isOpen,
      dataFirebase:firebaseReducers.dataFirebase,
      isRequestingFirebase:firebaseReducers.isRequesting,
      firebaseType:firebaseReducers.type,
      isRequesting:tranferReducers.type==ActionTypes.POST_TRANFER_PENDING
    }
  }
  
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch)
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(WalletTransfer)
  

const Form = (props) => {
    const {label, bottomLabel, bottomLabelText} = props
   return (<View style={styles.formSection}>
        <Text style={styles.formSectionTitle}>{label}</Text>
        <View style={[styles.formContainer]}>
            <TextInput  {...props}  style={styles.formControl} underlineColorAndroid={'transparent'}/>
        </View>
       {bottomLabel &&
        <Text style={{color: '#fff', textAlign: 'right'}}>{bottomLabelText}</Text>
       }
    </View>)
}