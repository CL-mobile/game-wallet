import React from 'react'
import {
  View,
  Dimensions,
  Image,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  FlatList,
  Button,
  RefreshControl,
  ScrollView,
  Alert,
  LayoutAnimation,
  ActivityIndicator
} from 'react-native'
import Carousel from 'react-native-snap-carousel';
import Modal from 'react-native-modalbox';
import GridView from 'react-native-gridview';

import styles from './style'
import {SearchBar,Promotions,BrowserByCategory,Brands,Products, ProductMessage, GameListItem,ListView, ContentLoader} from '@components'

import {  Text } from 'native-base';
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import LinearGradient from "react-native-linear-gradient";
import {Rect,Circle} from 'react-native-svg'
import * as ActionTypes from '@actions/ActionTypes'

import {Config,Constants, Icons, Colors,Utils} from '@common'
import {NavigationActions, StackActions} from 'react-navigation'
import {GET_MENU_SUCCESS} from "../../actions/ActionTypes";
import firebase from 'react-native-firebase';

var screen = Dimensions.get('window');

const itemsPerRow = 3;


class Home extends React.Component {

  state = {
    images:this.props.dataBanner,
    data:Config.dataProduct,
    dataSource:[],
    refreshing:false,
    token:this.props.dataUser.token,
    menuItem : [],
    isMenuLoading:true,
    isModalShow : false,
    loadingFirebase:true,
    modalItem : {},
      loadCount:0
  };

    componentWillMount() {

    }

  componentDidMount()
  {
        this.props.getMenu(this.props.dataUser.token)
        this.props.getPromotionSliders(this.props.dataUser.token)
        this.props.getDataFirebase(this.props.dataUser.data.id)
        this.props.getHomeBalanceInfo(this.props.dataUser.token)
        
  }

  renderItem = ({item, index}) => {
      return (
            <Image style={styles.logoStyle} source={{ uri: item.banner_url }} />
      );
  }

  showHideMessage = (item, sectionID, rowID, itemIndex, itemID) => {
    
    //item.sub_menu.length > 0 ? this.props.set_active_menu(item.active, this.props.dataMenu, item) : this._showModal(item)
    this.showListProduct(item) 
    //item.sub_menu.length > 0 ?  this.showListProduct(item) : this._showModal(item) 
  }

  showListProduct(item){

      let {sub_menu, name, gameId} = item;

      this.props.showGame({
          vendor : name,
          gameProduct:sub_menu[0],
          subCategory:sub_menu,
          gameId: gameId
      });
  }

  

  _showModal(item){
      this.setState({isModalShow:true, modalItem:item})
  }


  _onRefresh = () => {
        this.setState({refreshing: true,isMenuLoading:true});
        this.props.getPromotionSliders(this.props.dataUser.token)
       this.props.getHomeBalanceInfo(this.props.dataUser.token)
       this.props.getMenu(this.props.dataUser.token)
  }

  render(){
let {navigation}=this.props
    return (
      <SafeAreaView style={styles.container}>
        
        <LinearGradient colors={['#130b44','#2b188e']} style={{flex:1}}>
          <View style={{flex:1,backgroundColor:'rgba(0,0,0,.7)', paddingTop: StatusBar.currentHeight}}>
        <StatusBar
                translucent={true}
                backgroundColor={'transparent'}
            />
        <View style={styles.headerContainer}>
          <TouchableOpacity style={{paddingVertical: 16}}>
            <Image
              source={Icons.Drawer}
              style={styles.headerMenu}
            />
          </TouchableOpacity>
            <TouchableOpacity onPress={() => this.props.balanceDetail()}>
              <View style={{flex:1, flexDirection:'column', justifyContent:'center'}}>
                  <Text style={styles.headerTitle}>TOTAL BALANCE</Text>
              {this.state.loadingFirebase? 
              <ActivityIndicator size="small" color="white" style={[styles.activityIndicat,{alignSelf:'center'}]} />
              : 
              <Text style={[styles.subHeaderText1,{alignSelf:'center'}]}>{Utils.convertCurrency(this.props.dataBalance.currency)} {Utils.convertFormatAmount(this.props.dataFirebase.total_balance, this.props.dataBalance.currency)} </Text>}
              </View>
            </TouchableOpacity>

          <TouchableOpacity style={{paddingVertical: 16}}>
            <Image
              source={Icons.NotificationActive}
              style={styles.headerNotif}
            />
          </TouchableOpacity>
        </View>

        <ScrollView showsVerticalScrollIndicator={false} refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
        />}>
          <View style={styles.content}>
            <View style={styles.contentSliderContainer}>

                <Carousel
                  autoplayDelay={500}
                  autoplay={true}
                  loop={true}
                  inactiveSlideOpacity={0.6}
                  inactiveSlideScale={0.65}
                  firstItem={1}
                  sliderWidth={Dimensions.get('window').width}
                  itemWidth={Dimensions.get('window').width}
                  data={this.props.dataBanner}
                  renderItem={this.renderItem}
                  
                />

            </View>

            {
              this.state.loadingFirebase?
              <View></View>
              :
                !Utils.isEmpty(this.props.dataFirebase.pending) &&
                <TouchableOpacity onPress={()=>navigation.navigate(Constants.Screen.DepositConfirmStep)}>
                <LinearGradient colors={[ '#E5CD60', '#E5CD60']} style={[{ margin:10, padding:10, borderRadius:3, borderWidth: .6, borderColor:'#E5C02C'}]} >
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Image source={Icons.icWarning} style={{width:20, height:20, marginRight:10,  tintColor:'#d2ad2c'}}/>
                        <Text style={styles.warningText}>{this.props.dataFirebase.pending.ket}</Text>
                        <ActivityIndicator size={'small'} color={'#fff'} style={{width:20,height:20, alignSelf:'flex-end'}}/>
                    </View>
                </LinearGradient>
                </TouchableOpacity>
            }
            <View style={styles.contentWTContainer} >

              <TouchableOpacity style={styles.contentWTButtonContainer}>
                <LinearGradient colors={[ 'rgba(47,53,111,0.3)', 'transparent',]} style={styles.contentWTButtonSubContainer}>
                  <View style={styles.contentWTButtonMainContainer}>
                    <Image
                      source={Icons.Refresh}
                      style={[styles.contentWTButtonIcon, {marginRight:10}]}
                    />
                    <Text style={styles.contentWTButtonText}>Withdraw</Text>
                  </View>
                </LinearGradient>
              </TouchableOpacity>

              <TouchableOpacity style={styles.contentWTButtonContainer}>
                <LinearGradient colors={[ 'rgba(47,53,111,0.3)', 'transparent']} style={styles.contentWTButtonSubContainer}>
                  <View style={styles.contentWTButtonMainContainer}>
                    <Image
                      source={Icons.Add2}
                      style={[styles.contentWTButtonIcon, {marginRight:10}]}
                    />
                    <Text style={styles.contentWTButtonText}>Top Up</Text>
                  </View>
                </LinearGradient>
              </TouchableOpacity>
            </View>
            
            <View style={styles.contentOPContainer}>
                  <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', paddingHorizontal:5}}>
                    <Text style={[styles.contentOPTitle,{marginLeft:5}]}>Our Products</Text>
                    <TouchableOpacity onPress={() => this.props.showGameAll(this.props.dataMenu)}>
                      <Text style={{fontSize:14, fontWeight:'500', color:Colors.AppColor, fontFamily:Constants.FontFamily, marginBottom:5}}>Show All</Text>
                    </TouchableOpacity>
                  </View>
                  {this.state.isMenuLoading || this.state.menuItem.length <= 0 ?
                      <LoaderMenu/>
                  :
                  <GridView
                    data={this.state.menuItem}
                    dataSource={this.props.randomizeRows ? this.state.dataSource : null}
                    itemsPerRow={itemsPerRow}
                    style={{flex:1}}
                    renderItem={(item, sectionID, rowID, itemIndex, itemID) => {
                      return (
                        <View style={styles.contentOPMainSubContainer}>
                          <TouchableOpacity onPress={() =>  this.showHideMessage(item, sectionID, rowID, itemIndex, itemID)} >
                            <LinearGradient colors={['rgba(5,70,91,.3)', 'transparent']}  style={[styles.contentOPButton]} >

                              <Image
                                source={{uri:item.icon}}
                                style={styles.contentOPButtonIcon}
                              />
                              <Text style={styles.contentOPButtonText}>{(item.name).substring(0,11)}</Text>
                            </LinearGradient>
                          </TouchableOpacity>
                          {
                            item.active  ? <View style={{alignItems:'center'}}><Image source={Icons.ArrowUp} style={styles.contentOPArrow}/></View> : null
                          }
                          {
                            item.active && itemIndex == 0?
                            <View style={{backgroundColor:'white', padding:20, borderRadius:5, width:screen.width - 20, }}>
                              <ProductMessage title={item.name} showGameProduct={this.props.showGameProduct} subCategory={item.sub_menu} />
                            </View>
                            : null
                          }
                          {
                            item.active && itemIndex == 1?
                            <View style={{backgroundColor:'white', padding:20, borderRadius:5, width:screen.width - 20, marginLeft:-115}}>
                              <ProductMessage title={item.name} showGameProduct={this.props.showGameProduct} subCategory={item.sub_menu} />
                            </View>
                            : null
                          }
                          {
                            item.active && itemIndex == 2?
                            <View style={{backgroundColor:'white', padding:20, borderRadius:5, width:screen.width - 20, marginLeft:-235 }}>
                              <ProductMessage title={item.name} showGameProduct={this.props.showGameProduct} subCategory={item.sub_menu} />
                            </View>
                            : null
                          }

                        </View>
                      );
                    }}
                  />
                  }
            </View>

            <View style={{marginTop:10, marginHorizontal:2}}>
              <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center', paddingHorizontal:5}}>
                <Text style={[styles.contentOPTitle,{marginLeft:5}]}>Most Popular</Text>
                <TouchableOpacity>
                  <Text style={{fontSize:14, fontWeight:'500', color:Colors.AppColor, fontFamily:Constants.FontFamily, marginBottom:5}}>Others</Text>
                </TouchableOpacity>
              </View>
              
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                {
                  this.props.dataPopular.map((val, index)=>
                    <GameListItem item={val} index={index} key={index}/>
                  )
                }
              </ScrollView>

            </View>
          </View>
        </ScrollView>
            <ModalBox itemRender={this.state.modalItem} isOpen={this.state.isModalShow} onClosed={() => this.setState({isModalShow: false})} />
          </View>
        </LinearGradient>
      </SafeAreaView>
    )
  }
    componentWillReceiveProps(nextProps){


        if(nextProps.menuType == ActionTypes.GET_MENU_SUCCESS){
                const dataSource = new GridView.DataSource({
                    rowHasChanged: (r1, r2) => r1 !== r2,
                }).cloneWithRows(nextProps.dataMenu);

                    this.setState({
                        menuItem: nextProps.dataMenu.slice(0, 6),
                        dataSource: [],
                        loadCount:this.state.loadCount+1
                    })


                setTimeout(()=>{
                    this.setState({
                        isMenuLoading: false,
                        refreshing: false
                    })
                },250)
        }
        
        else if(nextProps.menuType == ActionTypes.GET_MENU_FAIL){
          Alert.alert(
            'Expired',
            nextProps.messageMenu,
            [
              {text: 'OK', onPress: this.logout},
            ],
            {cancelable: false},
          );
        }
        

        if(nextProps.firebaseType == ActionTypes.GET_FIREBASE_SUCCESS){
          this.setState({loadingFirebase:false})
        }
    }

    logout = () => {
      this.props.logout()
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName:Constants.Screen.Login})],
      });
      this.props.navigation.dispatch(resetAction);
    }

}


function mapStateToProps({othersReducers,authReducers,menusReducers,bannerReducers,balanceInfoReducers,firebaseReducers}){

  return {
    isProductMessage:othersReducers.isProductMessage,
    dataUser:authReducers.dataUser,
    menuType:menusReducers.type,
    dataMenu:menusReducers.dataMenu,
    dataPopular:menusReducers.dataPopular,
    dataBanner:bannerReducers.dataBanner,
    dataBalance:balanceInfoReducers.dataBalance,
    dataFirebase:firebaseReducers.dataFirebase,
    isRequestingFirebase:firebaseReducers.isRequesting,
    firebaseType:firebaseReducers.type,
    isRequesting:menusReducers.isRequesting,
    isLoadingB:balanceInfoReducers.isLoadingB,
  }
}


function mapDispatchToProps(dispatch){
  return bindActionCreators(ActionCreators,dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(Home)


class LoaderMenu extends React.Component{

  constructor(props){
    super(props)
  }
  render(){
        let arr = [1,2,3,4,5,6]
      return(<FlatList
              data={arr}
              keyExtractor={(arr, index) => index}
              renderItem={({ item }) => (
                  <View style={styles.contentOPMainSubContainer}>
                    <LinearGradient colors={['rgba(5,70,91,.3)', 'transparent']} style={[styles.contentOPButton]}>
                      <ContentLoader primaryColor="rgba(0, 54, 94,.2)" secondaryColor="rgba(91, 183, 255,.1)" height={100}>
                        <Rect x="109.59" y="70.4" rx="5" ry="5" width="85" height="9"/>
                        <Circle cx="152" cy="29" r="23" />
                      </ContentLoader>
                    </LinearGradient>
                  </View>
              )}
              numColumns={3}/>)
    }
}


const ModalBox = props => {
  return <Modal {...props} style={{height:200, backgroundColor:'rgba(255,255,255,.7)', padding:16}}  position={"bottom"} swipeArea={20} >
              <View style={{marginTop: 5,flex:1, alignItems:'center'}}>
                   <Image style={{width:50, height:50,  alignSelf:'center', marginBottom:5}} source={{uri:props.itemRender.icon}}/>
                  <Text style={{textAlign: 'center', color:'#fff'}}>{props.itemRender.name}</Text>
                  <Text style={{textAlign: 'center', color:'#414141', fontWeight:'500', marginTop:10}}>COMING SOON</Text>
              </View>
          </Modal>
}


