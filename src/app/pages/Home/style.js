import {StyleSheet, Dimensions} from 'react-native'
import {Config,Constants, Icons, Colors} from '@common'
import {robotoWeights, iOSColors} from 'react-native-typography'
export default StyleSheet.create({
  container:{
    flex:1
  },
  headerContainer:{
    flexDirection:'row', justifyContent:'space-between', paddingHorizontal:16, backgroundColor:'transparent' , alignItems:'center'
  },
  headerMenu:{
    height:25, width:25, tintColor:'white'
  },
  headerTitle:{
      ...robotoWeights.condensed,
      borderWidth:.5,
      borderRadius:50,
      paddingHorizontal:8,
      paddingVertical:3,
      borderColor:iOSColors.lightGray,
      fontSize:8,
      color:iOSColors.lightGray,
      alignSelf: 'center'
  },
    subHeaderText:{
        ...robotoWeights.light,
        fontSize:15,
        marginTop:5,
        color:'#FFF',
        alignSelf: 'center'
    },
    subHeaderText1:{
      ...robotoWeights.light,
      fontSize:15,
      marginTop:5,
      color:'#FFF',
  },
    activityIndicat:{
      marginTop:5
    },
  headerNotif:{
    height:25, width:21
  },
  content:{ marginBottom:16, backgroundColor:'transparent'},
  contentBalanceInfo:{
    fontSize:25, fontWeight:'500', textAlign:'center', color:'white', fontFamily:Constants.FontFamily
  },
  contentSliderContainer:{
    marginBottom:20, alignItems:'center',
  },
  contentWTContainer:{
    flexDirection:'row',
    paddingHorizontal:5
  },
  contentWTButtonContainer:{
    flex:1, marginRight:5
  },
  contentWTButtonSubContainer:{
    padding:10, borderRadius:5,  marginHorizontal: 5
  },
  contentWTButtonMainContainer:{
    flexDirection:'row', justifyContent:'center', padding:10, alignItems:'center'
  },
  contentWTButtonIcon:{
    height:30, width:30
  },
  contentWTButtonText:{
      ...robotoWeights.light, fontSize:14, color:iOSColors.tealBlue,
      textShadowColor: 'rgba(54, 136, 135, 0.65)',
      textShadowOffset: {width: -.1 , height: 0.1},
      textShadowRadius: 10
  },
  contentOPContainer:{
    marginTop:12,
     paddingHorizontal:5,
  },
  contentOPTitle:{
      ...robotoWeights.medium,color:'white',  marginBottom:5, fontSize:18
  },
  contentOPSubContainer:{

  },
  contentOPMainContainer:{
    flexDirection:'row',flex:1
  },
  contentOPMainSubContainer:{
    flex:1, marginHorizontal: 5, marginVertical: 5
  },
  contentOPButton:{
    padding:10, borderRadius:5,alignItems:'center', flex:1
  },
  contentOPButtonIcon:{
    height:50, width:50
  },
  contentOPButtonText:{
      ...robotoWeights.thin,
    fontSize:14, fontWeight:'400',marginTop: 10,
      color:iOSColors.lightGray
  },
  contentOPArrow:{
    height:20, width:45, tintColor:'white', alignItems:'center'
  },
  logoStyle: {
    width: Dimensions.get('window').width,
    height: 200,
    resizeMode:'cover'
  },
  item: {
    alignItems: "center",
    backgroundColor: "#dcda48",
    flexBasis: 0,
    flexGrow: 1,
    margin: 4,
    padding: 20
  },
  itemEmpty: {
    backgroundColor: "transparent"
  },
  text: {
    color: "#333333"
  },
  image: {
    width: 100,
    height: 100,
    borderRadius: 10,
  },
  modal: {
    alignItems: 'center'
  },
  modal4: {
    height: Dimensions.get('window').height
  },
    warningText:{
        flex:1,
        ...robotoWeights.light,
        color:iOSColors.lightGray,
        fontSize:14,
    }
})
