import React, { Component } from 'react';
import {
  View,
  Dimensions,
  Image,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  LayoutAnimation,
  ActivityIndicator,
  Linking,
  KeyboardAvoidingView,
  TextInput,
  DatePickerAndroid,
  DatePickerIOS,
  Platform
} from 'react-native'


import styles from './style'
import {SearchBar,Promotions,BrowserByCategory,Brands,Products, Layout, HeaderBar, NavBack,GradientButton} from '@components'
import {Icons,Constants,Colors,Utils} from '@common'
import GridView from 'react-native-gridview';
import { Container, Header, Content, Card, CardItem, Text, BodyTab, Tabs, Tab, ScrollableTab } from 'native-base';
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'
import { Tooltip } from 'react-native-elements';

class EditProfile extends Component {

    constructor(props)
    {
        super(props)
        this.state = {
            fname:"Husni",
            lname:"Ailatat",
            bod:"1998-08-10",
        }
    }
    render() {
        let {goBack} = this.props

        return (
            <Layout>
                <HeaderBar left={<NavBack onPress={goBack}/>} center={<Text style={styles.topHeader }>Edit Profile</Text>}/>
                <ScrollView>
                    <View>
                        <KeyboardAvoidingView enabled={true}  keyboardVerticalOffset={10} behavior={'padding'} style={{marginVertical:20}}>
                            <View style={{marginVertical: 10}}>
                                <Form label={'First Name'} placeholderTextColor="#aaa" placeholder="Enter First Name" value={this.state.fname} onChangeText={(text) => this.setState({fname:text})} />
                                <Form label={'Last Name'} placeholderTextColor="#aaa" placeholder="Enter Last Name" value={this.state.lname} onChangeText={(text) => this.setState({lname:text})} />
                                <Form type="date" label={'Birth of Day'}  valueDate={this.state.bod} onPress={this.openDatePicker} />
                                <Form label={'Phone Number'}  value="+6281237123" editable={false} />
                            </View>
                            <View style={{marginVertical: 10}}>
                                <Form label={'Username'}  value="husni123" editable={false} />
                                <Form label={'Email'}  value="husniailatat@gmail.com" editable={false} />
                            </View>
                        </KeyboardAvoidingView>
                    </View>
                </ScrollView>
                <GradientButton colors={['rgba(63,154,255,.4)', 'rgba(60,122,191,.3)', 'rgba(32,65,138,.1)']}  style={styles.buttoBottom } title={'SAVE'} onPress={this.submitDeposit}/>
            </Layout>
        )
    }

    openDatePicker = () => {
        if(Platform.OS === "android"){
            try {
                const {action, year, month, day} = DatePickerAndroid.open({
                  // Use `new Date()` for current date.
                  // May 25 2020. Month 0 is January.
                  date: new Date(),
                  
                });

                if (action !== DatePickerAndroid.dateSetAction) {
                    // Selected year, month (0-11), day
                    console.log(month)
                  }
                
              } catch ({code, message}) {
                console.warn('Cannot open date picker', message);
              }
        }
    }

}

export default EditProfile

class Form extends Component{

    render(){
        const {label, type, valueDate, onPress} = this.props
        return(<View style={styles.formBox} >
            <View style={{flex:1,flexDirection:'row', justifyContent: 'space-between', alignItems:'center', }}>
                
                <Text style={styles.formLabel}>{label}</Text>
                {type == "date" ? 
                <TouchableOpacity onPress={onPress} style={{paddingRight:11, paddingVertical:15}}>
                    <Text style={styles.formLabel}>{valueDate}</Text>
                </TouchableOpacity>
                :
                <TextInput
                    {...this.props}
                    underlineColorAndroid="transparent"
                    style={styles.formControl}
                />
                }
            </View>
        </View>)
    }

}

Form.defaultProps = {
    isError:false,
    type:null,
    valueDate:""
}