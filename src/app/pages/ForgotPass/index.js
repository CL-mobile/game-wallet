import React, { Component } from 'react';
import { View,KeyboardAvoidingView, ToastAndroid,Text ,StatusBar,ImageBackground,Image,TextInput,TouchableOpacity,ScrollView,Dimensions} from 'react-native';
import {Icons,Constants} from '@common'
import {GradientButton} from '@components'
import {  Item, Input, Icon } from 'native-base';
import style from './style'
import {iOSColors} from "react-native-typography"
import LinearGradient from 'react-native-linear-gradient';
import {NavigationActions, StackActions} from 'react-navigation'
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'
class ForgotPass extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading:false,
        contentHeight:0,
        email:"",
    };
  }

    measureView(event) {
        alert(event.nativeEvent.layout.height);
        this.setState({
            contentHeight: event.nativeEvent.layout.height
        })
    }
onValidationCOde=()=>{
    this.props.forgotPass(this.state.email)
}

  render() {
      const {showHome,showRegister,showactive,isRequesting} = this.props
      const {width,height} = Dimensions.get('window')

    return (
        <KeyboardAvoidingView styles={{flex:1}} enabled resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={false}>
            <ScrollView styles={{flex:1}}>
             <ImageBackground source={Icons.AppBG} style={{flex:1,resizeMode:'cover',minHeight: height }}>
                 <StatusBar translucent backgroundColor="transparent" barStyle="light-content"/>

                    <LinearGradient colors={['rgba(0,0,0,.8)','rgba(0,0,0,.6)','rgba(0,0,0,.5)']} style={{flex:1}}>
                        <View style={{flex:1}}>
                            <Image  style ={style.appLogo} source={Icons.AppIconLogo }/>
                            <View style={ style.container }>
                                <Text style={ style.titlePages }>Forgot Password</Text>
                                <Text style={ style.subTitle }>Easy Buy Your Games Wallet with Optimus Solutions</Text>
                                <Text  style={ style.subTitle }>We guarented You for safe buy a Wallet</Text>
                                <View style={{marginTop:60}}>
                                    <View style={style.formGroup}>
                                        <TextInput value={this.state.email} style={style.form} onChangeText={(text)=>this.setState({email:text})} placeholder="Your Email" underlineColorAndroid="transparent" placeholderTextColor={iOSColors.gray} />
                                    </View>
                                    
                                    <GradientButton title={'Get Your account Now'} style={{marginTop:30 }} loading={isRequesting} onPress={this.onValidationCOde}  />
                                    <View style={{marginTop:2 }}>
                                        <View style={[style.forgotPassword,{ flexDirection:'row', alignSelf:'center', alignItems:'center',marginVertical:10}]}>
                                        <Text style={[style.forgotPassword,{ alignSelf:'flex-start' }]}> Don't have account ?
                                            </Text>
                                            <TouchableOpacity onPress={showRegister}>
                                                <Text style={[style.linkText,{alignSelf:'flex-end'}]}> Register Now </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </LinearGradient>
             </ImageBackground>
            </ScrollView>
        </KeyboardAvoidingView>
    );
  }
  componentWillReceiveProps(nextProps){
    const {navigation } = this.props

    if (nextProps.type == ActionTypes.FORGOTPASSWORD_SUCCESS) {

        navigation.navigate(Constants.Screen.ActiveCode)
        const data={
            email:this.state.email
        }
        const datas={
            data:data
        }
    this.props.goToInputCode(datas)
    ToastAndroid.show(nextProps.message, ToastAndroid.SHORT);
    } else if (nextProps.type == ActionTypes.FORGOTPASSWORD_FAIL) {

        ToastAndroid.show(nextProps.message, ToastAndroid.SHORT);
    }
}
}



function mapStateToProps({authReducers}){
    return {
        type:authReducers.type,
        message:authReducers.message,
        isRequesting:authReducers.type == ActionTypes.FORGOTPASSWORD_PENDING,
    }
}
  
function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch)
}
  
export default connect(mapStateToProps,mapDispatchToProps)(ForgotPass)