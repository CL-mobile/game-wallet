import React, { Component } from 'react';
import { View,KeyboardAvoidingView, ToastAndroid, Text ,StatusBar,ImageBackground,Image,TextInput,TouchableOpacity,ScrollView,Dimensions} from 'react-native';
import {Icons,Constants} from '@common'
import {AsyncStorage} from 'react-native';
import {GradientButton} from '@components'
import {  Item, Input, Icon } from 'native-base';
import {NavigationActions, StackActions} from 'react-navigation'
import style from './style'
import {iOSColors} from "react-native-typography"
import LinearGradient from 'react-native-linear-gradient';

import firebase from 'react-native-firebase';
import axios from 'axios'

import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
        isLoading:false,
        email:"",
        password:"",
        contentHeight:0,
        showPassword:true,
    };
  }

  _focusNextField(nextField) {
    this.refs[nextField].focus()
    }

  componentDidMount()
  {
      if(this.props.dataUser.length != 0){
          if(this.props.dataUser.token != null){
            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName:Constants.Screen.Home})],
            });
             this.props.navigation.dispatch(resetAction);
          }
      }
  }

ShowHidePassword = () =>{

    if(this.state.showPassword == true)
    {
        this.setState({showPassword: false})
    }
    else
    {
        this.setState({showPassword: true})
    }
}

    measureView(event) {
        alert(event.nativeEvent.layout.height);
        this.setState({
            contentHeight: event.nativeEvent.layout.height
        })
    }

    submitLogin = async () => {
        let fcmToken = await AsyncStorage.getItem('fcmToken')

        this.props.login(this.state.email,this.state.password,fcmToken)

    }


  render() {
      const {showRegister,showForgot} = this.props
      const {width,height} = Dimensions.get('window')

    return (
        <KeyboardAvoidingView styles={{flex:1}} enabled resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={false}>
            <ScrollView styles={{flex:1}}>
             <ImageBackground source={Icons.AppBG} style={{flex:1,resizeMode:'cover',minHeight: height }}>
                 <StatusBar translucent backgroundColor="transparent" barStyle="light-content"/>

                    <LinearGradient colors={['rgba(0,0,0,.8)','rgba(0,0,0,.6)','rgba(0,0,0,.5)']} style={{flex:1}}>
                        <View style={{flex:1}}>
                            <Image  style ={style.appLogo} source={Icons.AppIconLogo }/>
                            <View style={ style.container }>
                                <Text style={ style.titlePages }>SIGN IN</Text>
                                <Text style={ style.subTitle }>Easy Buy Your Games Wallet with E-Wallet</Text>
                                <Text  style={ style.subTitle }>We guarented You for safe buy a Wallet</Text>
                                <View style={{marginTop:60}}>
                                    <View style={style.formGroup}>
                                        <TextInput  
                                            keyboardType="email-address"
                                            onSubmitEditing={() => this._focusNextField('password')}
                                            returnKeyType = {"next"}
                                            ref="email"
                                         style={[style.form]} placeholder="Your Email" underlineColorAndroid="transparent" placeholderTextColor={iOSColors.gray} onChangeText={(email) => this.setState({email})} />
                                    </View>
                                    <View style={style.formGroup}>
                                        <Item underline={false} style={[style.form,{justifyContent:'space-between'}]}>
                                            <TextInput  
                                                returnKeyType = {"done"}
                                                ref="password"
                                                placeholder='Password' style={[style.form, {width:90}]} placeholderTextColor={iOSColors.gray} secureTextEntry={this.state.showPassword} underlineColorAndroid="transparent" onChangeText={(password) => this.setState({password})} />
                                            <TouchableOpacity onPress={this.ShowHidePassword}>
                                                <Icon active name='eye'/>
                                            </TouchableOpacity>
                                        </Item>
                                    </View>
                                    <View style={{marginTop:2 }}>
                                        <View style={[style.forgotPassword,{ flexDirection:'row', alignSelf:'flex-end', alignItems:'center',marginVertical:3}]}>
                                            <Text style={[style.forgotPassword,{ alignSelf:'flex-start' }]}> Lost password ?
                                            </Text>
                                            <TouchableOpacity onPress={showForgot}>
                                                <Text style={[style.linkText,{alignSelf:'flex-end'}]}> Click Here</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <GradientButton title={'Login to Your account'} style={{marginTop:30 }} loading={this.state.isLoading} onPress={this.submitLogin}  />
                                    <View style={{marginTop:2 }}>
                                        <View style={[style.forgotPassword,{ flexDirection:'row', alignSelf:'center', alignItems:'center',marginVertical:10}]}>
                                        <Text style={[style.forgotPassword,{ alignSelf:'flex-start' }]}> Don't have account ?
                                            </Text>
                                            <TouchableOpacity onPress={showRegister}>
                                                <Text style={[style.linkText,{alignSelf:'flex-end'}]}> Register Now </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </LinearGradient>
             </ImageBackground>
            </ScrollView>
        </KeyboardAvoidingView>
    );
  }

    componentWillReceiveProps(nextProps){
        const {navigation } = this.props

        if (nextProps.type == ActionTypes.LOGIN_SUCCESS) {

            const resetAction = StackActions.reset({
                index: 0,
                actions: [NavigationActions.navigate({routeName:Constants.Screen.Home})],
            });
            navigation.dispatch(resetAction);
            

        }else{
            ToastAndroid.show(nextProps.message, ToastAndroid.SHORT);
        }
    }
}

Login.defaultProps = {
    dataUser:[],
  }

function mapStateToProps({authReducers}){
    return {
        type:authReducers.type,
        message:authReducers.message,
        dataUser:authReducers.dataUser,
        isRequesting:authReducers.type == ActionTypes.LOGIN_PENDING,
    }
}
  
function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch)
}
  
export default connect(mapStateToProps,mapDispatchToProps)(Login)
