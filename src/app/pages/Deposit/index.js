import React, { Component } from 'react';
import { View,ImageBackground,StatusBar,Image,ScrollView,SafeAreaView ,TextInput,TouchableOpacity,FlatList,Keyboard} from 'react-native';
import { Container, Header, Content, List, ListItem, Text, Body,Left,Right,Icon ,Col,Thumbnail,Card,CardItem,Form,Picker,Item} from 'native-base';
import {Icons,Constants,Colors} from '@common'
import {ButtonGroup} from 'react-native-elements'
import {Button, Layout, HeaderBar} from '@components'
import styles from "../../../components/HeaderBar/style";
import {SharedElement} from 'react-native-motion'

class Deposit extends Component {
  constructor(props) {
    super(props);
    this.state = {
        selected:'',
        selected2: '',
        active:"deposit",
        status:true,
        selectedIndex: null,
        selectedIndex1: null,
    };
    this.focusNextField = this.focusNextField.bind(this);
    this.inputs = {};
    this.updateIndex = this.updateIndex.bind(this)
    this.updateIndex1 = this.updateIndex1.bind(this)
  }
  updateIndex (selectedIndex,selectedIndex1) {
    this.setState({selectedIndex,selectedIndex1:selectedIndex1===null})
  }
  updateIndex1 (selectedIndex1,selectedIndex) {
    this.setState({selectedIndex1,selectedIndex:selectedIndex===null})
  }
  
  focusNextField(id) {
    this.inputs[id].focus();
    }
  
    updateUser = (selected) => {
      this.setState({ selected: selected })
    }
    // onValueChange2(value: string) {
    //   this.setState({
    //     selected2: value
    //   });
    // }
  render() {
    data=['10','20','50','100','250','500','1000','2000']
    let dataRow1 = data.slice(0, 4)
    let dataRow2 = data.slice(4, 8)
 
    onButtonPress = (e) => {
      console.log(e.target.value);
    };
  const { selectedIndex,selectedIndex1 } = this.state
    
    return (<Layout sourceId={'account'} animated={true}>
            <HeaderBar
              left={<TouchableOpacity style={{paddingVertical: 16}}>
                          <Image
                              source={Icons.Drawer}
                              style={styles.headerMenu}
                          />
                      </TouchableOpacity>}
              center={<TouchableOpacity>
                  <View style={{flex:1, flexDirection:'column', justifyContent:'center'}}>
                      <Text style={styles.headerTitle}>TOTAL BALANCE</Text>
                      <Text style={[styles.subHeaderText,{alignSelf:'flex-start'}]}>1000</Text>
                  </View>
              </TouchableOpacity>}
              right={<TouchableOpacity style={{paddingVertical: 16}}>
                  <Image
                      source={Icons.NotificationActive}
                      style={styles.headerNotif}
                  />
              </TouchableOpacity>}
            />
            <Text style={{color:'white',width:300,marginLeft:-20}}>$12.500,00</Text>

     {/*<StatusBar*/}
                {/*backgroundColor="#1c1c1c"*/}
            {/*/>*/}

        {/*<Header androidStatusBarColor="#1c1c1c"   style={{backgroundColor:'#1c1c1c',height:60,marginTop:30}}>*/}
        {/*<Left  style={{flexDirection:'row'}}>*/}
        {/*<TouchableOpacity onPress={this.props.goBack}>*/}
        {/*<Image  source={Icons.Left} style={{width:20,height:20,tintColor:'white'}}/>*/}
        {/*</TouchableOpacity>*/}
        {/*</Left>*/}
          {/*<Body >*/}
            {/*<Text style={{marginLeft:50,color:'white',fontSize:20,width:200}}>Asep Jumadi</Text>*/}
          {/*</Body>*/}
          {/*<Right>*/}
          {/*<Thumbnail small   source={Icons.AvatarPro} />*/}
          {/*</Right>*/}
        {/*</Header>*/}
        {/*<ScrollView>*/}
        {/*<View style={{flex:1}}>*/}
        {/*<List>*/}
        {/*<ListItem style={{height:40,marginLeft:5,marginVertical:10}} noBorder={true} >*/}
        {/*<Left>*/}
        {/*<Text style={{color:'white',opacity:0.50,fontSize:12,marginLeft:10}}>Your Balance</Text>*/}
        {/*</Left>*/}
        {/*<Body>*/}
        {/*<Text style={{color:'white',width:300,marginLeft:-20}}>$12.500,00</Text>*/}
        {/*</Body>*/}
        {/*<Right>*/}
        {/*<Image source={Icons.More} style={{width:20,height:20,resizeMode:'contain',tintColor:'white'}}/>*/}
        {/*</Right>*/}
        {/*</ListItem>*/}
        {/*<ListItem style={{height:40,marginLeft:5,marginVertical:10}} noBorder={true} >*/}
        {/*<Body>*/}
        {/*<Text style={{color:'white'}}>Deposit</Text>*/}
        {/*</Body>*/}
        {/*<Right>*/}
        {/*<Image source={Icons.Updown} style={{width:20,height:20,resizeMode:'contain',tintColor:'white'}}/>*/}
        {/*</Right>*/}
        {/*</ListItem>*/}
        {/*<ListItem noBorder={true} style={{height:30}}>*/}
          {/*<Left>*/}
          {/*<Text style={{color:'white',opacity:0.60,fontSize:12}}>Choose Your Amount</Text>*/}
          {/*</Left>*/}
        {/*</ListItem>*/}
        {/*</List>*/}
        {/*<ScrollView horizontal={true}>*/}
        {/**/}
          {/*<View style={{flexDirection:'column'}}>*/}
        {/*<View  style={{flexDirection:'row'}}>*/}
        {/*<ButtonGroup*/}
          {/*onPress={this.updateIndex}*/}
          {/*selectedIndex={selectedIndex}*/}
          {/*buttons={dataRow1}*/}
          {/*lastBorderStyle={{color:'transparent'}}*/}
          {/*containerStyle={{height: 50,backgroundColor:'transparent',borderColor:'transparent',marginHorizontal:5}} */}
            {/*buttonStyle={{marginHorizontal:8,backgroundColor:'grey',opacity:0.80,width:100,paddingVertical:-30}}*/}
            {/*textStyle={{color:'white',marginHorizontal:5}}*/}
            {/*selectedButtonStyle={{backgroundColor:'green'}}*/}
            {/*innerBorderStyle={{width:0.01}} />*/}
        {/*</View>*/}
        {/*<View  style={{flexDirection:'row'}}>*/}
        {/*<ButtonGroup*/}
          {/*onPress={this.updateIndex1}*/}
          {/*selectedIndex={selectedIndex1}*/}
          {/*buttons={dataRow2}*/}
          {/*lastBorderStyle={{color:'transparent'}}*/}
          {/*containerStyle={{height: 50,backgroundColor:'transparent',borderColor:'transparent',marginHorizontal:5}} */}
            {/*buttonStyle={{marginHorizontal:8,backgroundColor:'grey',opacity:0.80,width:100,paddingVertical:-30}}*/}
            {/*textStyle={{color:'white',marginHorizontal:5}}*/}
            {/*selectedButtonStyle={{backgroundColor:'green'}}*/}
            {/*innerBorderStyle={{width:0.01}}*/}
          {/*/>*/}
        {/*</View>*/}
        {/*</View>*/}
        {/**/}
        {/*</ScrollView>*/}
        {/*<List>*/}
        {/*<ListItem noBorder={true} style={{height:30}}>*/}
          {/*<Left>*/}
          {/*<Text style={{color:'white',opacity:0.60,fontSize:12}}>Choose Bank</Text>*/}
          {/*</Left>*/}
        {/*</ListItem>*/}
        {/*<ListItem style={{height:40,marginLeft:5,marginVertical:5}} noBorder={true} >*/}
          {/*<Left>*/}
          {/*<Form>*/}
            {/*<Item picker>*/}
                {/*<Picker*/}
                    {/*mode="dropdown"*/}
                    {/*itemTextStyle={{color:'black'}}*/}
                    {/*style={{ width:200 ,color:'white'}}*/}
                    {/*renderToHardwareTextureAndroid={true}*/}
                    {/*selectedValue={this.state.selected2}*/}
                    {/*onValueChange={this.onValueChange2.bind(this)}*/}
                    {/*placeholder="Bank Name"*/}
                    {/*placeholderIconColor="white"*/}
                    {/*placeholderStyle={{color:'grey'}}*/}
                {/*>*/}
                    {/*<Picker.Item  label="Bank Name" value="key1" />*/}
                    {/*<Picker.Item  label="Bank Central Asia" value="key0" />*/}
                    {/*<Picker.Item  label="Bank Rakyak Idonesia" value="key2" />*/}
                    {/*<Picker.Item  label="Bank Negara Indonesia" value="key3" />*/}
                {/*</Picker>*/}
              {/*</Item>*/}
          {/*</Form>*/}
          {/*</Left>*/}
          {/*<Right>*/}
            {/*<Image source={Icons.Updown} style={{width:20,height:20,resizeMode:'contain',tintColor:'white'}}/>*/}
          {/*</Right>*/}
        {/*</ListItem>*/}
        {/*<ListItem style={{height:40,marginLeft:5,marginVertical:-5}} noBorder={true}>*/}
          {/*<Body>*/}
            {/*<Text style={{color:'white',opacity:0.60,fontSize:12}}>Account Number</Text>*/}
          {/*</Body>*/}
        {/*</ListItem>*/}
        {/*</List>*/}
        {/*<View >*/}
        {/*<View style={{flex:1,height:40,borderColor:'white',borderWidth:1,flexDirection:'row',marginHorizontal:20}}>*/}
        {/*<View style={{marginHorizontal:50}}>*/}
        {/*<TextInput placeholder="XXXX-XXXX-XXXX-XXXX"  underlineColorAndroid="transparent" maxLength={16}  placeholderTextColor="white" style={{color:'white',color:'grey',opacity:0.70,width:200}} keyboardType={"numeric"} */}
          {/*onSubmitEditing={() => {*/}
                    {/*this.focusNextField('two');*/}
                              {/*}}*/}
                    {/*returnKeyType={ "next" }*/}
                    {/*ref={ input => {*/}
                      {/*this.inputs['one'] = input;*/}
                              {/*}}*/}
        {/*/>*/}
        {/*</View>*/}
        {/*</View>*/}
        {/*</View>*/}
        {/*<List>*/}
        {/*<ListItem style={{height:40,marginLeft:5,marginVertical:-5}} noBorder={true}>*/}
          {/*<Body>*/}
            {/*<Text style={{color:'white',opacity:0.60,fontSize:12}}>Account Name</Text>*/}
          {/*</Body>*/}
        {/*</ListItem>*/}
        {/*<View >*/}
        {/*<View style={{flex:1,borderColor:'white',borderWidth:1,height:40,marginHorizontal:20}}>*/}
        {/*<View style={{marginHorizontal:50}}>*/}
        {/*<TextInput placeholder="Account Name" underlineColorAndroid="transparent" maxLength={30}  placeholderTextColor="white"  style={{color:'grey',opacity:0.70,width:200,textAlign:'justify'}}*/}
          {/*returnKeyType={ "done" }*/}
                  {/*ref={ input => {*/}
                    {/*this.inputs['two'] = input;*/}
                  {/*}}*/}
                  {/*onSubmitEditing={Keyboard.dismiss}*/}
        {/*/>*/}
        {/*</View>*/}
        {/*</View>*/}
        {/*</View>*/}
        {/*<View style={{marginHorizontal:50,marginVertical:15}}>*/}
        {/*<Button  title="Deposit" style={{height:30}}/>*/}
        {/*</View>*/}
        {/*</List>*/}
        {/*</View>*/}
        {/*</ScrollView>*/}


        </Layout>
    );
  }
}

export default Deposit;