import React,{Component} from 'react'
import {KeyboardAvoidingView,DeviceEventEmitter,NativeAppEventEmitter,Platform, Alert,Text, TextInput, View,ScrollView,CameraRoll, Dimensions, FlatList, TouchableOpacity,Keyboard,ToastAndroid,Modal,Image} from 'react-native'
import {Layout, GradientButton, HeaderBar, NavBack} from '@components'
import {Transition} from 'react-navigation-fluid-transitions'
import VirtualKeyboard from 'react-native-virtual-keyboard';
import styles from './style'
import {NavigationActions, StackActions} from 'react-navigation'
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import LinearGradient from "react-native-linear-gradient";
import * as ActionTypes from '@actions/ActionTypes'
import PhotoUpload from 'react-native-photo-upload'
import ImagePicker from 'react-native-image-picker';
import {Config,Constants, Icons, Colors, Utils} from '@common'
import RNFS from 'react-native-fs';
const dirPicutures = `${RNFS.ExternalStorageDirectoryPath}/Pictures`;
class ConfirmStep extends Component {
  constructor(props) {
    super(props);
    this.state = {
    account_bank:'',
    account_name:'',
    id_ref:'',
    isOpen: false,
    isDisabled: false,
    swipeToClose: true,
    sliderValue: 0.3,
    isErrorBank:false,
    isErrorName:false,
    photos:[],
    status:false,
    count:1,
    cot:0,
    avatarSource: null,
    imagepath:"",

    };
    
    this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
  }
  ShowHidePlus = () =>{

    if(this.state.status == true)
    {
        this.setState({status: false})
    }
    else
    {
        this.setState({status: true})
    }
    }
  selectPhotoTapped() {
    
    const options = {
      quality: 1.0,
      maxWidth: 1500,
      maxHeight: 1500,
      storageOptions: {
        skipBackup: false,
        cameraRoll: true,
        waitUntilSaved: true,
      },
    };

    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      console.log(response.fileName)
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri };
        
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source,
          imagepath:response.fileName,
        });
      }
    });
  }

  onRemove=()=>{
    this.setState({
     avatarSource:null 
    })
  }
  onTaskDelete=()=>{
    Alert.alert(
      'remove photo on upload',
      'you will remove photo in stack upload',
      [
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
        {text: 'OK', onPress: this.onRemove},
      ],
      {cancelable: false},
    );
  }
  /*onPhotoHandle=()=>{
    if(this.state.count==1){
      this.setState({count:this.state.count+1})
    }
      
      CameraRoll.getPhotos({
        first:this.state.count,
        assetType:'Photos'
    }).then(r=>{
        this.setState({photos:r.edges})
    }).catch((Err)=>{
        console.log(Err)
    })
  }
  minPhotoHandle=()=>{
    if(this.state.count>1){
      this.setState({count:this.state.count-1})
    }
    
    CameraRoll.getPhotos({
      first:this.state.count,
      assetType:'Photos'
  }).then(r=>{
      this.setState({photos:r.edges})
  }).catch((Err)=>{
      console.log(Err)
  })
}*/

  render() {
    const {goBack, nextStep} = this.props;
    return (
      <Layout>
        <HeaderBar left={<NavBack onPress={goBack}/>} style={{borderWidth:1}}/>
        <ScrollView style={{paddingHorizontal: 16}}>
          <View>
                  <Transition appear={'horizontal'}>
                      <View style={{flex:1,paddingTop:20,paddingBottom:20}}>
                          <Text style={styles.sectionTitle}> Confirmation </Text>
                          <Text style={styles.sectionSubTitle}>Check All Information in your deposit.</Text>
                      </View>
                  </Transition>
                        <View style={styles.BoxContainer}>
                        <View style={{flex:1 ,flexDirection:'row',justifyContent:'space-between'}}>
                        <Image onError={this.state.isErrorBank} style ={styles.appLogo} source={Icons.AppIconLogo}/>
                        <View style={{flexDirection:'column',marginTop:20}}>
                            <Text>18-02-2019</Text>
                            <Text style={{textAlign:'right'}}>10:12</Text>
                        </View>
                        </View>
                        <View style={{flex:1 ,flexDirection:'row',justifyContent:'space-between',marginVertical:15}}>
                            <Text style={styles.BoxTitleHead}>Your Deposit </Text>
                                <View style={{flexDirection:'column'}}>
                                    <Text style={styles.BoxTitleChild}>Bank Central Asia(BCA)</Text>
                                    <Text style={[styles.BoxTitleChild,{textAlign:'right'}]}>Rp 200.000,00</Text>
                                    
                            </View>
                            </View>
                            <View style={{flex:1 ,flexDirection:'row',justifyContent:'space-between',marginBottom:15}}>
                            <Text style={styles.BoxTitleHead}>Account Name</Text>
                                <View style={{flexDirection:'column'}}>
                                    <Text style={styles.BoxTitleChild}>Asep Jumadi</Text>
                            </View>
                            </View>
                            <View style={{flex:1 ,flexDirection:'row',justifyContent:'space-between',marginBottom:15}}>
                            <Text style={styles.BoxTitleHead}>Account Number</Text>
                                <View style={{flexDirection:'column'}}>
                                    <Text style={styles.BoxTitleChild}>XXXXXXXXXXXX9696</Text>
                            </View>
                            </View>
                            <View style={{flex:1 ,flexDirection:'row',justifyContent:'space-between',marginBottom:15}}>
                            <Text style={styles.BoxTitleHead}>Number Referensi</Text>
                                <View style={{flexDirection:'column'}}>
                                    <Text style={styles.BoxTitleChild}>1010101063639696</Text>
                            </View>
                            </View>
                        </View>
                        
          </View>
        
         
          
         <ScrollView style={{flexDirection:'row'}} horizontal={true}>
         {this.state.avatarSource === null ? (
              null
            ) : (
              <View style={{flexDirection:'row'}}>
                <View style={{flexDirection:'column',marginHorizontal:10}}>
                  <PhotoUpload
                  containerStyle={{marginTop:10,flexDirection:'row'}}
                  onPhotoSelect={avatar => {
                      if (avatar) {
                      console.log('Image base64 string: ', avatar)
                      }
                  }}
                  >
                  <Image style={{
                width: 100,
                height: 100}} source={this.state.avatarSource} />
                </PhotoUpload>
                    <TouchableOpacity style={{marginTop:10,alignSelf:'center'}} onPress={this.onTaskDelete}>
                    <Image source={Icons.Minus} style={{width:50,height:50,resizeMode:'contain'}}  />
                    </TouchableOpacity>
              </View>
                
                
             </View>
            )
            }
          {/*{this.state.photos.map((p, i) => {
       return (
           <View style={{flexDirection:'row',marginHorizontal:10}}>
        <PhotoUpload
        containerStyle={{marginTop:20,flexDirection:'row'}}
        onPhotoSelect={avatar => {
            if (avatar) {
            console.log('Image base64 string: ', avatar)
            }
    
        }}
        >
        <Image
           key={i}
           style={{
             width: 100,
             height: 100,
           }}
         source={{ uri: p.node.image.uri }}
        />
       </PhotoUpload>
       </View>
       );
     })}*/}
     <View style={{flexDirection:'column'}}>
     <TouchableOpacity style={{marginTop:20}} onPress={this.selectPhotoTapped.bind(this)}>
          <Image source={Icons.Add2} style={{width:50,height:50,resizeMode:'contain'}}  />
          </TouchableOpacity>
          
          </View>
     </ScrollView>
          
     
        
        </ScrollView>
        
                <GradientButton colors={['rgba(63,154,255,.4)', 'rgba(60,122,191,.3)', 'rgba(32,65,138,.1)']} style={styles.buttoBottom} title={"CONFIRM DEPOSIT"} onPress={this.nextPage}/>
      </Layout>
    );
  }
  backToAccount = () => {
    const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({ routeName: Constants.Screen.Account })],
      });
    this.props.navigation.dispatch(resetAction);
    this.props.navigation.dispatch(NavigationActions.back())
}
}
class Form extends Component{

  render(){
      const {label, isError} = this.props
      return(<View style={styles.formElement}>
          <Text style={styles.formLabel}>{label} </Text>
          <View style={[styles.formContainer, isError && styles.isError]}>
              <TextInput {...this.props} style={styles.formControl}  underlineColorAndroid={'transparent'}/>
          </View>
      </View>)
  }
}

Form.defaultProps = {
  isError:false
}
export default ConfirmStep;
