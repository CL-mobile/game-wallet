import React,{Component} from 'react'
import {KeyboardAvoidingView, Text, TextInput, View, Dimensions, FlatList, TouchableOpacity,Keyboard,ToastAndroid} from 'react-native'
import {Layout, GradientButton, HeaderBar, NavBack} from '@components'
import {Transition} from 'react-navigation-fluid-transitions'
import VirtualKeyboard from 'react-native-virtual-keyboard';
import styles from './style'

import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import LinearGradient from "react-native-linear-gradient";
import * as ActionTypes from '@actions/ActionTypes'

import {Config,Constants, Icons, Colors, Utils} from '@common'

class FirstStep extends Component{

    constructor(props){
        super(props)
        this.state = {
            focus: false,
            width: 10,
            textValue:'',
        }
        this.textChange = this.textChange.bind(this)
    }
    myCustomTransitionFunction = (transitionInfo) => {
        const { progress, start, end } = transitionInfo;
        const scaleInterpolation = progress.interpolate({
            inputRange: [0, 0],
            outputRange: [1, 0 ]
        });
        const {width, height} = Dimensions.get('window');
        const anim = {
            transform: [
                {
                    translateX: progress.interpolate({
                        inputRange: [0, .1],
                        outputRange: [20 - (width / 2) - (width * 1 / 2), 1]
                    })
                },
                {
                    translateY: progress.interpolate({
                        inputRange: [0, 1],
                        outputRange: [1, 1]
                    })
                }
            ]
        }
        return anim;
    }


    render(){
        const {goBack, nextStep} = this.props;

        return(<Layout  >
            <HeaderBar left={<NavBack onPress={goBack}/>} style={{borderWidth: 1}}/>
                <KeyboardAvoidingView style={[styles.flex, {maxHeight: 120}]}  behavior={'padding'}  enabled={true} keyboardVerticalOffset={-3}  >
                    <View style={styles.container} >

                        <Transition appear={'flip'} >
                            <View style={styles.formGroup}>
                                <Text style={styles.labelTitle}>Amount to deposit</Text>
                                <View style={styles.form}>
                                    <Text style={styles.leftLabel}>{Utils.convertCurrency(this.props.dataBalance.currency)}</Text>
                                    <TextInput ref={x => this.input = x} keyboardType="numeric"
                                                onChangeText={(text) => 
                                                    {
                                                        if(this.props.dataBalance.currency == "IDR"){
                                                            if(Number(text) <= 1000000)
                                                                this.setState({textValue:text})
                                                        }else if(this.props.dataBalance.currency == "USD"){
                                                            if(Number(text) <= 1000)
                                                                this.setState({textValue:text})
                                                        }
                                                    }
                                                }
                                                autoFocus={false}
                                               underlineColorAndroid={'transparent'}
                                               onSubmitEditing={this.nextPage}
                                               returnKeyType={'send'}
                                               value={this.state.textValue}
                                               placeholder="0"
                                               style={styles.inputControl}
                                    />
                                </View>
                                <View style={{height:1, width:Dimensions.get('window').width-60, backgroundColor:'rgba(16, 121, 186,.6)'}}/>
                            </View>
                        </Transition>

                    </View>
                </KeyboardAvoidingView>
            <View style={{ flex:1, justifyContent:'flex-end'}}>
                <FlatList
                    contentContainerStyle={{marginTop:20, justifyContent: 'center',
                        flexDirection: 'row',
                        flexWrap: 'wrap'}}
                        keyExtractor={(item,index)=>`${index}`}
                    data={Utils.getDataAmount(this.props.dataBalance.currency)}
                    renderItem={({item, index}) => 
                        <TouchableOpacity onPress={() => this.setState({textValue:item.key})}>
                            <View style={styles.buttonOptions}>
                                <Text style={styles.textBtnOptions}>{item.label}</Text>
                            </View>
                        </TouchableOpacity>
                    }
                />
                <GradientButton colors={['rgba(63,154,255,.4)', 'rgba(60,122,191,.3)', 'rgba(32,65,138,.1)']} style={styles.buttoBottom} title={"NEXT"} onPress={this.nextPage}  />
            </View>
        </Layout>)
    }

    componentDidMount() {
       this.input.focus();

        console.log(this.props.dataBalance)
    }

    componentWillUnmount() {

    }

    textChange(item) {
        this.setState({textValue:item});
        
    }


    nextPage = ()=>{
        Keyboard.dismiss()
        if(this.state.textValue != ''){
            this.props.nextStep(this.state.textValue);
        }else{
            ToastAndroid.show("please fill the amount", ToastAndroid.SHORT);
        }
        
    }
}

function mapStateToProps({authReducers,balanceInfoReducers}){
    return {
      dataUser:authReducers.dataUser,
      dataBalance:balanceInfoReducers.dataBalance,
    }
  }
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch)
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(FirstStep)