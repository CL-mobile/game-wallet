import React,{Component} from 'react'
import {Text, ScrollView, View, Image, TextInput, KeyboardAvoidingView, Dimensions, ToastAndroid, Button, TouchableOpacity} from 'react-native'
import {Layout, HeaderBar, NavBack, GradientButton} from '@components'
import styles from './style'
import {Transition} from "react-navigation-fluid-transitions";
import Modal from 'react-native-modalbox';
import {NavigationActions, StackActions} from 'react-navigation'
import LinearGradient from "react-native-linear-gradient";

import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'
import axios from 'axios'
import {Config,Constants, Icons, Colors, Utils} from '@common'

class ThirdStep extends Component{

    constructor(props){
        super(props)
        this.state = {
            account_bank:'',
            account_name:'',
            id_ref:'',
            isOpen: false,
            isDisabled: false,
            swipeToClose: true,
            sliderValue: 0.3,
            isErrorBank:false,
            isErrorName:false,
        }
    }

    

    render(){
        const {goBack,nextStep} = this.props;
        const data = this.props.navigation.state.params.data
        
        var BContent = <TouchableOpacity 
                            onPress={() => this.setState({isOpen: false})} 
                            style={[styles.btn, styles.btnModal]} 
                           ><Text style={{color:'black', fontSize:20}}>X</Text></TouchableOpacity>;

        return(
            <Layout>
                <HeaderBar left={<NavBack onPress={goBack}/>}/>
                <ScrollView style={{paddingHorizontal: 16, }}>
                    <View>
                        <Transition appear={'horizontal'}>
                            <View style={{flex:1,paddingTop: 20}}>
                                <Text style={styles.sectionTitle}>Your Bank Account</Text>
                                <Text style={styles.sectionSubTitle}>Please fill all field below to request deposit.</Text>
                            </View>
                        </Transition>
                        <View style={styles.BoxContainer}>
                            <Text style={styles.BoxTitle}>Your Request</Text>
                            <View style={styles.sectionContent}>
                                <View style={{flex:1}}>
                                    <Image source={{uri:data.dataBank.logo}}
                                           style={{width:50,height:40}}
                                           resizeMode={'contain'}
                                    />
                                    <Text style={styles.bankTitle}>{data.dataBank.name} ({data.dataBank.code})</Text>
                                </View>
                                <Text style={styles.amountCount}>{Utils.convertCurrency(this.props.dataBalance.currency)} {Utils.convertFormatAmount(data.amount, this.props.dataBalance.currency)}</Text>
                            </View>
                        </View>

                        <KeyboardAvoidingView enabled={true}  keyboardVerticalOffset={10} behavior={'padding'} style={{marginVertical:20}}>
                            <Form label={'Account Name'} isError={this.state.isErrorName} onChangeText={(text) => this.setState({account_name:text})} />
                            <Form label={'Account Number'}
                                isError={this.state.isErrorBank}
                                returnKeyType = {"send"}
                                onSubmitEditing={this.submitDeposit}
                                    onChangeText={(text) => this.setState({account_bank:text})}
                                  keyboardType="numeric"/>
                        </KeyboardAvoidingView>

                    </View>
                </ScrollView>
                <GradientButton colors={['rgba(63,154,255,.4)', 'rgba(60,122,191,.3)', 'rgba(32,65,138,.1)']}  style={styles.buttoBottom } title={'SUBMIT REQUEST'} onPress={this.submitDeposit} loading={this.props.isRequesting} disabled={this.props.isRequesting}
                />
                
                <Modal isOpen={this.state.isOpen} 
                        onClosed={() => this.setState({isOpen: false})} 
                        style={[styles.modal4, styles.modal]} 
                        swipeToClose={false}
                        backdrop={false}
                        backdropPressToClose={false}
                        position={"bottom"} >
                    
                    <LinearGradient colors={['rgba(63,154,255,.4)', 'rgba(60,122,191,.3)', 'rgba(32,65,138,.1)']} start={{ x: 0, y: -1}} end={{ x: 1, y: 3 }}   style={{flex:1, marginHorizontal:5, alignItems:'center', justifyContent:'center'}}>


                        <Text style={{color:'white', fontSize:20, textAlign:'center'}}>Silahkan Transfer ke Bank Account {this.state.account_bank}</Text>
                        <Text style={{color:'white', fontSize:20, textAlign:'center'}}>Nomor Refferensi {this.props.dataDeposit.id_ref}</Text>
                    
                    </LinearGradient>
                    <TouchableOpacity 
                                onPress={this.backToAccount} 
                                style={[styles.btn, styles.btnModal]} 
                            >
                            <Text style={{color:'white', fontSize:30}}>X</Text>
                    </TouchableOpacity>
                </Modal>
            </Layout>
        )
    }

    submitDeposit = () => {

        if(this.state.account_name == '' && this.state.account_bank == ''){
            this.setState({isErrorBank:true, isErrorName:true})
        }else if(this.state.account_name == ''){
            this.setState({isErrorName:true})
            this.setState({isErrorBank:false})
        }else if(this.state.account_bank == ''){
            this.setState({isErrorName:false})
            this.setState({isErrorBank:true})
        }else{
            this.setState({isErrorName:false})
            this.setState({isErrorBank:false})

            const dataParams = this.props.navigation.state.params.data

            let data = {
                payment_method:'transfer', 
                bank_name:dataParams.dataBank.code, 
                amount:dataParams.amount,
                account_bank:this.state.account_bank,
                account_name:this.state.account_name
            }
            
           this.props.submitDeposit(data, this.props.dataUser.token)
        }
        
    }

    backToAccount = () => {
        const resetAction = StackActions.reset({
            index: 0,
            actions: [NavigationActions.navigate({ routeName: Constants.Screen.Account })],
          });
        this.props.navigation.dispatch(resetAction);
        this.props.navigation.dispatch(NavigationActions.back())
    }

    componentWillReceiveProps(nextProps){

        if (nextProps.depositType == ActionTypes.SUBMIT_DEPOSIT_SUCCESS) {
            this.setState({isOpen:true})//change isOpen:true if not use confirmstep
        }else{
            ToastAndroid.show(nextProps.message, ToastAndroid.SHORT);
        }
        
    }
}

function mapStateToProps({authReducers,balanceInfoReducers,depositReducers}){
    return {
      dataUser:authReducers.dataUser,
      dataBalance:balanceInfoReducers.dataBalance,
      dataDeposit:depositReducers.dataDeposit,
      depositType:depositReducers.type,
      message:depositReducers.message,
      isRequesting:depositReducers.type==ActionTypes.SUBMIT_DEPOSIT_PENDING
    }
  }
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch)
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(ThirdStep)

class Form extends Component{

    render(){
        const {label, isError} = this.props
        return(<View style={styles.formElement}>
            <Text style={styles.formLabel}>{label} </Text>
            <View style={[styles.formContainer, isError && styles.isError]}>
                <TextInput {...this.props} style={styles.formControl}  underlineColorAndroid={'transparent'}/>
            </View>
        </View>)
    }
}

Form.defaultProps = {
    isError:false
}
