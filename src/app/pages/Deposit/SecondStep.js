import React,{Component} from 'react'
import {View, Text, ScrollView, Image, TouchableOpacity,ActivityIndicator} from 'react-native'
import {Layout, HeaderBar, NavBack} from '@components'
import {Transition} from 'react-navigation-fluid-transitions'
import styles from './style'

import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import LinearGradient from "react-native-linear-gradient";
import * as ActionTypes from '@actions/ActionTypes'

class SecondStep extends Component{

    constructor(props){
        super(props)
    }

    componentDidMount()
    {
        this.props.getBank(this.props.dataUser.token)
    }

    render(){
        
        const {goBack, nextStep} = this.props;
        const amount = this.props.navigation.state.params.amount

        return(<Layout>
            <HeaderBar left={<NavBack onPress={goBack}/>}/>
            <ScrollView style={{paddingHorizontal: 16, }}>
                <View>
                <Transition appear={'horizontal'}>
                    <View style={{flex:1,paddingTop: 20}}>
                        <Text style={styles.sectionTitle}>Choose your bank</Text>
                        <Text style={styles.sectionSubTitle}>Select your bank to deposit via transfer method.</Text>
                    </View>
                </Transition>
                <View style={styles.containerBox} >

                    {
                        this.props.isRequesting ?

                        <ActivityIndicator size="small" color="white" style={[{alignItems:'center'}]} />

                        :
                        this.props.dataBank.map((item, index) => 
                        <TouchableOpacity style={styles.itemList} onPress={() => nextStep({amount, dataBank:item})}>
                            <Image source={{uri:item.logo}} resizeMode={'contain'} style={{width:50,height:35}}/>
                            <Text style={styles.itemListText}>{item.name} ({item.code})</Text>
                        </TouchableOpacity>
                        )
                    
                    }

                </View>
                </View>
            </ScrollView>
        </Layout>)
    }

}

function mapStateToProps({bankReducers,authReducers}){
  return {
    dataBank:bankReducers.dataBank,
    isRequesting:bankReducers.isRequesting,
    dataUser:authReducers.dataUser
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators(ActionCreators,dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(SecondStep)