import {StyleSheet, Dimensions} from 'react-native'
import {robotoWeights, iOSColors} from 'react-native-typography'

export default StyleSheet.create({
    flex:{
        flex:1
    },
    container:{
        flex:1,
        paddingHorizontal:30,
        paddingTop: 20
    },
    formGroup:{
        justifyContent:'center',
        alignItems: 'center'
    },
    labelTitle : {
        ...robotoWeights.thin,
        textAlign: 'center',
        color:iOSColors.midGray,
        fontSize:12
    },
    form:{flexDirection:'row', alignItems:'center'},
    leftLabel : {
        ...robotoWeights.thin,
        textAlign: 'right',
        color: iOSColors.midGray,
        fontSize:24,
        paddingRight: 5
    },
    inputControl : {
        ...robotoWeights.thin,
        color:iOSColors.lightGray,
        textAlign:'center',
        fontSize:24
    },

    buttonOptions:{
        borderWidth: .5,
        borderColor: iOSColors.lightGray,
        borderRadius: 3,
        paddingVertical: 6, paddingHorizontal:10, marginHorizontal: 5, marginVertical: 5
    },

    textBtnOptions:{
        ...robotoWeights.thin,
        fontSize:14,
        color:iOSColors.lightGray
    },

    //STEP2
    sectionTitle:{
        ...robotoWeights.bold,
        flex:1,
        fontSize:24,
        color:iOSColors.lightGray
    },
    sectionSubTitle:{
        ...robotoWeights.thin,
        paddingRight:20,
        marginTop:2,
        lineHeight:18,
        fontSize:12,
        color:iOSColors.midGray
    },
    containerBox:{
        flex:1, paddingVertical: 10, borderRadius:3, backgroundColor:'#f7f7f7', marginTop: 30
    },
    itemList:{
        flex:1, flexDirection:'row', paddingHorizontal:10, alignItems: 'center', paddingVertical: 10, borderBottomColor:'#ddd',borderBottomWidth: .5
    },
    itemListText:{
        ...robotoWeights.medium,
        flex:1,
        marginLeft: 15,
        fontSize:17,
        color:'#121212',
    },

    //Step3
    BoxContainer:{backgroundColor:'#fff', borderRadius:3, paddingVertical: 10,paddingHorizontal:10, marginTop:20},
    BoxTitle:{
        ...robotoWeights.light,
        color:'#121212',
        fontSize:15.5,
    },
    BoxTitleChild:{
        ...robotoWeights.light,
        color:'#121212',
        fontSize:14,
    },
    BoxTitleHead:{
        ...robotoWeights.medium,
        color:iOSColors.gray,
        fontSize:14,
    },
    bankTitle:{
        ...robotoWeights.light,
        fontSize:11,
        color:'#121212'
    },
    amountCount:{
        ...robotoWeights.thin,
        alignSelf: 'center',
        color:'#121212',
        fontSize:26
    },
    sectionContent:{flex:1, flexDirection:'row', marginTop:10,justifyContent: 'space-between', alignItems: 'center'},
    formElement:{flex:1, marginBottom:20},
    formLabel:{
        ...robotoWeights.thin,
        color:'#f7f7f7',
        marginBottom: 7.6
    },
    formContainer:{
        borderWidth: .5,
        borderColor:'rgba(60,122,191,.8)',
        borderRadius:3,
        height:45,
        backgroundColor:'rgba(60,122,191,.1)'
    },
    isError:{
        borderColor:'rgba(179,30,54,.8)',
        backgroundColor:'rgba(179,30,54,.1)'
    },
    formControl:{
        ...robotoWeights.light,
        paddingHorizontal:10,
        color:iOSColors.lightGray,
        fontSize:16.5 ,
        paddingTop:5
    },
    buttoBottom:{
        alignSelf:'flex-end',
        borderRadius:0,
        paddingHorizontal: 0,
        width:Dimensions.get('window').width
    },

    modal: {
        flex:1,
        alignItems: 'center'
    },
    modal4: {
        height: Dimensions.get('window').height, 
        backgroundColor:'rgba(0,0,0,.9)'
    },
    btn: {
        margin: 10,
        color: '#fff',
        padding: 10
      },
    btnModal: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 50,
        height: 50,
        backgroundColor: "transparent"
      },
      appLogo:{
        width:125,
        height:50,
        resizeMode:'contain',
        marginTop:10,
        marginBottom:30,
    },
    onchildParent:{
        flex:1 ,
        flexDirection:'row',
        justifyContent:'space-between'
    },
})