import React, { Component } from 'react';
import { View,KeyboardAvoidingView, Text ,ToastAndroid,StatusBar,ImageBackground,Image,TextInput,TouchableOpacity,ScrollView,Dimensions} from 'react-native';
import {Icons,Constants} from '@common'
import {GradientButton} from '@components'
import {  Item, Input, Icon } from 'native-base';
import style from './style'
import {iOSColors} from "react-native-typography"
import LinearGradient from 'react-native-linear-gradient';
import CodeInput from 'react-native-code-input';
import {NavigationActions, StackActions} from 'react-navigation'
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'

class ActiveCode extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading:false,
            contentHeight:0,
            code:"",
        };
      }
    
        measureView(event) {
            alert(event.nativeEvent.layout.height);
            this.setState({
                contentHeight: event.nativeEvent.layout.height
            })
        }
    
        onResetPass=()=>{
            let {code}=this.state
            let datas = this.props.navigation.state.params
            let Emaildata=datas.data.email
            console.log(code)
            console.log(Emaildata)
            this.props.inputVer(Emaildata,code)
        }
      render() {
          const {showHome,showRegister,showForgot,isRequesting} = this.props
          const {width,height} = Dimensions.get('window')
    
        return (
            <KeyboardAvoidingView styles={{flex:1}} enabled resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={false}>
            <ScrollView styles={{flex:1}}>
             <ImageBackground source={Icons.AppBG} style={{flex:1,resizeMode:'cover',minHeight: height }}>
                 <StatusBar translucent backgroundColor="transparent" barStyle="light-content"/>

                    <LinearGradient colors={['rgba(0,0,0,.8)','rgba(0,0,0,.6)','rgba(0,0,0,.5)']} style={{flex:1}}>
                        <View style={{flex:1}}>
                            <Image  style ={style.appLogo} source={Icons.AppIconLogo }/>
                            <View style={ style.container }>
                                <Text style={ style.titlePages }>Verification Code</Text>
                                <Text style={ style.subTitle }>Easy Buy Your Games Wallet with Optimus Solutions</Text>
                                <Text  style={ style.subTitle }>We send your verification code to  email address,if you don't get verification code,please resend or check your email address</Text>
                                <View style={{marginTop:60}}>
                                    
                                    <CodeInput
                                    ref='codeInputRef1'
                                    borderType='underline'
                                    space={10}
                                    activeColor='rgb(255, 255, 255)'
                                    inactiveColor='rgb(0, 123, 254)'
                                    codeInputStyle={{ borderWidth: 1.5 ,colors:iOSColors.lightGray}}
                                    size={40}
                                    inputPosition='center'
                                    onFulfill={(code) => this.setState({code:code})}
                                    />
                                    
                                    <GradientButton title={'Submit Now'} style={{marginTop:30 }} loading={isRequesting} onPress={this.onResetPass}  />
                                    <View style={{marginTop:2 }}>
                                        <View style={[style.forgotPassword,{ flexDirection:'row', alignSelf:'center', alignItems:'center',marginVertical:10}]}>
                                        <Text style={[style.forgotPassword,{ alignSelf:'flex-start' }]}> can't receive varification code ?
                                            </Text>
                                            <TouchableOpacity onPress={showForgot}>
                                                <Text style={[style.linkText,{alignSelf:'flex-end'}]}>  Resend now</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    </LinearGradient>
             </ImageBackground>
            </ScrollView>
        </KeyboardAvoidingView>
        );
      }
      componentWillReceiveProps(nextProps){
        const {navigation } = this.props
    
            if (nextProps.type == ActionTypes.INPUTCODE_SUCCESS) {
            // console.log(nextProps.data)
            navigation.navigate(Constants.Screen.ResetPassword)
                        const Vc ={
                        vcdata:nextProps.data
                       
                    }
                    console.log(Vc)
                    this.props.showReset(Vc)
            
        } else if (nextProps.type == ActionTypes.INPUTCODE_FAIL) {
    
            ToastAndroid.show(nextProps.message, ToastAndroid.SHORT);
        }
    }
    }

    function mapStateToProps({authReducers}){
        return {
            type:authReducers.type,
            data:authReducers.data,
            message:authReducers.message,
            isRequesting:authReducers.type == ActionTypes.INPUTCODE_PENDING,
        
        }
    }
    
    function mapDispatchToProps(dispatch){
        return bindActionCreators(ActionCreators,dispatch)
    }
    
    export default connect(mapStateToProps,mapDispatchToProps)(ActiveCode)
