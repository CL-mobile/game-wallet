import React, { Component } from 'react';
import { View,ImageBackground,StatusBar,Image,ScrollView,SafeAreaView ,TouchableOpacity} from 'react-native';
import { Container, Header, Content, List, ListItem, Text, Body,Left,Right,Icon,Button, Col} from 'native-base';
import {Icons,Constants,Colors} from '@common'
class Transaction extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    data=[
      {
        id:1,
        date:'MON, Dec 20 ,2018',
        monfee:'$6,000.00',
        montitle:'Internet Access',
        monnote:'Monthly Subscription Fee',
        monsubfee:'35.45',
        buttonmon:'Bank Transfer',
        indication:'Today',
        todayfee:'$6,200.00',
        subindication:'Utilities',
        notesubin:'Partial Refund',
        feesub:'50.00',
        buttontoday:'Cash',
        subindication1:'Rental Car',
        notesubin1:'Trip To London',
        feesub1:'150.00',
        buttontoday1:'Credit Card',
        subindication2:'Gas/Fuel',
        notesubin2:"Janet's Car Fill Up",
        feesub2:'55.47',
        buttontoday2:'Cash',
       
      },
      {
        id:2,
        date:'MON, Dec 20 ,2018',
        monfee:'$4,972.14',
        montitle:'Home',
        monnote:'Monthly Rent',
        monsubfee:'45.00',
        buttonmon:'Bank Transfer',
        indication:'Today',
        todayfee:'$6,200.00',
        subindication:'Utilities',
        notesubin:'Partial Refund',
        feesub:'50.00',
        buttontoday:'Cash',
        subindication1:'Rental Car',
        notesubin1:'Trip To London',
        feesub1:'150.00',
        buttontoday1:'Credit Card',
        subindication2:'Gas/Fuel',
        notesubin2:"Janet's Car Fill Up",
        feesub2:'55.47',
        buttontoday2:'Cash',
      },
      {
        id:3,
        date:'MON, Dec 20 ,2018',
        monfee:'$4,972.14',
        montitle:'Home',
        monnote:'Monthly Rent',
        monsubfee:'45.00',
        buttonmon:'Bank Transfer',
        indication:'Today',
        todayfee:'$6,200.00',
        subindication:'Utilities',
        notesubin:'Partial Refund',
        feesub:'50.00',
        buttontoday:'Cash',
        subindication1:'Rental Car',
        notesubin1:'Trip To London',
        feesub1:'150.00',
        buttontoday1:'Credit Card',
        subindication2:'Gas/Fuel',
        notesubin2:"Janet's Car Fill Up",
        feesub2:'55.47',
        buttontoday2:'Cash',
      }
    ]
    return (
    <SafeAreaView style={{height:720,maxHeight:820,flex:1}}>
     <StatusBar
                translucent
                backgroundColor="transparent"
                barStyle="light-content"
            />
            
    <View style={{backgroundColor:'#1c1c1c',maxHeight:820,height:720}}>
        <Header androidStatusBarColor="#1c1c1c"  style={{backgroundColor:'#1c1c1c',marginTop:20,marginBottom:20}}>
        <Left style={{marginTop:20}}>
        <TouchableOpacity onPress={this.props.goBack}>
        <Image  source={Icons.Left} style={{width:20,height:20,tintColor:'white'}}/>
        </TouchableOpacity>
        </Left>
          <Body style={{marginTop:10,marginLeft:20}}>
            <Text style={{color:'white',fontSize:20}}>Asep Jumadi</Text>
          </Body>
        </Header>
        <ScrollView >
        
        {
          data.map((item,index)=>(
            <Content key={index} style={{flex:1}}>
          <List>
            <ListItem itemDivider style={{backgroundColor:'grey',opacity:0.50}}>
            <Left>
              <Text style={{color:'white',fontWeight:'bold'}}>{item.date}</Text>
            </Left>
            <Right>
              <Text  style={{color:Colors.Green,fontWeight:'bold'}}>{item.monfee}</Text>
            </Right>
            </ListItem>                    
            <ListItem style={{height:40,marginLeft:5}} noBorder={true} >
            <Body>
              <Text style={{color:'white'}}>{item.montitle}</Text>
              <Text note style={{color:'white',opacity:0.70}}> {item.monnote}</Text>
              </Body>
            <Right>
              <Text style={{color:'white'}}> {item.monsubfee}</Text>
            </Right>
            </ListItem>
            <ListItem style={{marginTop:-15}} noBorder={true}>
            <Left>
              <Image source={Icons.Redo} style={{width:20,height:20,tintColor:'green',marginRight:10}}/>
              <Text style={{color:'white',opacity:0.70,fontSize:10}}>Reccuring</Text>
            </Left>
            <Right>
            <View style={{width:85,paddingHorizontal:5,paddingVertical:5,borderBottomColor:'green',borderBottomWidth:2,borderTopColor:'green',borderTopWidth:2,borderLeftWidth:2,borderLeftColor:'green',borderRightColor:'green',borderRightWidth:2}}>
            <Text style={{fontSize:11,color:Colors.Green}}>{item.buttonmon}</Text>
            </View>
            </Right>
            </ListItem>
            <ListItem itemDivider style={{backgroundColor:'grey',opacity:0.50}}>
              <Left>
              <Text style={{color:'white',fontWeight:'bold'}}>{item.indication}</Text>
              </Left>
              <Right>
              <Text  style={{color:Colors.Green,fontWeight:'bold'}}>{item.todayfee}</Text>
            </Right>
            </ListItem>  
            <ListItem noBorder={true} style={{height:40,marginLeft:5}}>
            <Body>
              <Text style={{color:'white'}}>{item.subindication}</Text>
              <Text note style={{color:'white',opacity:0.70}}> {item.notesubin} </Text>
              </Body>
            <Right>
              <Text style={{color:'white'}}> {item.feesub}</Text>
            </Right>
            </ListItem>
            <ListItem style={{marginTop:-15}} noBorder={true}>
            <Left>
                <Image source={Icons.Fa} style={{width:20,height:20,tintColor:'green',marginRight:10}}/>
                <Text note style={{color:'white',opacity:0.70,fontSize:10}}>Once-Off</Text>
              </Left>
            <Right>
            <View style={{width:85,paddingHorizontal:5,paddingVertical:5,borderBottomColor:'green',borderBottomWidth:2,borderTopColor:'green',borderTopWidth:2,borderLeftWidth:2,borderLeftColor:'green',borderRightColor:'green',borderRightWidth:2}}>
            <Text style={{fontSize:11,color:Colors.Green,alignSelf:'center'}}>{item.buttontoday}</Text>
            </View>
            </Right>
            </ListItem>
            <ListItem style={{height:40,marginLeft:5}} noBorder={true}>
            <Body>
              <Text style={{color:'white'}}>{item.subindication1}</Text>
              <Text note style={{color:'white',opacity:0.70}}> {item.notesubin1} </Text>
              </Body>
            <Right>
              <Text style={{color:'white'}}> {item.feesub1}</Text>
            </Right>
            </ListItem>
            <ListItem style={{marginTop:-15}} noBorder={true}>
            <Left>
                <Image source={Icons.Fa} style={{width:20,height:20,tintColor:'green',marginRight:10}}/>
                <Text note style={{color:'white',opacity:0.70 ,fontSize:10}}>Once-Off</Text>
              </Left>
            <Right>
            <View style={{width:85,paddingHorizontal:5,paddingVertical:5,borderBottomColor:'green',borderBottomWidth:2,borderTopColor:'green',borderTopWidth:2,borderLeftWidth:2,borderLeftColor:'green',borderRightColor:'green',borderRightWidth:2}}>
            <Text  style={{fontSize:11,alignSelf:'center',color:Colors.Green}}>{item.buttontoday1}</Text>
            </View>
            </Right>
            </ListItem>
            <ListItem style={{height:40,marginLeft:5}} noBorder={true}>
            <Body>
              <Text style={{color:'white'}}>{item.subindication2}</Text>
              <Text note style={{color:'white',opacity:0.70}}> {item.notesubin2}</Text>
              </Body>
            <Right>
              <Text style={{color:'white'}}> {item.feesub2}</Text>
            </Right>
            </ListItem>
            <ListItem style={{marginTop:-15}} noBorder={true}>
            <Left>
                <Image source={Icons.Fa} style={{width:20,height:20,tintColor:'green',marginRight:10}}/>
                <Text note style={{color:'white',opacity:0.70 ,fontSize:10}}>Once-Off</Text>
              </Left>
            <Right>
            <View style={{width:85,paddingHorizontal:5,paddingVertical:5,borderBottomColor:'green',borderBottomWidth:2,borderTopColor:'green',borderTopWidth:2,borderLeftWidth:2,borderLeftColor:'green',borderRightColor:'green',borderRightWidth:2}}>
            <Text  style={{fontSize:11,alignSelf:'center',color:Colors.Green}}>{item.buttontoday2}</Text>
            </View>
            </Right>
            </ListItem>
          </List>
          </Content>
          ))
        }
        
        </ScrollView>
        </View>
        </SafeAreaView>
    );
  }
}

export default Transaction;