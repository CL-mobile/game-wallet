import {StyleSheet, Dimensions} from 'react-native'
import {robotoWeights,iOSColors} from 'react-native-typography'

export default StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#1c1c1c',
    paddingTop: 10,
  },
  loading:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'white'
  },
  content:{
    paddingBottom:10
  },
  topHeader :{
      ...robotoWeights.light,
      marginRight:18,
      color:'white',
      textAlign:'center',
      flex:1
  },
  formBox:{
    flex:1,
    flexDirection:'row',
    borderBottomWidth:.6,
    borderColor:'rgba(58,203,218,.1)',
    justifyContent:'space-between',
    alignItems:'center',
    paddingRight: 11,
    paddingVertical:5,
    marginLeft: 18,
    },
    menuBox:{
      flex:1,
      flexDirection:'row',
      borderBottomWidth:.6,
      borderColor:'rgba(58,203,218,.1)',
      justifyContent:'space-between',
      alignItems:'center',
      paddingRight: 11,
      paddingVertical:5,
      marginLeft: 18,
      },
    formLabel:{
        ...robotoWeights.light,
        flex:1,
        color:iOSColors.lightGray,
        fontSize:15
    },
    menuLabel:{
      ...robotoWeights.light,
      flex:1,
      color:iOSColors.lightGray,
      fontSize:15
  },
    rightIconMenu:{width:17,height:17, tintColor:iOSColors.midGray},
    formControl:{
      ...robotoWeights.light,
      paddingHorizontal:10,
      color:iOSColors.lightGray,
      fontSize:15 ,
      width:200,
      textAlign:"right",
      paddingTop:5
  },
  buttoBottom:{
    alignSelf:'flex-end',
    borderRadius:0,
    paddingHorizontal: 0,
    width:Dimensions.get('window').width
  },
  datePickerBox:{
    marginTop: 9,
    borderColor: '#FF5722',
    borderWidth: 0.5,
    padding: 0,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    height: 38,
    justifyContent:'center'
  },

  datePickerText: {
    fontSize: 14,
    marginLeft: 5,
    borderWidth: 0,
    color: '#000',

  },
})
