import React, { Component } from 'react';
import {
  View,
  Dimensions,
  Image,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  LayoutAnimation,
  ActivityIndicator,
  Linking,
  KeyboardAvoidingView,
  TextInput,
  DatePickerAndroid,
  DatePickerIOS,
  Platform,
  ToastAndroid
} from 'react-native'


import styles from './style'
import {SearchBar,Promotions,BrowserByCategory,Brands,Products, Layout, HeaderBar, NavBack,GradientButton} from '@components'
import {Icons,Constants,Colors,Utils} from '@common'
import GridView from 'react-native-gridview';
import { Container, Header, Content, Card, CardItem, Text, BodyTab, Tabs, Tab, ScrollableTab } from 'native-base';
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'
import { Tooltip } from 'react-native-elements';
import { DatePickerDialog } from 'react-native-datepicker-dialog'
import moment from 'moment';

class AccountSetting extends Component {

    constructor(props)
    {
        super(props)
        this.state = {
            fname:props.Profile.firstname,
            lname:props.Profile.lastname,
            bod:props.Profile.birthdate,
            DateText: props.Profile.birthdate,
            DateHolder: null,
            phonenum:this.props.Profile.phone_number
        }
    }

    /**
     * Textbox click listener
     */
    DatePickerMainFunctionCall = () => {
    
        let DateHolder = this.state.DateHolder;
    
        if(!DateHolder || DateHolder == null){
    
        DateHolder = new Date();
        this.setState({
            DateHolder: DateHolder
        });
        }
    
        //To open the dialog
        this.refs.DatePickerDialog.open({
    
        date: DateHolder,
    
        });
    
    }
    onPushProfile=()=>{
        let data={
            "firstname":this.state.fname,
            "lastname":this.state.lname,
            "birthdate":this.state.DateText,
            "phone_number":this.state.phonenum,
        }
        this.props.changeprofile(data,this.props.dataUser.token)
        
    }
    /**
     * Call back for dob date picked event
     *
     */
    onDatePickedFunction = (date) => {
        this.setState({
        dobDate: date,
        DateText: moment(date).format('DD-MMM-YYYY')
        });
    }
    render() {
        let {goBack, changePassword} = this.props
        console.log(this.props.Profile)
        return (
            <Layout>
                <HeaderBar left={<NavBack onPress={goBack}/>} center={<Text style={styles.topHeader }>Edit Profile</Text>}/>
                <ScrollView>
                    <View>
                        <KeyboardAvoidingView enabled={true}  keyboardVerticalOffset={10} behavior={'padding'} style={{marginVertical:20}}>
                            <View style={{marginVertical: 10}}>
                                <Form label={'First Name'} placeholderTextColor="#aaa" placeholder="Enter First Name" value={this.state.fname} onChangeText={(text) => this.setState({fname:text})} />
                                <Form label={'Last Name'} placeholderTextColor="#aaa" placeholder="Enter Last Name" value={this.state.lname} onChangeText={(text) => this.setState({lname:text})} />
                                <Form type="date" label={'Birth of Day'}  valueDate={this.state.DateText} onPress={this.DatePickerMainFunctionCall.bind(this)} />
                                <Form label={'Phone Number'}  value={this.state.phonenum} onChangeText={(text)=>this.setState({phonenum:text})}/>
                            </View>
                            <View style={{marginVertical: 10}}>
                                <Form label={'Username'}  value={this.props.Profile.username} editable={false} />
                                <Form label={'Email'}  value={this.props.Profile.email} editable={false} />
                            </View>
                            <View style={{marginVertical: 15}}>
                                <Menu label={"Change Password"} onPress={changePassword}
                                        icon={null}/>
                            </View>
                        </KeyboardAvoidingView>
                    </View>
                </ScrollView>
                <GradientButton colors={['rgba(63,154,255,.4)', 'rgba(60,122,191,.3)', 'rgba(32,65,138,.1)']}  style={styles.buttoBottom } title={'SAVE'} onPress={this.onPushProfile}/>
                
                {/* Place the dialog component at end of your views and assign the references, event handlers to it.*/}
                <DatePickerDialog ref="DatePickerDialog" onDatePicked={this.onDatePickedFunction.bind(this)} />
            </Layout>
        )
    }

    openDatePicker = () => {
        if(Platform.OS === "android"){
            try {
                const {action, year, month, day} = DatePickerAndroid.open({
                  // Use `new Date()` for current date.
                  // May 25 2020. Month 0 is January.
                  date: new Date(),
                  
                });

                if (action !== DatePickerAndroid.dateSetAction) {
                    // Selected year, month (0-11), day
                    console.log(month)
                  }
                
              } catch ({code, message}) {
                console.warn('Cannot open date picker', message);
              }
        }
    }
    componentWillReceiveProps(nextProps){
        if(nextProps.type==ActionTypes.POST_CHANGE_PROFILE_SUCCESS){
            ToastAndroid.show(nextProps.message,ToastAndroid.SHORT)
        }else if(nextProps.type==ActionTypes.POST_CHANGE_PROFILE_FAIL){
            ToastAndroid.show(nextProps.message,ToastAndroid.SHORT)
        }
    }

}

function mapStateToProps({authReducers,balanceInfoReducers,changeprofileReducers}){

    return {
      dataUser:authReducers.dataUser,
      Profile:balanceInfoReducers.Profile,
      type:changeprofileReducers.type,
      message:changeprofileReducers.message,
    }
  }
  
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch)
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(AccountSetting)

class Form extends Component{

    render(){
        const {label, type, valueDate, onPress} = this.props
        return(<View style={styles.formBox} >
            <View style={{flex:1,flexDirection:'row', justifyContent: 'space-between', alignItems:'center', }}>
                
                <Text style={styles.formLabel}>{label}</Text>
                {type == "date" ? 
                <TouchableOpacity onPress={onPress} style={{paddingRight:11, paddingVertical:15}}>
                    <Text style={styles.formLabel}>{valueDate}</Text>
                </TouchableOpacity>
                :
                <TextInput
                    {...this.props}
                    underlineColorAndroid="transparent"
                    style={styles.formControl}
                />
                }
            </View>
        </View>)
    }

}

class Menu extends Component{

    render(){
        const {icon,label, rightIcon, onPress} = this.props
        return(<TouchableOpacity style={styles.menuBox} onPress={onPress}>
            <View style={{flex:1,flexDirection:'row', justifyContent: 'space-between', alignItems:'center', paddingVertical:15}}>
                <Text style={styles.menuLabel}>{label}</Text>
                <Image style={styles.rightIconMenu} source={Icons.Right}/>
            </View>
        </TouchableOpacity>)
    }

}

Menu.defaultProps={
    rightIcon:true
}

Form.defaultProps = {
    isError:false,
    type:null,
    valueDate:""
}