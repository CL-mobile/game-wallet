import React, { Component } from 'react';
import {
  View,
  Dimensions,
  Image,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  LayoutAnimation,
  ActivityIndicator,
  Linking,
  KeyboardAvoidingView,
  TextInput,
  Alert,
  ToastAndroid
} from 'react-native'


import styles from './style'
import {SearchBar,Promotions,BrowserByCategory,Brands,Products, Layout, HeaderBar, NavBack, GradientButton} from '@components'
import {Icons,Constants,Colors,Utils} from '@common'
import GridView from 'react-native-gridview';
import { Container, Header, Content, Card, CardItem, Text, BodyTab, Tabs, Tab, ScrollableTab } from 'native-base';
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'

class ChangePassword extends Component {
    constructor(props) {
        super(props);
        this.state={
            oldpass:"",
            newpass:"",
            confrimpass:""
        };
        }
        onChangePass=()=>{
            let{oldpass,newpass,confrimpass}=this.state
            if(oldpass==""&&newpass==""&&confrimpass==""){
                ToastAndroid.show("The Old Password field is required.The New Password field is required.The Confirm Password field is required.",ToastAndroid.SHORT)
            }else if(oldpass!=""&&newpass!=confrimpass){
                ToastAndroid.show("The Confirm Password field does not match the New Password field",ToastAndroid.SHORT)
            }else if(oldpass!=0&&newpass!=""&&confrimpass==""){
                ToastAndroid.show("The Confirm Password field is required.",ToastAndroid.SHORT)
            }else if (oldpass!=""&&newpass==""&&confrimpass==""){
                ToastAndroid.show("The New Password field is required.The Confirm Password field is required",ToastAndroid.SHORT)
            }else if (oldpass!=""&&newpass==""&&confrimpass!=""){
                ToastAndroid.show("The New Password field is required",ToastAndroid.SHORT)
            }else {
                let data={
                    "oldPass":this.state.oldpass,
                    "newPass":this.state.newpass, 
                    "confirmPass":this.state.confrimpass
                }
                this.props.changepassword(data,this.props.dataUser.token)
            }
            // console.log(this.props.dataUser.token)
            
           
        }
    render() {
        let {goBack} = this.props
        
        if(__DEV__)
        return (
            <Layout>
                <HeaderBar left={<NavBack onPress={goBack}/>} center={<Text style={styles.topHeader }>Change Password</Text>}/>
                <ScrollView>
                    <View>
                        <KeyboardAvoidingView enabled={true}  keyboardVerticalOffset={10} behavior={'padding'} style={{marginVertical:20}}>
                            <View style={{marginVertical: 10}}>
                                <Form label={'Old Password'} placeholderTextColor="#aaa" placeholder="Enter" secureTextEntry={true} value={this.state.oldpass} onChangeText={(text)=>this.setState({oldpass:text})}/>
                            </View>
                            <View style={{marginVertical: 10}}>
                                <Form label={'New Password'} placeholderTextColor="#aaa" placeholder="Enter" secureTextEntry={true} value={this.state.newpass} onChangeText={(text)=>this.setState({newpass:text})}/>
                                <Form label={'Confirm New Password'} placeholderTextColor="#aaa" placeholder="Enter" secureTextEntry={true} value={this.state.confrimpass} onChangeText={(text)=>this.setState({confrimpass:text})}/>
                            </View>
                        </KeyboardAvoidingView>
                    </View>
                </ScrollView>
                <GradientButton colors={['rgba(63,154,255,.4)', 'rgba(60,122,191,.3)', 'rgba(32,65,138,.1)']}  style={styles.buttoBottom } title={'SAVE'} onPress={this.onChangePass}/>
            </Layout>
        )
    }
    componentWillReceiveProps(nextProps){
        console.log(nextProps.message)
        if(nextProps.datapassType==ActionTypes.POST_CHANGE_PASS_SUCCESS){
            Alert.alert(nextProps.message)
            console.log(nextProps.datapassType)
            this.setState({
                oldpass:"",newpass:"",confrimpass:""
            })
        }
    else if(nextProps.datapassType == ActionTypes.POST_CHANGE_PASS_FAIL){
        Alert.alert(nextProps.message)
    }
    
    }

}
function mapStateToProps({changepassReducers,authReducers}){
    return {
      dataUser:authReducers.dataUser,
      datapassType:changepassReducers.type,
      message:changepassReducers.message,
      isRequesting:changepassReducers.type==ActionTypes.POST_CHANGE_PASS_PENDING
    }
  }
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch)
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(ChangePassword)

class Form extends Component{

    render(){
        const {label, type, valueDate, onPress} = this.props
        return(<View style={styles.formBox} >
            <View style={{flex:1,flexDirection:'row', justifyContent: 'space-between', alignItems:'center', }}>
                
                <Text style={styles.formLabel}>{label}</Text>
                {type == "date" ? 
                <TouchableOpacity onPress={onPress} style={{paddingRight:11, paddingVertical:15}}>
                    <Text style={styles.formLabel}>{valueDate}</Text>
                </TouchableOpacity>
                :
                <TextInput
                    {...this.props}
                    underlineColorAndroid="transparent"
                    style={styles.formControl}
                />
                }
            </View>
        </View>)
    }

}

Form.defaultProps = {
    isError:false,
    type:null,
    valueDate:""
}