import React from 'react'
import {
  View,
  Dimensions,
  Image,
  SafeAreaView,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  LayoutAnimation,
  ActivityIndicator
} from 'react-native'


import styles from './style'
import {SearchBar,Promotions,BrowserByCategory,Brands,Products} from '@components'

import { Container, Header, Content, Card, CardItem, Text, Body } from 'native-base';
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'

import {Config,Constants, Icons} from '@common'

const screenWidth = Dimensions.get('window').width

const data = [ 50, 10, 40, 95, -4, -24, 85, 91, 35, 53, -53, 24, 50, -20, -80 ]

class Balance extends React.Component {
  

  render() {
    return (
        <SafeAreaView style={styles.container}>
            <StatusBar
                translucent
                backgroundColor="transparent"
                barStyle="light-content"
            />
            <View style={{flexDirection:'row', justifyContent:'space-between', padding:16, backgroundColor:'#1c1c1c', alignItems:'center'}}>
                <TouchableOpacity>
                    <Image
                    source={Icons.Drawer}
                    style={{height:25, width:25, tintColor:'white'}}
                    />
                </TouchableOpacity>
                <Text style={{fontWeight:'500', color:'white'}}>Your Balance</Text>
                <TouchableOpacity>
                    <Image
                    source={Icons.NotificationActive}
                    style={{height:25, width:21}}
                    />
                </TouchableOpacity>
            </View>

            <ScrollView showsVerticalScrollIndicator={false} >
                <View style={{marginHorizontal:16, marginBottom:16}}>
                    <Text style={{fontSize:25, fontWeight:'500', textAlign:'center', color:'white'}}>$ 1.085.628</Text>
                    
                </View>
            </ScrollView>
        </SafeAreaView>
    );
  }
}

export default Balance;
