import {StyleSheet, Dimensions} from 'react-native'

export default StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#1c1c1c',
    paddingTop: 10,
  },
  loading:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'white'
  },
  content:{
    paddingBottom:10
  },
})
