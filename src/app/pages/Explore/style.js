import {StyleSheet} from 'react-native'
import {robotoWeights, iOSColors} from 'react-native-typography'

export default StyleSheet.create({
    SectionHeader : {
        paddingHorizontal: 10,
        flexDirection:'row',
        justifyContent: 'space-between',
        paddingVertical: 8
    },
    SectionTitle:{
        ...robotoWeights.bold,
        fontSize:24,
        color:iOSColors.lightGray
    },
    HorizontalItem:{
        marginVertical: 5
    }


})