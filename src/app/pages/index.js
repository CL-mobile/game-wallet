import _Home from './Home'
export const Home = _Home

import _ActiveCode from './ActiveCode'
export const ActiveCode=_ActiveCode

import _ResetPassword  from './ResetPassword'
export const ResetPassword =_ResetPassword

import _ForgotPass from './ForgotPass'
export const ForgotPass=_ForgotPass

import _TransactionHistory from './TransactionHistory'
export const TransactionHistory = _TransactionHistory

import _Register from './Register'
export const Register =_Register

import _Login from './Login'
export const Login =_Login

import _Balance from './Balance'
export const Balance = _Balance

import _GameProduct from './GameProduct'
export const GameProduct =_GameProduct

import _Account from './Account'
export const Account = _Account

import _Deposit from './Deposit'
export const Deposit =_Deposit

import _Transaction from './Transaction'
export const Transaction =_Transaction

import _Deals from './Deals'
export const Deals = _Deals

import _Search from './Search'
export const Search = _Search

import _Carts from './Carts'
export const Carts = _Carts

import _MyProfile from './MyProfile'
export const MyProfile = _MyProfile

import _MyWishList from './MyWishList'
export const MyWishList = _MyWishList

import _Languages from './Languages'
export const Languages = _Languages

import _MyAddress from './MyAddress'
export const MyAddress = _MyAddress

import _Launch from './Launch'
export const Launch = _Launch

import _Detail from './Detail'
export const Detail = _Detail

import _ProductsByCategory from './ProductsByCategory'
export const ProductsByCategory = _ProductsByCategory

import _SetLanguage from './SetLanguage'
export const SetLanguage = _SetLanguage

import _SignIn from './SignIn'
export const SignIn = _SignIn

import _SignUp from './SignUp'
export const SignUp = _SignUp

import _Feedback from './Feedback'
export const Feedback = _Feedback

import _ShippingInfo from './ShippingInfo'
export const ShippingInfo = _ShippingInfo

import _ShippingAddress from './ShippingAddress'
export const ShippingAddress = _ShippingAddress



import _AddAddress from './AddAddress'
export const AddAddress = _AddAddress

import _MyOrders from './MyOrders'
export const MyOrders = _MyOrders


//DepositFlow
import _FirstStep from './Deposit/FirstStep'
export const DepositFirstStep = _FirstStep

import _SecondStep from './Deposit/SecondStep'
export const DepositSecondStep = _SecondStep

import _ThirdStep from './Deposit/ThirdStep'
export const DepositThirdStep = _ThirdStep

import _ConfirmStep from './Deposit/ConfirmStep'
export const DepositConfirmStep= _ConfirmStep
//Explore Page
import _Explore from './Explore'
export const ExplorePage = _Explore

//WalletDetail
import _WalletDetail from './WalletDetail'
export const WalletDetail = _WalletDetail

//TRANSFER
import _WalletTransfer from './WalletTransfer'
export const WalletTransfer = _WalletTransfer

//Account Setting
import _AccountSetting from './AccountSetting'
export const AccountSetting = _AccountSetting

//Edit Profile
import _EditProfile from './EditProfile'
export const EditProfile = _EditProfile

//Change Password
import _ChangePassword from './ChangePassword'
export const ChangePassword = _ChangePassword

//Game
import _Game from './Game'
export const Game = _Game

//Game
import _GameAll from './GameAll'
export const GameAll = _GameAll