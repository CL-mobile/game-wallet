import React, { Component } from 'react';
import { View, Text ,StatusBar,ImageBackground,Image,TextInput,Alert,Keyboard,TouchableOpacity,ScrollView,Dimensions,KeyboardAvoidingView,ToastAndroid,AlertIOS,Platform,IOS} from 'react-native';
import {Icons,Constants} from '@common'
import {Button} from '@components'
import { Container, Header, Content, Item, Input, Icon } from 'native-base';
import styles from './style'
import LinearGradient from 'react-native-linear-gradient';
import {GradientButton} from '@components'
import PhoneInput from 'react-native-phone-input'
import { robotoWeights,iOSColors } from 'react-native-typography'
import CountryPicker from 'react-native-country-picker-modal';
import axios from 'axios'
import {NavigationActions, StackActions} from 'react-navigation'
 class Register extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
        cca2: "ID",
        fname:"",
        lname:"",
        email:"",
        username:"",
        password:"",
        copass:"",
        bod:"",
        pnumber:"",
        countryid:"id",
        valid: "",
        type: "",
        value: " ",
        pickerData:"",
        status1:true,
        status2:true,
        underlineEmail:"transparent",
        underlineUser:"transparent",
        underlineBirth:"transparent",
        underlinePass:"transparent",
        underlineCopass:"transparent",
        underlinelname:"transparent",
        underlinefname:"transparent",
        boxEmail:"white",
        boxUser:"white",
        boxBirth:"white",
        boxPass:"white",
        boxCopass:"white",
        boxlname:"white",
        boxfname:"white",
        validEmail:"none",
        validBirth:"none",
        validPassword:"none",
        validUsername:"none",
        validCopass:"none",
        validlname:"none",
        validfname:"none"
    

    };
    this.focusNextField = this.focusNextField.bind(this);
    this.inputs = {};
    this.id={};
    this.onPressFlag = this.onPressFlag.bind(this);
    this.selectCountry = this.selectCountry.bind(this);
    this.updateInfo = this.updateInfo.bind(this);
    // this.OnDataSendWithAxios=this.OnDataSendWithAxios.bind(this);
  }
//   setSortActive =(active)=> {
//     this.setState({ unerlineemail: {
//         ...this.state.underlineemail,
//         active
//     }})
//     }
focusNextField(id) {
    this.inputs[id].focus();
    }
    focusNextField1(id) {
        this.id.focus();
        }
_focusNextField(nextField) {
    this.refs[nextField].focus()
    }
  ShowHidePassword = () =>{

    if(this.state.status1 == true)
    {
        this.setState({status1: false})
    }
    else
    {
        this.setState({status1: true})
    }
    }
    ShowHideCopass = () =>{

        if(this.state.status2 == true)
        {
            this.setState({status2: false})
        }
        else
        {
            this.setState({status2: true})
        }
        }
    validateEmail = (text) => {
        console.log(text);
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/ ;
        if(reg.test(text) === false)
        {
        // if(Platform.OS==='android'){
        //     ToastAndroid.show("Email is Not Correct",ToastAndroid.SHORT)
        // }else{
        //     AlertIOS.alert("Email is Not Correct")
        // }
        
        console.log("Email is Not Correct");
        this.setState({email:text,boxEmail:"#DA4F52",validEmail:"false"})
        return false;
          }
        else {
            // if(Platform.OS==='android'){
            //     ToastAndroid.show("Email is Correct",ToastAndroid.SHORT)
            // }else{
            //     AlertIOS.alert("Email is Correct")
            // }
          this.setState({email:text,boxEmail:"white",validEmail:"true"})
          console.log("Email is Correct");
        }
        }
        validateFname = (text) => {
            console.log(text);
            let reg = /[a-zA-Z]{2,}/ ;
            if(reg.test(text) === false)
            {
            // if(Platform.OS==='android'){
            //     ToastAndroid.show("Firts name is Not Correct",ToastAndroid.SHORT)
            // }else{
            //     AlertIOS.alert("Firts name is Not Correct")
            // }
            
            console.log("Firts name is Not Correct");
            this.setState({fname:text,boxfname:"#DA4F52",validfname:"false"})
            return false;
              }
            else {
                // if(Platform.OS==='android'){
                //     ToastAndroid.show("firts name is Correct",ToastAndroid.SHORT)
                // }else{
                //     AlertIOS.alert("firts name is Correct")
                // }
              this.setState({fname:text,boxfname:"white",validfname:"true"})
              console.log("firts name is Correct");
            }
            }//1
            validateLname = (text) => {
                console.log(text);
                let reg = /[a-zA-Z]{2,}/ ;
                if(reg.test(text) === false)
                {
                // if(Platform.OS==='android'){
                //     ToastAndroid.show("Last name is Not Correct",ToastAndroid.SHORT)
                // }else{
                //     AlertIOS.alert("Last name is Not Correct")
                // }
                
                console.log("Last name is Not Correct");
                this.setState({lname:text,boxlname:"#DA4F52",validlname:"false"})
                return false;
                  }
                else {
                    // if(Platform.OS==='android'){
                    //     ToastAndroid.show("Last name is Correct",ToastAndroid.SHORT)
                    // }else{
                    //     AlertIOS.alert("Last name is Correct")
                    // }
                  this.setState({lname:text,boxlname:"white",validlname:"true"})
                  console.log("Last name is Correct");
                }
                }
        validateUserName = (text) => {
            console.log(text);
            let reg = /^[a-z0-9]{6,15}$/ ;
            if(reg.test(text) === false)
            {
            // if(Platform.OS==='android'){
            //     ToastAndroid.show("Username Invalid,must  min 6 character contain number and character ",ToastAndroid.SHORT)
            // }else{
            //     AlertIOS.alert("Username Invalid,must  min 6 character contain number and character")
            // }
            
            console.log("Username is Not Correct");
            this.setState({username:text,boxUser:"#DA4F52",validUsername:"false"})
            return false;
              }
            else {
                // if(Platform.OS==='android'){
                //     ToastAndroid.show("Username is Correct",ToastAndroid.SHORT)
                // }else{
                //     AlertIOS.alert("Username is Correct")
                // }
              this.setState({username:text,boxUser:"white",validUsername:"true"})
              console.log("Username is Correct");
            }
            }
            validateBirth = (text) => {
                console.log(text);
                let reg = /([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))/ ;
                if(reg.test(text) === false)
                {
                // if(Platform.OS==='android'){
                //     ToastAndroid.show("Birtday invalid,must YYYY-MM-DD",ToastAndroid.SHORT)
                // }else{
                //     AlertIOS.alert("Birtday invalid,must YYYY-MM-DD")
                // }
                
                console.log("Birthday is Not Correct");
                this.setState({bod:text,boxBirth:"#DA4F52",validBirth:"false"})
                return false;
                  }
                else {
                    // if(Platform.OS==='android'){
                    //     ToastAndroid.show("Birthday is Correct",ToastAndroid.SHORT)
                    // }else{
                    //     AlertIOS.alert("Birthday is Correct")
                    // }
                  this.setState({bod:text,boxBirth:"white",validBirth:"true"})
                  console.log("Birthday is Correct");
                }
                }
                validatePassword = (text) => {
                    console.log(text);
                    let reg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/ ;
                    if(reg.test(text) === false)
                    {
                    // if(Platform.OS==='android'){
                    //     ToastAndroid.show("password invalid,must 8 character with number and character ",ToastAndroid.SHORT)
                    // }else{
                    //     AlertIOS.alert("password invalid,must 8 character with number and character ")
                    // }
                    
                    console.log("Password is Not Correct");
                    this.setState({password:text,boxPass:"#DA4F52",validPassword:"false"})
                    return false;
                      }
                    else {
                        // if(Platform.OS==='android'){
                        //     ToastAndroid.show("Password is Correct",ToastAndroid.SHORT)
                        // }else{
                        //     AlertIOS.alert("Password is Correct")
                        // }
                      this.setState({password:text,boxPass:"white",validPassword:"true"})
                      console.log("Password is Correct");
                    }
                    }
                    validateCopass= (text) => {
                        console.log(text);
                        let reg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/ ;
                        if(reg.test(text) === false)
                        {
                        // if(Platform.OS==='android'){
                        //     ToastAndroid.show("confrim password invalid,must 8 character with number and character ",ToastAndroid.SHORT)
                        // }else{
                        //     AlertIOS.alert("confrim password invalid,must 8 character with number and character ")
                        // }
                        
                        console.log("confrim password is Not Correct");
                        this.setState({copass:text,boxCopass:"#DA4F50",validCopass:"false"})
                        return false;
                          }
                        else {
                            // if(Platform.OS==='android'){
                            //     ToastAndroid.show("confrim password is Correct",ToastAndroid.SHORT)
                            // }else{
                            //     AlertIOS.alert("confrim password is Correct")
                            // }
                          this.setState({copass:text,boxCopass:"white",validCopass:"true"})
                          console.log("confrim password is Correct");
                        }
                        }
//   OnSendData(){
//     var result = fetch(url, {
//         method: 'POST',
//         headers: {
//             'Accept': 'application/json',
//             'Content-Type': 'text/html',
//         },
//         body: JSON.stringify({ 
//             'data': JSON.stringify(param) 
//         })
//         })
//         .then(response => checkStatus(response))
//         .then(response => response.json())
//         .catch(e => { throw e; });

//         return result;
// }
updateInfo() {
    this.setState({
    valid: this.phone.isValidNumber(),
    type: this.phone.getNumberType(),
    value: this.phone.getValue(),
    country:this.phone.getCountryCode(),
    });
}

OnDataSendWithAxios = () => {
    if( this.state.validfname=="false"
    && this.state.validlname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="true"
    && this.state.validUsername=="true"
    && this.state.validPassword=="true"
    &&this.state.validCopass=="true"
    &&this.state.copass==this.state.password){
        console.log("firts name not valid ,please fill at least 2 character and no number");
        ToastAndroid.show("firts name not valid ,please fill at least 2 character and no number",ToastAndroid.SHORT)
    }else if( this.state.validfname=="false"
    && this.state.validlname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="true"
    && this.state.validUsername=="true"
    && this.state.validPassword=="true"
    &&this.state.validCopass=="true"
    &&this.state.copass!=this.state.password){
        console.log("please fill the firts name and its not match password")
        ToastAndroid.show("please fill the firts name and its not match password",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="false"
    &&this.state.validEmail=="false"
    && this.state.validBirth=="false"
    && this.state.validUsername=="false"
    && this.state.validPassword=="false"
    &&this.state.validCopass=="false"
    ){
        ToastAndroid.show("please fill  last name,email,birthday,username,password,confrim password with correct value",ToastAndroid.SHORT)
        console.log("please fill  last name,email,birthday,username,password,confrim password with correct value")
    }
    else if(this.state.validfname=="none"
    && this.state.validlname=="none"
    &&this.state.validEmail=="none"
    && this.state.validBirth=="none"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        console.log("please fill  all field")
        this.setState({boxlname:"yellow",boxUser:"yellow",boxfname:"yellow",boxEmail:"yellow",boxBirth:"yellow",boxPass:"yellow",boxCopass:"yellow"})
        ToastAndroid.show("please fill  all field",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="none"
    &&this.state.validEmail=="none"
    && this.state.validBirth=="none"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        console.log("please fill  last name,email,birthday,username,password,confrim password  field")
        ToastAndroid.show("please fill  last name,email,birthday,username,password,confrim password",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="true"
    &&this.state.validEmail=="none"
    && this.state.validBirth=="none"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        console.log("please fill  email,birthday,username,password,confrim password  field")
        ToastAndroid.show("please fill  email,birthday,username,password,confrim password",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="none"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        console.log("please fill  birthday,username,password,confrim password  field")
        ToastAndroid.show("please fill  birthday,username,password,confrim password",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="true"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        console.log("please fill  username,password,confrim password  field")
        ToastAndroid.show("please fill  username,password,confrim password",ToastAndroid.SHORT)
        this.setState({boxUser:"yellow",boxPass:"yellow",boxCopass:"yellow"})
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="true"
    && this.state.validUsername=="true"
    && this.state.validPassword=="false"
    &&this.state.validCopass=="none"){
        console.log("password are invalid,please fill  confrim password  field")
        ToastAndroid.show("password are invalid,please fill  username,password,confrim password",ToastAndroid.SHORT)
        this.setState({boxCopass:"yellow"})
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="false"
    &&this.state.validEmail=="none"
    && this.state.validBirth=="none"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        this.setState({boxEmail:"yellow",boxBirth:"yellow",boxUser:"yellow",boxPass:"yellow",boxCopass:"yellow"})
        console.log("last name not valid must at least 2 character except number and please fill other field or form")
        ToastAndroid.show("last name not valid must at least 2 character except number  and please fill other field or form",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="true"
    &&this.state.validEmail=="false"
    && this.state.validBirth=="none"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        this.setState({boxBirth:"yellow",boxUser:"yellow",boxPass:"yellow",boxCopass:"yellow"})
        console.log("email not valid must exampe@emailprovider.com  and please fill other field or form")
        ToastAndroid.show("email not valid must exampe@emailprovider.com   and please fill other field or form",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="false"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none" 
    &&this.state.validCopass=="none"){
        this.setState({boxUser:"yellow",boxPass:"yellow",boxCopass:"yellow"})
        console.log("Birthday not valid must YYYY-MM-DD and please fill other field or form")
        ToastAndroid.show("Birthday not valid must YYYY-MM-DD and please fill other field or form",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="true"
    && this.state.validUsername=="false"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        this.setState({boxPass:"yellow",boxCopass:"yellow"})
        console.log("username not valid must at least 6 character and please fill other field or form")
        ToastAndroid.show("username not valid must ust at least 6 character   and please fill other field or form",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="false"
    && this.state.validlname=="none"
    &&this.state.validEmail=="none"
    && this.state.validBirth=="none"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        this.setState({boxlname:"yellow",boxEmail:"yellow",boxUser:"yellow",boxBirth:"yellow",boxPass:"yellow",boxCopass:"yellow"})
        console.log("firts name not valid ,firts name must  least 2 character except number and please fill other field or form")
        ToastAndroid.show("firts name not valid ,firts name must  least 2 character except number  and please fill other field or form",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="false"
    && this.state.validlname=="false"
    &&this.state.validEmail=="none"
    && this.state.validBirth=="none"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        this.setState({boxEmail:"yellow",boxBirth:"yellow",boxUser:"yellow",boxPass:"yellow",boxCopass:"yellow"})
        console.log("firts name and last name not valid  ,firts name and last name must  least 2 character except number;please fill other field or form")
        ToastAndroid.show("firts name not valid ,please fill other field or form",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="false"
    && this.state.validlname=="false"
    &&this.state.validEmail=="false"
    && this.state.validBirth=="none"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        this.setState({boxBirth:"yellow",boxUser:"yellow",boxPass:"yellow",boxCopass:"yellow"})
        console.log("firts name, last name and email not valid ;please fill a correct lastname,firtsname,email and  other field or form")
        ToastAndroid.show("firts name, last name and email not valid ;please fill a correct email and  other field or form",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="false"
    && this.state.validlname=="false"
    &&this.state.validEmail=="false"
    && this.state.validBirth=="false"
    && this.state.validUsername=="none"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        this.setState({boxUser:"yellow",boxUser:"yellow",boxPass:"yellow",boxCopass:"yellow"})
        console.log("firts name, last name , email,birthday not valid ;please fill a correct lastname,firtsname,birthday and  other field ")
        ToastAndroid.show("firts name, last name and email not valid ;please fill a correct email and  other field or form",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="false"
    && this.state.validlname=="false"
    &&this.state.validEmail=="false"
    && this.state.validBirth=="false"
    && this.state.validUsername=="false"
    && this.state.validPassword=="none"
    &&this.state.validCopass=="none"){
        this.setState({boxPass:"yellow",boxCopass:"yellow"})
        console.log("firts name, last name , email,username not valid ;please fill a correct lastname,firtsname,email,birthday and  other field ")
        ToastAndroid.show("firts name, last name and email not valid ;please fill a correct email and  other field or form",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="false"
    && this.state.validlname=="false"
    &&this.state.validEmail=="false"
    && this.state.validBirth=="false"
    && this.state.validUsername=="false"
    && this.state.validPassword=="false"
    &&this.state.validCopass=="false"){
        console.log(" all field that you fill  are wrong")
        ToastAndroid.show("all field that you fill  are wrong",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="false"
    &&this.state.validEmail=="false"
    && this.state.validBirth=="false"
    && this.state.validUsername=="false"
    && this.state.validPassword=="false"
    &&this.state.validCopass=="false"){
        console.log("last name ,email,birthday,username,password,comfrim password is invalid tfffff")
        ToastAndroid.show("last name ,email,birthday,username,password,comfrim password is invalid",ToastAndroid.SHORT)
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="true"
    &&this.state.validEmail=="false"
    && this.state.validBirth=="false"
    && this.state.validUsername=="false"
    && this.state.validPassword=="false"
    &&this.state.validCopass=="false"){
        ToastAndroid.show("please fill the correct  email,birthday,username,password,confrim password",ToastAndroid.SHORT)
        console.log("please fill the correct email,birthday,username,password,confrim password 1")
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="false"
    && this.state.validUsername=="false"
    && this.state.validPassword=="false"
    &&this.state.validCopass=="false"){
        ToastAndroid.show("please fill the correct birthday,username,password,confrim password",ToastAndroid.SHORT)
        console.log("please fill fill the correct  birthday,username,password,confrim password 1")
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="true"
    && this.state.validUsername=="false"
    && this.state.validPassword=="false"
    &&this.state.validCopass=="false"){
        ToastAndroid.show("please fill the correct username,password,confrim password",ToastAndroid.SHORT)
        console.log("please fill the correct username,password,confrim password 1")
    }
    else if(this.state.validfname=="true"
    && this.state.validlname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="true"
    && this.state.validUsername=="true"
    && this.state.validPassword=="false"
    &&this.state.validCopass=="false"){
        ToastAndroid.show("please fill the valid  password and confrim password ",ToastAndroid.SHORT)
        console.log("please fill the valid password and confrim password ")
    }
    else if(this.state.validlname=="false"
    && this.state.validfname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="true"
    && this.state.validUsername=="true"
    && this.state.validPassword=="true"
    &&this.state.validCopass=="true"
    &&this.state.copass==this.state.password){
        console.log("last name that you fill are wrong,please fill last name until correct");
        ToastAndroid.show("last name that you fill are wrong,please fill last name until correct",ToastAndroid.SHORT)
    }
    else if(this.state.validEmail=="false"
    && this.state.validlname=="true"
    && this.state.validfname=="true"
    && this.state.validBirth=="true"
    && this.state.validUsername=="true"
    && this.state.validPassword=="true"
    &&this.state.validCopass=="true"
    &&this.state.copass==this.state.password){
        ToastAndroid.show("email is not valid,please email until correct",ToastAndroid.SHORT)
        console.log("email is not valid,please email until correct");
    }else if(this.state.validBirth=="false"
    && this.state.validlname=="true"
    && this.state.validfname=="true"
    &&this.state.validEmail=="true"
    && this.state.validUsername=="true"
    && this.state.validPassword=="true"
    &&this.state.validCopass=="true"
    &&this.state.copass==this.state.password){
        ToastAndroid.show("Birthday of date are invalid,please fill  Birthday with format YYYY-MM-DD",ToastAndroid.SHORT)
        console.log("please fill Birthday")
    }else if(this.state.validUsername=="false"
    && this.state.validlname=="true"
    && this.state.validfname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="true"
    && this.state.validPassword=="true"
    &&this.state.validCopass=="true"
    &&this.state.copass==this.state.password){
        ToastAndroid.show("username are invalid,please fill username until correct",ToastAndroid.SHORT)
        console.log("username are invalid,please fill username until correct ")
    }else if(this.state.validPassword=="false"
    &&this.state.validCopass=="false"
    &&this.state.validlname=="true"
    && this.state.validfname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="true"
    && this.state.validUsername=="true"
    &&this.state.password!=this.state.copass){
        ToastAndroid.show(" password and confrim password are wrong",ToastAndroid.SHORT)
console.log("password and confrim password are wrong")
    }else if (this.state.password!=this.state.copass
    && this.state.validlname=="true"
    && this.state.validUsername=="true"
    && this.state.validfname=="true"
    &&this.state.validEmail=="true"
    && this.state.validBirth=="true"
    && this.state.validPassword=="true"
    &&this.state.validCopass=="true"){
        ToastAndroid.show("password and confrim password not match",ToastAndroid.SHORT)
        console.log("password and confrim password not match")
    }else if (this.state.password==this.state.copass
        && this.state.validlname=="true"
        && this.state.validUsername=="true"
        && this.state.validfname=="true"
        &&this.state.validEmail=="true"
        && this.state.validBirth=="true"
        && this.state.validPassword=="false"
        &&this.state.validCopass=="false"){
            ToastAndroid.show("password and confrim password wrong",ToastAndroid.SHORT)
            console.log("password and confrim password wrong")
        }
        else if (this.state.password==this.state.copass
            && this.state.validlname=="true"
            && this.state.validUsername=="true"
            && this.state.validfname=="true"
            &&this.state.validEmail=="true"
            && this.state.validBirth=="true"
            && this.state.validPassword=="false"
            &&this.state.validCopass=="false"
            &&this.state.validCopass!=this.state.password){
                ToastAndroid.show("password and confrim password wrong",ToastAndroid.SHORT)
                console.log("password and confrim password wrong")
            }
        else if (this.state.password==this.state.copass
            && this.state.validlname=="true"
            && this.state.validUsername=="true"
            && this.state.validfname=="true"
            &&this.state.validEmail=="true"
            && this.state.validBirth=="true"
            && this.state.validPassword=="none"
            &&this.state.validCopass=="none"){
                ToastAndroid.show("please fill password and confrim password ",ToastAndroid.SHORT)
                console.log("please fill password and confrim password wrong")
                this.setState({boxPass:"yellow",boxCopass:"yellow"})
            }
            else if (this.state.password==this.state.copass
                && this.state.validlname=="true"
                && this.state.validUsername=="true"
                && this.state.validfname=="true"
                &&this.state.validEmail=="true"
                && this.state.validBirth=="true"
                && this.state.validPassword=="true"
                &&this.state.validCopass=="none"){
                    ToastAndroid.show("please fill  confrim password ",ToastAndroid.SHORT)
                    console.log("please fill confrim password ")
                    this.setState({boxCopass:"yellow"})
                }
                else if (this.state.password==this.state.copass
                    && this.state.validlname=="true"
                    && this.state.validUsername=="true"
                    && this.state.validfname=="true"
                    &&this.state.validEmail=="true"
                    && this.state.validBirth=="true"
                    && this.state.validPassword=="none"
                    &&this.state.validCopass=="false"){
                        this.setState({boxPass:"yellow"})
                        ToastAndroid.show("your confrim password are wrong and please fill password  ",ToastAndroid.SHORT)
                        console.log("your confrim password are wrong and please fill password  ")
                    }else if(this.state.password==this.state.copass
                        && this.state.validlname=="true"
                        && this.state.validUsername=="true"
                        && this.state.validfname=="true"
                        &&this.state.validEmail=="true"
                        && this.state.validBirth=="true"
                        && this.state.validPassword=="true"
                        &&this.state.validCopass=="true"){
            const API='fbca3609-b742-4d4c-b89e-cfdb55a91efa';
            var url='http://35.185.188.21/web-ewallet/api/index.php/auth/register';
            var params = {
                "email":this.state.email,
                "username" : this.state.username,
                "password" : this.state.password,
                "firstname" : this.state.fname,
                "lastname":this.state.lname,
                "country":this.state.countryid,
                "birthdate":this.state.bod,
                "phone_number":this.phone.getValue(), 
                "confirm_password":this.state.copass,
            };
    
            axios.post(url, params, {
            headers: {
                    'content-type': 'application/json',
                    'API-KEY':API,
            },
            }).then((response) => {
            console.log(response)
            console.log(response.data)
            if(response.data.status===true){
                ToastAndroid.show(response.data.message,ToastAndroid.SHORT)
                this.setState({
                    email:"",username:"",password:"",fname:"",lname:"",countyid:"",bod:"",copass:""
                })
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({routeName:Constants.Screen.Login})],
                });
                this.props.navigation.dispatch(resetAction);
            }else{ 
                ToastAndroid.show(response.data.message,ToastAndroid.SHORT)
                // Alert.alert(response.data.message)
            
            }
                
    
            })
            .catch((error) => {
                // console.log(error)
            })
        
        }else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"){
            ToastAndroid.show("last name,birthday,username,comfrim password is invalid",ToastAndroid.LONG)
            console.log("TFTFTF")
        }else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"){
            ToastAndroid.show("last name,birthday,comfrim password is invalid",ToastAndroid.LONG)
            console.log("TFTFTF")
        
        }else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"){
console.log("valid email,email,pass...FFTFTF0")
ToastAndroid.show("firts name ,last name ,birthday,username,and confrim password is invalid",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"){
console.log("valid email,email,pass...FFTFTF1")
ToastAndroid.show("firts name ,last name ,birthday,and confrim password is invalid",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            ToastAndroid.show("last name ,username and birthday is invalid",ToastAndroid.SHORT)
console.log("valid fname,email,pass,copass ....TFTFTT0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            ToastAndroid.show("last name ,birthday is invalid",ToastAndroid.SHORT)
console.log("valid fname,email,pass,copass ....TFTFTT1")
        }else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="True"
        &&this.state.validCopass=="false"){
console.log("valid fname email...TFTTTF")
        ToastAndroid.show("last name, username and confrim password is invalid",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="True"
        &&this.state.validCopass=="false"){
console.log("valid fname email...TFTTTF")
        ToastAndroid.show("last name ,confrim password is invalid",ToastAndroid.SHORT)
        }else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"){
            ToastAndroid.show("lastname,birthday,username,password and confrim password is invalid",ToastAndroid.SHORT)
    console.log("valid fname email...TFTFFF")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"){
            ToastAndroid.show("lastname,birthday,password,confrim password is invalid",ToastAndroid.SHORT)
    console.log("valid fname email...TFTFFF")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"){
            ToastAndroid.show("lastname,birthday,username and password is invalid",ToastAndroid.SHORT)
console.log("TFTFFT0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"){
            ToastAndroid.show("lastname,birthday,password is invalid",ToastAndroid.SHORT)
console.log("TFTFFT1")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"){
            ToastAndroid.show("birthday ,username and confrim password is invalid",ToastAndroid.SHORT)
console.log("TTTFTF0")
        }else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"){
            ToastAndroid.show("birthday and confrim password is invalid",ToastAndroid.SHORT)
console.log("TTTFTF1")
        }else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"){
            ToastAndroid.show("Email,birthday,username,and confrim password is invalid",ToastAndroid.SHORT)
console.log("TTTFFF0")
        }else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"){
            ToastAndroid.show("Email,birthday,and confrim password is invalid",ToastAndroid.SHORT)
        console.log("TTTFFF1")
        }else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"){
            console.log("TTTFFF")
            ToastAndroid.show("birthday,password ,username and confrim password is invalid",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"){
            console.log("TTTFFF")
            ToastAndroid.show("birthday,password and confrim password is invalid",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            ToastAndroid.show("lastname , birthday,and username is invalid",ToastAndroid.SHORT)
            console.log("TFTFTT0 with match pass and copass")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            ToastAndroid.show("lastname and birthday is invalid",ToastAndroid.SHORT)
            console.log("TFTFTT1 with match pass and copass")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            ToastAndroid.show("lastname  birthday,and username is invalid with not match password and confrim password",ToastAndroid.SHORT)
            console.log("TFTFTT0 with not match pass and copass")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            ToastAndroid.show("lastname and birthday is invalid with not match password and confrim password",ToastAndroid.SHORT)
            console.log("TFTFTT1 with not match pass and copass")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("FTTFTT0 with not match pass and copass")
            ToastAndroid.show("firts name , birthday and username is invalid with not match password and confrim password",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("FTTFTT1 with not match pass and copass")
            ToastAndroid.show("firts name and birthday is invalid with not match password and confrim password",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("FTTFTT0 with  match pass and copass")
            ToastAndroid.show("first name , birthday and username is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("FTTFTT1 with  match pass and copass")
            ToastAndroid.show("last name,firts name,and birthday is invalid",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("FTTFTT0 with  not match pass and copass")
            ToastAndroid.show("first name ,last name, birthday and username is invalid with not match password and confrim password",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("FTTFTT1 with  not match pass and copass")
            ToastAndroid.show("first name ,last name,and birthday is invalid with not match password and confrim password",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("TFFTTT0 with  not match pass and copass")
            ToastAndroid.show("last name , email and username  is invalid with not match password and confrim password",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("TFFTTT1 with   match pass and copass")
            ToastAndroid.show("last name and email is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("FTFTTT0 with   match pass and copass")
            ToastAndroid.show("firts name  email and username is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("FTFTTT1 with   match pass and copass")
            ToastAndroid.show("firts name and email is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("FTFTTT0 with  not match pass and copass")
            ToastAndroid.show("firts name ,email and username is invalid with not match password and confrim password ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("FTFTTT1 with  not match pass and copass")
            ToastAndroid.show("firts name and email is invalid with not match password and confrim password ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("FFFTTT0 with  not match pass and copass")
            ToastAndroid.show("firts name ,last name,email and username is invalid with not macth password and confrim password ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("FFFTTT1 with  not match pass and copass")
            ToastAndroid.show("firts name ,last name,and email is invalid with not macth password and confrim password ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("FFFTTT0 with   match pass and copass")
            ToastAndroid.show("firts name,last name  ,email,and username is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("FFFTTT1 with   match pass and copass")
            ToastAndroid.show("firts name,last name and email is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("tffftt0 with   match pass and copass")
            ToastAndroid.show("last name , email,birthday is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("tffftt1 with   match pass and copass")
            ToastAndroid.show("last name , email,birthday is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("tffftt0 with  no match pass and copass")
            ToastAndroid.show("last name , email,birthday and username is invalid with not match password and confrim password ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("tffftt1 with  no match pass and copass")
            ToastAndroid.show("last name , email,birthday is invalid with not match password and confrim password ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("ftfftt0 with  no match pass and copass")
            ToastAndroid.show("firts name , email, birthday,and username is invalid with not match password and confrim password ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("ftfftt1 with  no match pass and copass")
            ToastAndroid.show("firts name , email, and birthday is invalid with not match password and confrim password ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("ftfftt0 with   match pass and copass")
            ToastAndroid.show("firts name , email,birthday and username is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("ftfftt1 with   match pass and copass")
            ToastAndroid.show("firts name , email and birthday is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("fffftt0 with no  match pass and copass")
            ToastAndroid.show("last name ,firts name ,email,birthday ,and username is invalid with not match password and confrim password ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password!=this.state.copass){
            console.log("fffftt1 with no  match pass and copass")
            ToastAndroid.show("last name ,firts name ,email,birthday is invalid with not match password and confrim password ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("fffftt0 with  match pass and copass")
            ToastAndroid.show("last name ,firts name ,email,birthday,and username,is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="true"
        &&this.state.password==this.state.copass){
            console.log("fffftt1 with  match pass and copass")
            ToastAndroid.show("last name ,firts name ,email,birthday is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name,username, and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fttttf0 ")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fttttf1 ")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name ,last name and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fftttf0 ")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name ,last name and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fftttf1 ")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("email,username, and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ttfttf0 ")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("email and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ttfttf1 ")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("last name ,email name and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("tffttf0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("last name ,email name and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("tffttf1 ")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name ,birth day ,and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fttftf0 ")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name ,birth day ,and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fttftf1 ")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name,email and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ftfttf0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name,email and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ftfttf1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            console.log("fffttf0")
            ToastAndroid.show("firts name,last name,email  and confrim password  is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            console.log("fffttf1")
            ToastAndroid.show("firts name,last name,email  and confrim password  is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validPassword=="true"
        &&this.state.validUsername=="false"
        &&this.state.validCopass=="false"
        ){
            console.log("ttfftf0")
            ToastAndroid.show("email ,birthday, and confrim password  is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            console.log("ttfftf1")
            ToastAndroid.show("email ,birthday, and confrim password  is invalid ",ToastAndroid.SHORT)
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("last name,email,birthday  and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("tffftf0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("last name,email,birthday  and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("tffftf1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name,email ,birthday, and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ftfftf0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name,email ,birthday, and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ftfftf1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name,last name,email,birthday  and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fffftf0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="true"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firts name,last name,email,birthday  and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fffftf1")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("last name and password  is invalid ",ToastAndroid.SHORT)
            console.log("tfttft0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("last name and password  is invalid ",ToastAndroid.SHORT)
            console.log("tfttft1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firts name and password  is invalid ",ToastAndroid.SHORT)
            console.log("ftttft0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firts name and password  is invalid ",ToastAndroid.SHORT)
            console.log("ftttft1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("last name,firts name, and password  is invalid ",ToastAndroid.SHORT)
            console.log("ffttft0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firts name,last name and password  is invalid ",ToastAndroid.SHORT)
            console.log("ffttft1")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("birth and password  is invalid ",ToastAndroid.SHORT)
            console.log("tttfft0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("birth and password  is invalid ",ToastAndroid.SHORT)
            console.log("tttfft1")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("email,birthday and password  is invalid ",ToastAndroid.SHORT)
            console.log("ttffft0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("email,birthday and password  is invalid ",ToastAndroid.SHORT)
            console.log("ttffft1")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("email ,password,andconfrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ttftft0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("email ,password,andconfrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ttftft1")
        }
        
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("lastname,email ,and password  is invalid ",ToastAndroid.SHORT)
            console.log("tfftft0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("lastname,email ,and password  is invalid ",ToastAndroid.SHORT)
            console.log("tfftft1")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("lastname,email,birthday ,and password is invalid ",ToastAndroid.SHORT)
            console.log("tfffft0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("lastname,email,birthday ,and password is invalid ",ToastAndroid.SHORT)
            console.log("tfffft1")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("lastname ,password and confrim password is invalid ",ToastAndroid.SHORT)
            console.log("tfttff0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("lastname ,password and confrim password is invalid ",ToastAndroid.SHORT)
            console.log("tfttff1")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("lastname ,email,password and confrim password is invalid ",ToastAndroid.SHORT)
            console.log("tfftff0")
        }
        else if(this.state.validfname=="true"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("lastname ,email,password and confrim password is invalid ",ToastAndroid.SHORT)
            console.log("tfftff1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firtname,lastname,birthday ,and password  is invalid ",ToastAndroid.SHORT)
            console.log("fftfft0")

        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firtname,lastname,birthday ,and password  is invalid ",ToastAndroid.SHORT)
            console.log("fftfft1")

        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firtname,email,and password  is invalid ",ToastAndroid.SHORT)
            console.log("ftftft0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firtname,email,and password  is invalid ",ToastAndroid.SHORT)
            console.log("ftftft1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firtname,email,birthday ,and password  is invalid ",ToastAndroid.SHORT)
            console.log("ftffft0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firtname,email,birthday ,and password  is invalid ",ToastAndroid.SHORT)
            console.log("ftffft1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &this.state.username=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname,password,and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ftttff0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname,password,and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ftttff1")
        }
        
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname,username,password,and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fttfff0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname,username,password,and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fttfff1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname,email,password,and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ftftff0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname,email,password,and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ftftff1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname,email,birthday,password,and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ftffff1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="true"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname,email,birthday,password,and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ftffff0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firtname,lastname email,and  password  is invalid ",ToastAndroid.SHORT)
            console.log("ffftft0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firtname,lastname email,and  password  is invalid ",ToastAndroid.SHORT)
            console.log("ffftft1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firtname, lastname email,birthday,and  password  is invalid ",ToastAndroid.SHORT)
            console.log("ffffft1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="false"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="true"
        ){
            ToastAndroid.show("firtname, lastname email,birthday,and  password  is invalid ",ToastAndroid.SHORT)
            console.log("ffffft0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname, lastname,username, password and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ffttff0")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="true"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname,lastname ,password and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("ffttff1")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="false"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname, lastname  ,birthday,username, password and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fftfff")
        }
        else if(this.state.validfname=="false"
        &&this.state.validlname=="false"
        &&this.state.validEmail=="true"
        &&this.state.validBirth=="false"
        &&this.state.validUsername=="true"
        &&this.state.validPassword=="false"
        &&this.state.validCopass=="false"
        ){
            ToastAndroid.show("firtname, lastname  ,birthday password and confrim password  is invalid ",ToastAndroid.SHORT)
            console.log("fftfff")
        }else{
            const API='fbca3609-b742-4d4c-b89e-cfdb55a91efa';
            var url='http://35.197.138.252/web-ewallet/api/index.php/auth/register';
            var params = {
                "email":this.state.email,
                "username" : this.state.username,
                "password" : this.state.password,
                "firstname" : this.state.fname,
                "lastname":this.state.lname,
                "country":this.state.countryid,
                "birthdate":this.state.bod,
                "phone_number":this.phone.getValue(), 
                "confirm_password":this.state.copass,
            };
    
            axios.post(url, params, {
            headers: {
                    'content-type': 'application/json',
                    'API-KEY':API,
            },
            }).then((response) => {
            console.log(response)
            console.log(response.data)
            if(response.data.status===true){
                ToastAndroid.show(response.data.message,ToastAndroid.SHORT)
            }else{ 
                ToastAndroid.show(response.data.message,ToastAndroid.SHORT)
                // Alert.alert(response.data.message)
            
            }
                
    
            })
            .catch((error) => {
                // console.log(error)
            })
        }
}

    


componentDidMount() {
    this.setState({
      pickerData: this.phone.getPickerData(),
    });
  }

  onPressFlag() {
    this.countryPicker.openModal();
  }

  selectCountry(country) {
    this.phone.selectCountry(country.cca2.toLowerCase());
    this.setState({ cca2: country.cca2 });
  }
  render() {
    const {navigation} = this.props
    const {width,height} = Dimensions.get('window')
    console.log("firts name"+this.state.fname)
    console.log("phone number"+this.state.value)
    return (
      <KeyboardAvoidingView styles={{flex:1}} enabled resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={false}>
        <ScrollView>
        <ImageBackground style={{flex:1,minHeight:height}} source={Icons.AppBG}>
        <StatusBar
                translucent
                backgroundColor="transparent"
                barStyle="light-content"
            />
            
            <LinearGradient colors={['rgba(0,0,0,.8)','rgba(0,0,0,.6)','rgba(0,0,0,.5)']} style={{flex:1}}>
            <View style={{flexDirection:'column'}}>
                    <Image  style ={styles.appLogo} source={Icons.AppIconLogo}/>
                    <Text style={styles.titlePages}> SIGN UP</Text>
                    <Text style={styles.subTitle} >Easy Buy Your Games Wallet with E-Wallet</Text>
                    <Text  style={styles.subTitle}>We guarented You for safe buy a Wallet</Text>
                    <View style={{flexDirection:'row',justifyContent:'space-between',alignContent:'space-between',marginTop:30,marginHorizontal:20}}>
                        <View  style={{backgroundColor:'white',width:150,height:45,shadowRadius:7.16,shadowColor:iOSColors.blue,borderRadius:3,borderColor:this.state.boxfname,borderWidth:3}}>
                        <TextInput 
                            onSubmitEditing={() => this._focusNextField('lname')}
                            returnKeyType = {"next"}
                            ref="fname"
                                
                                value={this.state.fname} onChangeText={(text)=>this.validateFname(text)} style={styles.form} placeholder="Firts Name" underlineColorAndroid={this.state.underlinefname} placeholderTextColor={iOSColors.gray}/>
                        </View>
                        <View  style={{backgroundColor:'white',width:150,height:45,shadowRadius:7.16,shadowColor:iOSColors.blue,borderRadius:3,borderColor:this.state.boxlname,borderWidth:3}}>
                        <TextInput 
                            onSubmitEditing={() => this._focusNextField('email')}
                            returnKeyType = {"next"}
                            ref="lname"
                                         value={this.state.lname.toString()} onChangeText={(text)=>this.validateLname(text)} style={styles.form} placeholder="Last Name" underlineColorAndroid={this.state.underlinelname}  placeholderTextColor={iOSColors.gray}/>
                        </View>
                    </View>
                    {/* <View style={{flexDirection:'row',justifyContent:'space-between',alignContent:'space-between',marginTop:20,marginHorizontal:20}}>
                        <View  style={{backgroundColor:'white',width:150,height:45,shadowRadius:7.16,shadowColor:iOSColors.blue,borderRadius:3}}>
                        <TextInput    style={styles.form} placeholder="Email"  placeholderTextColor={iOSColors.gray}/>
                        </View>
                        <View  style={{backgroundColor:'white',width:150,height:45,shadowRadius:7.16,shadowColor:iOSColors.blue,borderRadius:3}}>
                        <TextInput value={this.state.username.toString()} onChangeText={(text)=>this.validateUserName(text)} style={styles.form} placeholder="Username" underlineColorAndroid="transparent" placeholderTextColor={iOSColors.gray}/>
                        </View>
                    </View> */}
                    <View style={[styles.formGroup,{borderColor:this.state.boxEmail,borderWidth:3}]}>
                        
                        <TextInput 
                        onSubmitEditing={() => this._focusNextField('bod')}
                            returnKeyType = {"next"}
                            ref="email"
                                         value={this.state.email.toString()} onChangeText={(text)=>this.validateEmail(text)} placeholder='Email' underlineColorAndroid={this.state.underlineEmail} style={styles.form} placeholderTextColor={iOSColors.gray}  />
                        </View>
                        
                        <View style={[styles.formGroup,{borderColor:this.state.boxBirth,borderWidth:3}]}>
                        <TextInput  
                        onSubmitEditing={() => this._focusNextField('username')}
                            returnKeyType = {"next"}
                            ref="bod" value={this.state.bod} onChangeText={(text)=>this.validateBirth(text)}  placeholder='Birth of Day YYYY-MM-DD' placeholderTextColor={iOSColors.gray} keyboardType={"numeric"}  style={styles.form}   maxLength={10} underlineColorAndroid={this.state.underlineBirth}/>
                        </View>
                        <View style={[styles.formGroup]}>
                        <View style={[styles.form,{marginTop:10,marginLeft:20}]}>
                        <PhoneInput
                        initialCountry="id"
                            ref={(ref) => {
                                this.phone = ref;
                            }}
                            
                            onPressFlag={this.onPressFlag}
                            onSelectCountry={(iso2)=>this.setState({countryid:iso2})}
                            />
                        </View>
                        <CountryPicker
                        ref={(ref) => {
                            this.countryPicker = ref;
                        }}
                        
                        onChange={value => this.selectCountry(value)}
                        translation="eng"
                        cca2={this.state.cca2}
                        ></CountryPicker>
                    </View>
                        <View style={[styles.formGroup,{borderColor:this.state.boxUser,borderWidth:3}]}>
                        <TextInput 
                        onSubmitEditing={() => this._focusNextField('password')}
                            returnKeyType = {"next"}
                            ref="username" autoCapitalize value={this.state.username.toString()} onChangeText={(text)=>this.validateUserName(text)} style={styles.form} placeholder="Username" underlineColorAndroid={this.state.underlineUser} style={styles.form} placeholderTextColor={iOSColors.gray}  />
                        </View>
                    <View style={{flexDirection:'row',justifyContent:'space-between',alignContent:'space-between',marginTop:20,marginHorizontal:20}}>
                        <View  style={{backgroundColor:'white',width:150,height:45,shadowRadius:7.16,shadowColor:iOSColors.blue,borderRadius:3,borderColor:this.state.boxPass,borderWidth:3,flexDirection:'row'}}>
                        <TextInput
                        onSubmitEditing={() => this._focusNextField('copass')}
                            returnKeyType = {"next"}
                            ref="password"
                            value={this.state.password} onChangeText={(text)=>this.validatePassword(text)}  placeholder='Password'  style={[styles.form,{width:90}]} placeholderTextColor={iOSColors.gray}  secureTextEntry={this.state.status1} underlineColorAndroid={this.state.underlinePass} />
                        <Item >
                        <TouchableOpacity onPress={this.ShowHidePassword}>
                        <Icon active name='eye' />
                        </TouchableOpacity>
                        </Item>
                        </View>
                        <View  style={{backgroundColor:'white',width:150,height:45,shadowRadius:7.16,shadowColor:iOSColors.blue,borderRadius:3,borderColor:this.state.boxCopass,borderWidth:3,flexDirection:'row'}}>
                        <TextInput 
                        
                        returnKeyType = {"done"}
                        ref="copass"
                        onSubmitEditing={Keyboard.dismiss}value={this.state.copass} onChangeText={(text)=>this.validateCopass(text)} placeholder='Confrim Pass'   style={[styles.form,{width:90}]} placeholderTextColor={iOSColors.gray}  secureTextEntry={this.state.status2} underlineColorAndroid={this.state.underlineCopass}/>
                        <Item >
                        <TouchableOpacity onPress={this.ShowHideCopass}>
                        <Icon active name='eye' />
                        </TouchableOpacity>
                        </Item>
                        </View>
                    </View>
                        
                        
                    
                        <View style={{marginHorizontal:20,marginTop:20}}>
                    <GradientButton onPress={this.OnDataSendWithAxios} title="Sign Up" style={{height:45}}/>
                    </View>
                    
                    <View style={[styles.forgotPassword,{ flexDirection:'row', alignSelf:'center', alignItems:'center',marginVertical:5}]}>
                    <Text style={[styles.forgotPassword,{ alignSelf:'flex-start' }]}> If you already have account please 
                    </Text>
                    <TouchableOpacity onPress={()=>navigation.navigate(Constants.Screen.Login)}>
                    <Text style={[styles.linkText,{alignSelf:'flex-end'}]} > Sign In  </Text>
                    </TouchableOpacity>
                    </View>
                </View>
            </LinearGradient>
        </ImageBackground>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
export default Register;
