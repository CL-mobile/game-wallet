import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Image,
    SafeAreaView,
    StatusBar,
    TouchableOpacity,
    ScrollView,
    LayoutAnimation,
    ActivityIndicator,
    Linking,
    Text,
    TextInput
  } from 'react-native'
import GridView from 'react-native-gridview';
import {Layout, HeaderBar,GradientButton, NavBack} from '@components'
import styles from './style'
import {Icons,Constants,Utils} from '@common'
import LinearGradient from "react-native-linear-gradient";

var screen = Dimensions.get('window');

const itemsPerRow = 3;

class GameAll extends Component {
    state = {
        dataGame:this.props.navigation.state.params.data,
        dataSource:[],
        filter:'',
    }

    componentDidMount(){
        const dataSource = new GridView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2,
          }).cloneWithRows(this.state.dataGame);
      
          this.setState({dataSource:dataSource})
        
    }

    showHideMessage = (item, sectionID, rowID, itemIndex, itemID) => {
    
        //item.sub_menu.length > 0 ? this.props.set_active_menu(item.active, this.props.dataMenu, item) : this._showModal(item)
        this.showListProduct(item) 
        //item.sub_menu.length > 0 ?  this.showListProduct(item) : this._showModal(item) 
      }
    
      showListProduct(item){
    
          let {sub_menu, name, gameId} = item;
    
          this.props.showGame({
              vendor : name,
              gameProduct:sub_menu[0],
              subCategory:sub_menu,
              gameId: gameId
          });
      }

      filter = (query) => {
          return this.state.dataGame.filter((el) => 
                el.name.toLowerCase().indexOf(query.toLowerCase()) > -1
            )
      }

  render() {
    
    return (
      <Layout>
        <HeaderBar left={<NavBack onPress={this.props.goBack}/>} center={<Text style={styles.topHeader }>All Products</Text>}/>
        <View style={{marginVertical:10}}>
            <Form placeholder="Search" placeholderTextColor="#aaa" onChangeText={(text) => {
                this.setState({filter:text})
                if(this.state.filter.length > 0){
                    this.setState({dataGame:this.filter(text)})
                }else{
                    this.setState({dataGame:this.props.navigation.state.params.data})
                }
            }} />
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
        <GridView
            data={this.state.dataGame}
            dataSource={this.props.randomizeRows ? this.state.dataSource : null}
            itemsPerRow={itemsPerRow}
            style={{flex:1}}
            renderItem={(item, sectionID, rowID, itemIndex, itemID) => {
                return (
                <View style={styles.contentOPMainSubContainer}>
                    <TouchableOpacity onPress={() =>  this.showHideMessage(item, sectionID, rowID, itemIndex, itemID)}>
                    <LinearGradient colors={['rgba(5,70,91,.3)', 'transparent']}  style={[styles.contentOPButton]} >

                        <Image
                        source={{uri:item.icon}}
                        style={styles.contentOPButtonIcon}
                        />
                        <Text style={styles.contentOPButtonText}>{(item.name).length>10?((item.name).substring(0,10)+'...'):(item.name)}</Text>
                    </LinearGradient>
                    </TouchableOpacity>

                </View>
                );
            }}
            />
        </ScrollView>          
      </Layout>
    );
  }
}

export default GameAll;

const Form = (props) => {
    const {bottomLabel, bottomLabelText} = props
   return (<View style={styles.formSection}>
        
        <View style={[styles.formContainer]}>
            <Image
                source={Icons.Search}
                style={{height:25, width:25, tintColor:'white', marginLeft:5}}
            />
            <TextInput  {...props}  style={styles.formControl} underlineColorAndroid={'transparent'}/>
        </View>
       {bottomLabel &&
        <Text style={{color: '#fff', textAlign: 'right'}}>{bottomLabelText}</Text>
       }
    </View>)
}