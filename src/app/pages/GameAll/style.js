import {StyleSheet, Dimensions} from 'react-native'
import {robotoWeights,iOSColors} from 'react-native-typography'

export default StyleSheet.create({
  topHeader :{
      ...robotoWeights.light,
      marginRight:18,
      color:'white',
      textAlign:'center',
      flex:1
  },
   TabsTitle:{
       ...robotoWeights.light,
       color:iOSColors.lightGray
   },
    activeTabsText:{
        ...robotoWeights.medium,
        color:'#4acaff'
    },
    contentOPMainSubContainer:{
      flex:1, marginHorizontal: 5, marginVertical: 5
    },
    contentOPButton:{
      padding:10, borderRadius:5,alignItems:'center', flex:1
    },
    contentOPButtonIcon:{
      height:50, width:50
    },
    contentOPButtonText:{
      ...robotoWeights.thin,
    fontSize:14, fontWeight:'400',marginTop: 10,
      color:iOSColors.lightGray
  },
  formSection:{
    paddingVertical: 10,
    paddingHorizontal:16,

},
formSectionTitle:{
    ...robotoWeights.light,
    color:iOSColors.lightGray,
    fontSize:13.5,
    marginBottom: 7
},
formContainer:{
    flexDirection: 'row',
    alignItems:'center',
    borderWidth: .5,
    borderColor:'rgba(60,122,191,.8)',
    borderRadius:3,
    height:45,
    backgroundColor:'rgba(60,122,191,.1)'
},
formControl:{
    ...robotoWeights.light,
    paddingHorizontal:10,
    color:iOSColors.lightGray,
    fontSize:16.5 ,
    paddingTop:5,
    flex:1
},
})
