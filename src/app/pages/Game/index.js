import React, { Component } from 'react';
import { View, Text, ActivityIndicator, WebView } from 'react-native';
import {Layout, HeaderBar,GradientButton, NavBack} from '@components'
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import LinearGradient from "react-native-linear-gradient";
import * as ActionTypes from '@actions/ActionTypes'

import {Config,Constants, Icons, Colors,Utils} from '@common'

class Game extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount()
  {
        let {gameId} = this.props.navigation.state.params.data
      this.props.getGame(gameId, this.props.dataUser.token)
  }

  render() {
    return (
      <Layout>
        {this.props.isRequesting ?
        <ActivityIndicator size="small" color="#39c67d" style={{alignItems:'center', justifyContent:'center', flex:1}} />
        :
        <WebView 
        url={this.props.dataGame.gameUrl} 
        style={{
            flex:1
        }}
        scrollEnabled={false}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        startInLoadingState={true}
        />
        }
      </Layout>
    );
  }

  componentWillReceiveProps(nextProps){
      console.log(nextProps.type)
      console.log(nextProps.dataGame)
  }
}

function mapStateToProps({gameReducers, authReducers}){

    return {
      dataGame:gameReducers.dataGame,
      type:gameReducers.type,
      isRequesting:gameReducers.isRequesting,
      dataUser:authReducers.dataUser,
    }
  }
  
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch)
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(Game)
