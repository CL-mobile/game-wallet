import {StyleSheet, Dimensions} from 'react-native'
import {robotoWeights, iOSColors} from 'react-native-typography'

export default StyleSheet.create({
    GreetingBox:{
      paddingTop:20,
      paddingHorizontal:16,
        paddingBottom: 5
    },
    GreetingTitle:{
        ...robotoWeights.thin,
        fontSize: 24,
        color:iOSColors.lightGray
    },
    GreetingSubTitle:{
        ...robotoWeights.light,
        fontSize: 12,
        color:iOSColors.gray,
        marginTop: 5
    },
    balanceTitle:{
        ...robotoWeights.condensed,
        fontSize:10,
        color:iOSColors.midGray,
        lineHeight:14
    },
    balanceText:{
        ...robotoWeights.thin,
        fontSize:28,
        color:iOSColors.lightGray
    },
    container: {
        flex: 1,
        backgroundColor: '#1c1c1c',

    },
    loading: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: 'white'
    },
    content: {
        paddingBottom: 10
    },
    buttonGroup: {
        flex:1,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        paddingHorizontal: 16,
        marginBottom: 30
    },
    buttonBox:{
      marginHorizontal: 5
    },
    btnOutline:{
        flex:1,
        flexDirection:'row',
        alignItems:'center',
        borderRadius:3,
        borderWidth: .8,
        paddingHorizontal: 16,
        paddingVertical:12,
        shadowColor: iOSColors.black,
        shadowOffset: {
            width: 3,
            height: -10,
        },
        shadowOpacity: 1,
        shadowRadius: 7.16,
        elevation: 2,
    },
    buttonIcon:{
        width:15,
        height:15
    },
    buttonText:{
        ...robotoWeights.thin,
        fontSize:16.7,
        color:iOSColors.lightGray,
        marginLeft: 6.6
    },
    menuBox:{
        flex:1,
        flexDirection:'row',
        borderBottomWidth:.6,
        borderColor:'rgba(58,203,218,.1)',
        justifyContent:'space-between',
        alignItems:'center',
        paddingHorizontal: 16,
        paddingVertical:15
    },
    menuIcon:{
        width:20,
        height:20,
        marginRight: 12,
        tintColor:'#fafafa'
    },
    menuLabel:{
        ...robotoWeights.light,
        flex:1,
        color:iOSColors.lightGray,
        fontSize:14
    },
    rightIconMenu:{width:17,height:17, tintColor:iOSColors.midGray}

})
