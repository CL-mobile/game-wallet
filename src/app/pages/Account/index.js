import React, { Component } from 'react';
import { View,ImageBackground,StatusBar,Alert,RefreshControl,Image,ScrollView,SafeAreaView,TouchableOpacity, Dimensions ,ActivityIndicator} from 'react-native';
import { Text} from 'native-base';
import {Icons,Constants,Colors,Utils} from '@common'
import {Layout, HeaderBar, NavBack} from '@components'
import LinearGradient from 'react-native-linear-gradient'
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'
import styles from './style'
import { LOGOUT } from '../../actions/ActionTypes';
import {NavigationActions, StackActions} from 'react-navigation'
import { iOSColors } from 'react-native-typography';
class Account extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      loadingFirebase:true,
    };
  }
  logout = () =>
  {
     this.props.logout()
     const resetAction = StackActions.reset({
       index: 0,
       actions: [NavigationActions.navigate({routeName:Constants.Screen.Login})],
     });
     this.props.navigation.dispatch(resetAction);
  }
  componentDidMount()
  {
        this.props.getHomeBalanceInfo(this.props.dataUser.token)
        this.props.getDataFirebase(this.props.dataUser.data.id)
  }
  _onRefresh = () => {
    this.setState({refreshing: true,isLoadingB:true});
    this.props.getHomeBalanceInfo(this.props.dataUser.token)
    
  }
  render() {
    let {showDeposit,showTransaction,showTransactionHistory,transferPage, balanceDetail, accountSetting}=this.props
    return (<Layout id={'account'} animated={true}>
      <HeaderBar left={<NavBack icon={Icons.icSetting}/>} right={<NavBack icon={Icons.icDots}/>}/>
      <ScrollView
      refreshControl={
          <RefreshControl
            refreshing={this.state.refreshing}
            onRefresh={this._onRefresh}
          />
        }>
          <View>
              <View style={styles.GreetingBox}>
                    <Text style={styles.GreetingTitle}>Hi, {this.props.Profile.firstname}</Text>
                  <Text style={styles.GreetingSubTitle}>{this.props.Profile.email}</Text>
              </View>
            <View style={{ alignItems:'center', paddingVertical:20 }}>
              <Text style={styles.balanceTitle}>TOTAL BALANCE</Text>
              {this.state.loadingFirebase? 
              <ActivityIndicator size={'small'} color={iOSColors.lightGray}/>
              :
              <Text style={styles.balanceText}>{Utils.convertCurrency(this.props.dataBalance.currency)} {Utils.convertFormatAmount(this.props.dataFirebase.total_balance, this.props.dataBalance.currency)} </Text>}
            </View>

              <View style={styles.buttonGroup} >
                <TouchableOpacity style={styles.buttonBox} onPress={showDeposit}>
                 <LinearGradient colors={['rgba(27,74,217,.2)','transparent']} style={[styles.btnOutline, {borderColor: 'rgba(16, 121, 186,.4 )'}]}>
                    <Image source={Icons.Refresh} style={styles.buttonIcon}/>
                     <Text style={styles.buttonText}> DEPOSIT </Text>
                 </LinearGradient>
                </TouchableOpacity>
                  <TouchableOpacity style={styles.buttonBox} >
                      <LinearGradient colors={['rgba(27,74,217,.2)','transparent']} style={[styles.btnOutline, {borderColor: 'rgba(37, 106, 130,.4 )'}]}>
                          <Image source={Icons.Refresh} style={styles.buttonIcon}/>
                          <Text style={styles.buttonText}> WITHDRAW</Text>
                      </LinearGradient>
                  </TouchableOpacity>
              </View>

              <View style={{marginVertical: 10}}>
                  <Menu label={"Balance Detail"}
                        icon={Icons.icWalletCoin} onPress={balanceDetail}/>
                  <Menu label={"Transfer"}
                        icon={Icons.icWalletExchange} onPress={transferPage}/>
                  <Menu label={"Transaction History"}
                        icon={Icons.icTransHistory}/>
              </View>

              <View style={{marginVertical: 10}}>
                  <Menu label={"Account Setting"} onPress={accountSetting}
                        icon={Icons.User}/>
                  <Menu onPress={this.logout} label={"Logout"} rightIcon={false}
                        icon={Icons.SignOut}/>
              </View>

          </View>

      </ScrollView>
    </Layout>
    );
  }
  componentWillReceiveProps(nextProps)
  {
    if(nextProps.dataUser == [])
    {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [NavigationActions.navigate({routeName:Constants.Screen.Login})],
      });
      nextProps.navigation.dispatch(resetAction);
    } else if (nextProps.balanceType==ActionTypes.GET_HOME_BALANCE_INFO_SUCCESS){
      setTimeout(()=>{
        this.setState({
            isLoadingB: false,
            refreshing: false
        })
    },250)
    }

    if(nextProps.firebaseType == ActionTypes.GET_FIREBASE_SUCCESS){
      this.setState({loadingFirebase:false})
    }
    
    
  }
}

function mapStateToProps({othersReducers,authReducers,menusReducers,bannerReducers,balanceInfoReducers,firebaseReducers}){

    return {
      isProductMessage:othersReducers.isProductMessage,
      dataUser:authReducers.dataUser,
      balanceType:balanceInfoReducers.type,
      dataMenu:menusReducers.dataMenu,
      dataPopular:menusReducers.dataPopular,
      dataBanner:bannerReducers.dataBanner,
      dataBalance:balanceInfoReducers.dataBalance,
      isRequesting:menusReducers.isRequesting,
      dataFirebase:firebaseReducers.dataFirebase,
      isRequestingFirebase:firebaseReducers.isRequesting,
      firebaseType:firebaseReducers.type,
      isLoadingB:balanceInfoReducers.isLoadingB,
      Profile:balanceInfoReducers.Profile
    }
  }
  
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch)
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(Account)
  


class Menu extends Component{

    render(){
        const {icon,label, rightIcon, onPress} = this.props
        return(<TouchableOpacity style={styles.menuBox} onPress={onPress}>
            <View style={{flex:1,flexDirection:'row', justifyContent: 'space-between', alignItems:'center', }}>
                <Image source={icon}
                       style={styles.menuIcon}
                />
                <Text style={styles.menuLabel}>{label}</Text>
            </View>
            {rightIcon &&
            <Image style={styles.rightIconMenu} source={Icons.Right}/>}
        </TouchableOpacity>)
    }

}

Menu.defaultProps={
    rightIcon:true
}
LOGOUT.defaultProps={
    dataUser:[]
}
