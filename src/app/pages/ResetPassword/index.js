import React, { Component } from 'react';
import { View,KeyboardAvoidingView, Text ,ToastAndroid,StatusBar,ImageBackground,Image,TextInput,TouchableOpacity,ScrollView,Dimensions,StyleSheet,Keyboard,Alert} from 'react-native';
import {Icons,Constants} from '@common'
import {GradientButton} from '@components'
import {  Item, Input, Icon } from 'native-base';
import style from './style'
import {iOSColors} from "react-native-typography"
import LinearGradient from 'react-native-linear-gradient';
import {NavigationActions, StackActions} from 'react-navigation'
import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'
class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
        status1:true,
        status2:true,
        password:"",
        boxPass:'transparent',
        validPassword:'none',
        copass:"",
        boxCopass:'transparent',
        validCopass:'none'

    };
  }
  validatePassword = (text) => {
    console.log(text);
    let reg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/ ;
    if(reg.test(text) === false)
    {
    // if(Platform.OS==='android'){
    //     ToastAndroid.show("password invalid,must 8 character with number and character ",ToastAndroid.SHORT)
    // }else{
    //     AlertIOS.alert("password invalid,must 8 character with number and character ")
    // }
    
    console.log("Password is Not Correct");
    this.setState({password:text,boxPass:"red",validPassword:"false"})
    return false;
      }
    else {
        // if(Platform.OS==='android'){
        //     ToastAndroid.show("Password is Correct",ToastAndroid.SHORT)
        // }else{
        //     AlertIOS.alert("Password is Correct")
        // }
      this.setState({password:text,boxPass:"white",validPassword:"true"})
      console.log("Password is Correct");
    }
    }
    validateCopass= (text) => {
        console.log(text);
        let reg = /^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$/ ;
        if(reg.test(text) === false)
        {
        // if(Platform.OS==='android'){
        //     ToastAndroid.show("confrim password invalid,must 8 character with number and character ",ToastAndroid.SHORT)
        // }else{
        //     AlertIOS.alert("confrim password invalid,must 8 character with number and character ")
        // }
        
        console.log("confrim password is Not Correct");
        this.setState({copass:text,boxCopass:"red",validCopass:"false"})
        return false;
          }
        else {
            // if(Platform.OS==='android'){
            //     ToastAndroid.show("confrim password is Correct",ToastAndroid.SHORT)
            // }else{
            //     AlertIOS.alert("confrim password is Correct")
            // }
          this.setState({copass:text,boxCopass:"white",validCopass:"true"})
          console.log("confrim password is Correct");
        }
        }
_focusNextField(nextField) {
    this.refs[nextField].focus()
    }
    ShowHidePassword = () =>{

        if(this.state.status1 == true)
        {
            this.setState({status1: false})
        }
        else
        {
            this.setState({status1: true})
        }
        }
        ShowHideCopass = () =>{
    
            if(this.state.status2 == true)
            {
                this.setState({status2: false})
            }
            else
            {
                this.setState({status2: true})
            }
            }
            onValidationCOde=()=>{
                let {password,copass}=this.state
                let Vc = this.props.navigation.state.params
                let ID=Vc.vcdata.id
                console.log(ID)
                console.log(password)
                console.log(copass)
                this.props.resetpassword(ID,password,copass)
            }
  render() {
    const {showHome,showRegister,showForgot} = this.props
    const {width,height} = Dimensions.get('window')
    return (
        <KeyboardAvoidingView style={{flex:1}} enabled resetScrollToCoords={{ x: 0, y: 0 }} scrollEnabled={false}>
        <ScrollView style={{flex:1}}>
         <ImageBackground source={Icons.AppBG} style={{flex:1,resizeMode:'cover',minHeight: height }}>
             <StatusBar translucent backgroundColor="transparent" barStyle="light-content"/>

                <LinearGradient colors={['rgba(0,0,0,.8)','rgba(0,0,0,.6)','rgba(0,0,0,.5)']} style={{flex:1}}>
                    <View style={{flex:1}}>
                        <Image  style ={style.appLogo} source={Icons.AppIconLogo }/>
                        <View style={ style.container }>
                            <Text style={ style.titlePages }>Forgot Password</Text>
                            <Text style={ style.subTitle }>Easy Buy Your Games Wallet with Optimus Solutions</Text>
                            <Text  style={ style.subTitle }>We guarented You for safe buy a Wallet</Text>
                            <View style={{marginTop:60}}>
                        <View  style={{backgroundColor:'white',height:50,shadowRadius:7.16,shadowColor:iOSColors.blue,borderRadius:3,borderColor:this.state.boxPass,borderWidth:3,flexDirection:'row'}}>
                        <TextInput
                        onSubmitEditing={() => this._focusNextField('copass')}
                            returnKeyType = {"next"}
                            ref="password"
                            value={this.state.password} onChangeText={(text)=>this.validatePassword(text)}  placeholder='New Password'  style={[style.form,{width:250}]} placeholderTextColor={iOSColors.gray}  secureTextEntry={this.state.status1} underlineColorAndroid='transparent' />
                        <Item >
                        <TouchableOpacity onPress={this.ShowHidePassword}>
                        <Icon active name='eye' />
                        </TouchableOpacity>
                        </Item>
                        </View>
                        <View  style={{backgroundColor:'white',height:50,shadowRadius:7.16,shadowColor:iOSColors.blue,borderRadius:3,borderColor:this.state.boxCopass,borderWidth:3,flexDirection:'row',marginTop:20}}>
                        <TextInput 
                        
                        returnKeyType = {"done"}
                        ref="copass"
                        onSubmitEditing={Keyboard.dismiss} value={this.state.copass} onChangeText={(text)=>this.validateCopass(text)} placeholder='New Confrim Pass'   style={[style.form,{width:250}]} placeholderTextColor={iOSColors.gray}  secureTextEntry={this.state.status2} underlineColorAndroid='transparent'/>
                        <Item >
                        <TouchableOpacity onPress={this.ShowHideCopass}>
                        <Icon active name='eye' />
                        </TouchableOpacity>
                        </Item>
                        </View>
                    </View>
                                
                                <GradientButton title={'Reset Password  Now'} style={{marginTop:30 }} loading={this.props.isRequesting} onPress={this.onValidationCOde}  />
                                
                            </View>
                        </View>
                </LinearGradient>
         </ImageBackground>
        </ScrollView>
    </KeyboardAvoidingView>
    );
  }
  componentWillReceiveProps(nextProps){
    const {navigation } = this.props

    if (nextProps.type == ActionTypes.RESETPASSWORD_SUCCESS) {

        navigation.navigate(Constants.Screen.Login)
        Alert.alert("Your password has been Reset")
    
    ToastAndroid.show(nextProps.message, ToastAndroid.SHORT);
    } else if (nextProps.type == ActionTypes.RESETPASSWORD_FAIL) {

        ToastAndroid.show(nextProps.message, ToastAndroid.SHORT);
    }
}
}
function mapStateToProps({ authReducers}){
    return {
        type:authReducers.type,
        data:authReducers.data,
        message:authReducers.message,
        isRequesting:authReducers.type == ActionTypes.RESETPASSWORD_PENDING,
    
    }
  }
  
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch)
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(ResetPassword)

