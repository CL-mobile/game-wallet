import {Dimensions, StyleSheet} from "react-native"
import { robotoWeights,iOSColors } from 'react-native-typography'

export default StyleSheet.create({
    subTitle : {
        ...robotoWeights.light,
        fontSize:12,
        lineHeight:18,
        color:iOSColors.gray
    },
    container: {
        flex:1,
        paddingHorizontal: 20
    },
    titlePages: {
        ...robotoWeights.medium,
        marginTop:18,
        color:iOSColors.lightGray,
        fontSize:35,
        lineHeight:50
    },
    appLogo:{
        width:250,
        height:100,
        resizeMode:'contain',
        marginTop:45
    },
    formGroup:{
        shadowColor: iOSColors.blue,
        shadowOffset: {
            width: 3,
            height: -10,
        },
        shadowOpacity: 1,
        shadowRadius: 7.16,
        elevation: 2,
        backgroundColor:iOSColors.white,
        height:45,
        marginTop:20,
        borderRadius:3
    },
    form : {
        ...robotoWeights.light,
        height:45,
        fontSize:16,
        marginHorizontal:10
    },
    forgotPassword: {
        ...robotoWeights.light,
        fontSize:13,
        color:iOSColors.gray,
        marginVertical:3 ,
        alignSelf:'flex-end',
        paddingVertical: 5
    },
    linkText: {
        ...robotoWeights.light ,
        color:iOSColors.lightGray2,
        paddingVertical: 5
    }

})