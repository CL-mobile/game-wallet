import {StyleSheet, Dimensions} from 'react-native'
import {robotoWeights, iOSColors} from 'react-native-typography'

const { width: viewportWidth, height: viewportHeight } = Dimensions.get('window');

function wp (percentage) {
    const value = (percentage * viewportWidth) / 100;
    return Math.round(value);
}

function heightp (percentage) {
    const value = (percentage * viewportHeight) / 100;
    return Math.round(value);
}

const slideHeight = viewportHeight * 0.36;
const slideWidth = wp(56);
const itemHorizontalMargin = wp(2);
export const fixedBottom = heightp(50)

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

export default StyleSheet.create({
    pageTitle:{
        ...robotoWeights.bold,
        color:iOSColors.lightGray,
        fontSize:24
    },
    subTitle:{
        ...robotoWeights.condensed,
        color:iOSColors.midGray,
        fontSize: 12,
        marginTop: 15
    },
    balanceText:{
        ...robotoWeights.medium,
        color:iOSColors.lightGray,
        fontSize:26,
        marginTop: 5
    },
    cardBalance:{
        paddingHorizontal: 12,
        paddingVertical: 10,
        borderRadius:4
    },
    circle:{
      width:7,
      height:7,
      marginRight:10,
      borderRadius: 50,
      marginTop:7,
      backgroundColor: 'rgba(0,0,0,.3)'
    },
    cardTitle:{
        ...robotoWeights.light,
        color:'rgba(238,238,238,.5)',
        fontSize:16.5
    },
    balanceOnCard:{
        ...robotoWeights.medium,
        color:iOSColors.lightGray,
        fontSize:20,
        marginVertical: 7,
        paddingLeft: 15
    },
    bottomContainer:{
        flex:1,
        paddingHorizontal: 10,
        paddingVertical: 16,
        borderTopLeftRadius:5,
        borderTopRightRadius:10,
        backgroundColor:iOSColors.lightGray
    },
    bottomText:{
        ...robotoWeights.medium,
        color:'#1f1f1f',
        fontSize:18,
        paddingBottom:15
    },
    transactionTitle:{
        ...robotoWeights.light,
        color:'#2f2f2f',
        fontSize:16
    },
    transactionSubTitle:{
        ...robotoWeights.light,
        color:iOSColors.gray,
        fontSize:11,
        marginTop:5
    },
    rightTextTransaction:{
        ...robotoWeights.medium,
        color:'#339835',
        fontSize:14
    },
    rightDanger:{
        color:'#bd302e'
    }
})