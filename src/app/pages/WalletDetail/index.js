import React, {Component} from 'react'
import {View, Text, Dimensions, ScrollView, Image, TouchableOpacity} from 'react-native'

import {Layout, HeaderBar, NavBack} from '@components'
import styles, {itemWidth, sliderWidth, fixedBottom}  from './style'
import Carousel from 'react-native-snap-carousel'
import LinearGradient from 'react-native-linear-gradient'
import SlidingUpPanel from 'rn-sliding-up-panel';
import {Transition} from 'react-navigation-fluid-transitions'

import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import {Rect,Circle} from 'react-native-svg'
import axios from 'axios'
import * as ActionTypes from '@actions/ActionTypes'

import {Config,Constants, Icons, Colors, Utils} from '@common'

const {height} = Dimensions.get('window')

class WalletDetail extends Component{

    constructor(props){
        super(props)
        this.state = {
            visible: false,
            draggableRange: {
                top: height-100 ,
                bottom: fixedBottom
            },
            dataBalanceDetail:props.dataBalance.balance_type.map((x, i) => {

                x.balance = Object.values(props.dataBalance.balance)[i]
    
                return x
            }),
            scrolled:false
        }

    }

    componentDidMount(){
        this.props.getRecentTransaction(this.props.dataUser.token)   
    }

    render() {

        const {goBack, deposit,transfer} = this.props;
        const data = [{title:"Main Wallet", balance:2000},{title:"YSB Wallet", balance:2000},{title:"Cassino Wallet", balance:2000}, ]
        const tstData = [1,2,3,4,5,6,7,8,9,0,1,12,32,4,5,5,6,7,8,9,0]
        return (
            <Layout>
                <View style={{marginBottom:100}} onLayout={(event) => {
                    var {x, y, width, height:Vheight} = event.nativeEvent.layout;

                    this.setState({draggableRange:{top:this.state.draggableRange.top, bottom:height-(Vheight+130) }})
                }}>
                    <HeaderBar left={<NavBack onPress={goBack}/>}/>

                        <View style={{paddingHorizontal: 10, paddingVertical: 16, marginBottom: 10}}>
                            <Text style={styles.pageTitle}>My Wallet</Text>
                            <Transition  appear={'horizontal'}>
                                <View>
                                    <Text style={styles.subTitle}>Total Balance</Text>
                                    <Text style={styles.balanceText}>{Utils.convertCurrency(this.props.dataBalance.currency)} {Utils.convertFormatAmount(this.props.dataFirebase.total_balance, this.props.dataBalance.currency)}</Text>
                                </View>
                            </Transition>
                        </View>

                    <Transition appear={'flip'}>
                        <Carousel
                            data={this.state.dataBalanceDetail}
                            renderItem={e => this._renderItem(e, this.props)}
                            sliderWidth={sliderWidth}
                            itemWidth={itemWidth}
                            activeSlideOffset={1}
                            swipeThreshold={5}
                        />
                    </Transition>
                </View>
                <SlidingUpPanel
                    visible
                    startCollapsed
                    allowDragging={true}
                    draggableRange={this.state.draggableRange}
                    showBackdrop={false}
                    allowMomentum={true}

                    >
                    <View style={styles.bottomContainer}>
                        <View style={{flexDirection:'row' }}>
                            <Image source={Icons.ic_draggable} style={{width:16, height:16, tintColor:'#1f1f1f', marginRight: 10, marginTop:5}}/>
                            <Text style={styles.bottomText}>Recent Transactions</Text>
                        </View>

                        <ScrollView  bounces={false} showsVerticalScrollIndicator={false}>
                            <View style={{paddingVertical:10}} >
                                {this.props.dataRecentTransaction.map(e => (
                                    <View style={{flexDirection:'row', justifyContent: 'space-between', alignItems: 'center', borderBottomWidth: .8, borderColor:'#ccc', paddingVertical:8, paddingHorizontal:5}}>
                                        <View style={{flex:1}}>
                                            <Text style={styles.transactionTitle}>{e.ket}</Text>
                                            <Text style={styles.transactionSubTitle}>{e.date}</Text>
                                        </View>
                                        <View>
                                            {e.type == "debet" ? 
                                            <Text style={[styles.rightTextTransaction, styles.rightDanger]}>-{Utils.convertCurrency(this.props.dataBalance.currency)} {Utils.convertFormatAmount(e.amount, this.props.dataBalance.currency)}</Text>
                                            :
                                            <Text style={[styles.rightTextTransaction]}>{Utils.convertCurrency(this.props.dataBalance.currency)} {Utils.convertFormatAmount(e.amount, this.props.dataBalance.currency)}</Text>
                                            }
                                            <Text style={[styles.transactionSubTitle,{alignSelf:'flex-end'}]} >{e.wallet}</Text>
                                        </View>

                                    </View>
                                ))}

                            </View>
                        </ScrollView>
                    </View>
                </SlidingUpPanel>
            </Layout>
        );
    }

    _renderItem({item, index}, props){


        return(<LinearGradient colors={['rgba(101,146,255,.6)', 'rgba(63,55,157,.3)']} style={styles.cardBalance}>
            <View style={{flexDirection:'row'}}>
                <View style={styles.circle}/>
                <Text style={styles.cardTitle}>{item.name}</Text>
            </View>
            <Text style={styles.balanceOnCard}>{Utils.convertCurrency(this.props.dataBalance.currency)} {Utils.convertFormatAmount(item.total_balance, this.props.dataBalance.currency)}</Text>

            <View style={{flexDirection:'row', justifyContent:'flex-end', alignItems: 'flex-end'}}>
                <DarkButton type={'icon'}/>
                
                <DarkButton type={'text'} onPress={() => this.onNav(index, props)} />
            </View>
        </LinearGradient>)
    }

    onNav = (index, props) => {
        if(index == 0){
            props.deposit()
        }else if(index == 1){
            props.transfer(index)
        }else if(index == 2){

        }
    }

}

function mapStateToProps({authReducers,balanceInfoReducers,transactionsReducers,firebaseReducers}){

    return {
      dataUser:authReducers.dataUser,
      dataBalance:balanceInfoReducers.dataBalance,
      dataBalanceDetail:balanceInfoReducers.dataBalanceDetail,
      dataRecentTransaction:transactionsReducers.dataRecentTransaction,
      dataFirebase:firebaseReducers.dataFirebase,
      isRequestingFirebase:firebaseReducers.isRequesting,
      firebaseType:firebaseReducers.type,
    }
  }
  
  
  function mapDispatchToProps(dispatch){
    return bindActionCreators(ActionCreators,dispatch)
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(WalletDetail)

const DarkButton = (props) =>{

    return (<TouchableOpacity onPress={props.onPress} style={{backgroundColor:'rgba(0,0,0,.2)',borderRadius:2, paddingHorizontal:10, paddingVertical:7, marginHorizontal:2, marginVertical: 5}}>
        {props.type == 'text' &&
         <Text style={{color:'#eee', fontSize:11}}>DEPOSIT</Text>}
        {props.type == 'icon' &&
        <Image source={Icons.icLeftRounded} style={{width:15, height:15 , tintColor: '#eee'}}/>}
    </TouchableOpacity>)
}