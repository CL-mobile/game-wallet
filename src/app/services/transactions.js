import {Constants} from '@common'
import WService from './helper/WService'
var wservice = new WService()

export const getRecentTransaction = (token) => {
  return new Promise((resolve,reject)=>{
    wservice.getRecentTransaction(token)
    .then((response)=>{
      
      if (response.statusCode == 200) {
        resolve(response.body.data)
      }else if(response.statusCode == 401){
        reject(response.body.message)
      }else{
        reject(response.body.message)
      }
    })
    .catch(reject)
  })
}
