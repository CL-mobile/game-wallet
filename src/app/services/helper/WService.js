import {Config} from '@common'
var config = Config.WoocommerceConfig
import NetworkHelper from './NetworkHelper'

function WService(){
	this.url = config.endpoint+"/wp-json/wc/v2/"
}

WService.prototype.makeUrl = function(resource,params = null){
	var url = this.url+resource+"?consumer_key="+config.consumer_key+"&consumer_secret="+config.consumer_secret
	if (params) {
		url += "&"+params
	}
	return url
}

WService.prototype.URI = (resource,params = null) =>{
	
	var uri = "http://35.187.251.246/web-ewallet/api/index.php/"+resource
	if (params) {
		uri += "&"+params;
	}
	
	return uri;
}

WService.prototype.getGame = function(gameId, token){

	return NetworkHelper.requestGet(this.URI("game/login/"+gameId),token)
}

WService.prototype.login = function(email, password, tokenID){

	return NetworkHelper.requestPost(this.URI("auth/login"),{email:email, password:password, tokenID:tokenID})
}
WService.prototype.changepassword = function(params,token){
	return NetworkHelper.requestPost(this.URI("profile/updatePassword"),params,token)
}
WService.prototype.changeprofile = function(params,token){
	return NetworkHelper.requestPost(this.URI("profile/edit"),params,token)
}
WService.prototype.forgotPass = function(email){

	return NetworkHelper.requestPost(this.URI("Auth/email_verify"),{email:email})
}
WService.prototype.getPromotionSliders = function(token){

	return NetworkHelper.requestGet(this.URI("banner/promote"),token)
}
WService.prototype.getHomeBalanceInfo = function(token){
	
	return NetworkHelper.requestGet(this.URI("profile"),token)
}
WService.prototype.inputVer = function(email,verification_code){
	return NetworkHelper.requestPost(this.URI("Auth/verification_code"), {email:email,verification_code: verification_code})
}

WService.prototype.resetpassword = function(userid,password,confirm){
	console.log(userid+password+confirm)
	return NetworkHelper.requestPost(this.URI("Auth/reset_password"),  {id:userid,password:password,confirm_password:confirm})

}

WService.prototype.getMenu = function(token){
	return NetworkHelper.requestGet(this.URI("menu"), token)
}

WService.prototype.getBank = function(token){
	return NetworkHelper.requestGet(this.URI("bank"), token)
}

WService.prototype.getRecentTransaction = function(token){
	
	return NetworkHelper.requestGet(this.URI("transaction/recent"), token)
}

WService.prototype.submitDeposit = function(params, token){

	return NetworkHelper.requestPost(this.URI("transaction/deposit"),params, token)
}
WService.prototype.postTranfer = function(params, token){
	console.log(params)
	return NetworkHelper.requestPost(this.URI("transfer/cross"),params, token)
}

WService.prototype.getCategories = function(){
	return NetworkHelper.requestGet(this.makeUrl("products/categories"))
}

WService.prototype.getCategoryById = function(categoryId){
	return NetworkHelper.requestGet(this.makeUrl("products/categories/"+categoryId))
}

WService.prototype.getAllProducts = function(page,per_page){
	return NetworkHelper.requestGet(this.makeUrl("products","page="+page+"&per_page="+per_page))
}

WService.prototype.getProductsByCategory = function(categoryId,page,per_page){
	return NetworkHelper.requestGet(this.makeUrl("products","page="+page+"&per_page="+per_page+"&category="+categoryId))
}

WService.prototype.getRelatedProducts = function(categoryId,productId,page,per_page){
	return NetworkHelper.requestGet(this.makeUrl("products","page="+page+"&per_page="+per_page+"&category="+categoryId+"&exclude=["+productId+"]"))
}

WService.prototype.searchProducts = function(searchText,page,per_page){
	return NetworkHelper.requestGet(this.makeUrl("products","page="+page+"&per_page="+per_page+"&search="+searchText))
}

WService.prototype.getRecentProducts = function(per_page){
	return NetworkHelper.requestGet(this.makeUrl("products","page=1&per_page="+per_page))
}

WService.prototype.getRecentProducts = function(per_page){
	return NetworkHelper.requestGet(this.makeUrl("products","page=1&per_page="+per_page))
}

WService.prototype.getShippingMethods = function(){
	return NetworkHelper.requestGet(this.makeUrl("shipping_methods"))
}

WService.prototype.getPaymentMethods = function(){
	return NetworkHelper.requestGet(this.makeUrl("payment_gateways"))
}

WService.prototype.signUp = function({email, first_name, last_name, password}){
	return NetworkHelper.requestPost(this.makeUrl("customers"),{email, first_name, last_name, password})
}

WService.prototype.signIn = function(email, password){
	return NetworkHelper.requestPost(config.endpoint+"/wp-json/jwt-auth/v1/token",{username: email, password})
}

WService.prototype.getUserId = function(token){
	return NetworkHelper.requestGet(config.endpoint+"/wp-json/wp/v2/users/me",token)
}

WService.prototype.getUserInfo = function(userId){
	return NetworkHelper.requestGet(this.makeUrl("customers/"+userId))
}

WService.prototype.createOrder = function(params){
	return NetworkHelper.requestPost(this.makeUrl("orders"),params)
}

WService.prototype.getMyOrders = function(customer,page,per_page){
	return NetworkHelper.requestGet(this.makeUrl("orders","page="+page+"&per_page="+per_page+"&customer="+customer))
}

module.exports = WService
