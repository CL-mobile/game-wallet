import {Constants} from '@common'
import WService from './helper/WService'
var wservice = new WService()

export const submitDeposit = (params, token)=>{
  
  return new Promise((resolve,reject)=>{
    wservice.submitDeposit(params, token)
    .then((response)=>{
      console.log(response)
      if (response.statusCode == 200) {
        resolve(response.body.data)
      }else if(response.statusCode == 502){
        reject(response.body.message)
      }else{
        reject(response.body.message)
      }
    })
    .catch(reject)
  })
}