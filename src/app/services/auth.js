import {Constants} from '@common'
import WService from './helper/WService'
var wservice = new WService()

export const signUp = (first_name,last_name,email,password)=>{
  return new Promise((resolve,reject)=>{
    wservice.signUp({email, first_name, last_name, password})
    .then((response)=>{
      if (response.statusCode == 201) {
        resolve(response.body)
      }else{
        reject(response.body.message)
      }
    })
    .catch(reject)
  })
}

export const signIn = (email,password)=>{
  return new Promise((resolve,reject)=>{
    wservice.signIn(email,password)
    .then((response)=>{
      console.log("LOGIN: ",response);
      if (response.statusCode == 200) {
        resolve(response.body.token)
      }else if(response.statusCode == 403){
        reject(__.t('Email or password is incorrect'))
      }else{
        reject(response.body.message)
      }
    })
    .catch(reject)
  })
}

export const getCustomerInfo = ()=>{
  return new Promise((resolve,reject)=>{
    if (typeof global.userToken == "undefined" || global.userToken == null || global.userToken.length == 0) {
      return reject("invalid token")
    }
    wservice.getUserId(global.userToken)
    .then((response)=>{
      console.log("getUserId: ",global.userToken);
      if (response.statusCode == 200) {
        let userId = response.body.id
        wservice.getUserInfo(userId)
        .then(({statusCode, body})=>{
          console.log(body);
          if (statusCode == 200) {
            resolve(body)
          }else{
            reject(body.message)
          }
        })
        .catch(reject)
      }else{
        reject(response.body.message)
      }
    })
    .catch(reject)
  })
}

export const login = (email,password,tokenID)=>{
  
  return new Promise((resolve,reject)=>{
    wservice.login(email,password,tokenID)
    .then((response)=>{

      if (response.statusCode == 200) {
        if(response.body.status == false){
          reject(response.body.message)
        }else{
          resolve(response.body.data)
        }
        
      }else{
        reject(response.body.message)
      }
    })
    .catch(reject)
  })
}
export const forgotPass = (email)=>{
  
  return new Promise((resolve,reject)=>{
    wservice.forgotPass(email)
    .then((response)=>{

      if (response.statusCode == 200) {
        if(response.body.status == false){
          reject(response.body.message)
        }else{
          resolve(response.body.data)
        }
        
      }else{
        reject(response.body.message)
      }
    })
    .catch(reject)
  })
}
export const inputVer = (email,verification_code)=>{
  return new Promise((resolve,reject)=>{
    // console.log(email)
    wservice.inputVer(email,verification_code)
    .then((response)=>{
      // console.log(response)
      if (response.statusCode == 200) {
        resolve(response.body.data)
      }else{
        reject(response.body.message)
      }
    })
    .catch(reject)
  })
}

export const resetpassword = (userid,password,confirm)=>{
  return new Promise((resolve,reject)=>{
    // console.log(email)
    wservice.resetpassword(userid,password,confirm)
    .then((response)=>{
      console.log(response)
      if (response.statusCode == 200) {
        resolve(response.body.message)
      }else{
        reject(response.body.message)
      }
    })
    .catch(reject)
  })
}