import {persistCombineReducers} from 'redux-persist'
import storage from 'redux-persist/es/storage'

import authReducers from './auth'
import categoriesReducers from './categories'
import productsReducers from './products'
import productsByCategoryReducers from './productsByCategory'
import cartsReducers from './carts'
import settingsReducers from './settings'
import othersReducers from './others'
import menusReducers from './menus'
import bannerReducers from './banner'
import balanceInfoReducers from './balance'
import bankReducers from './bank'
import depositReducers from './deposit'
import transactionsReducers from './transactions'
import tranferReducers from './tranfer'
import firebaseReducers from './firebase'
import changepassReducers from './changepass'
import changeprofileReducers from './changeprofile'
import gameReducers from './game'

const config = {
  key:'root',
  storage,
  whitelist:["categoriesReducers","cartsReducers","settingsReducers", "authReducers"]
}

const reducers = persistCombineReducers(config,{
  authReducers,
  categoriesReducers,
  productsReducers,
  productsByCategoryReducers,
  cartsReducers,
  settingsReducers,
  othersReducers,
  menusReducers,
  bannerReducers,
  balanceInfoReducers,
  bankReducers,
  depositReducers,
  transactionsReducers,
  tranferReducers,
  firebaseReducers,
  changepassReducers,
  changeprofileReducers,
  gameReducers

})

export default reducers
