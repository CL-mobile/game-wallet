import * as ActionTypes from '@actions/ActionTypes'

const initialState = {
  dataTranfer:[],
  isOpen:true
}

export default function base(state = initialState, action){
  switch(action.type){
    case ActionTypes.POST_TRANFER_PENDING:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:true,
        message:"",
      }
    }
    case ActionTypes.POST_TRANFER_FAIL:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:action.message
      }
    }
    case ActionTypes.POST_TRANFER_SUCCESS:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:action.message
      }
    }

    case ActionTypes.SET_MODAL_TRANFER:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        isOpen:action.isOpen
      }
    }
    default:
      return state
  }
  
}