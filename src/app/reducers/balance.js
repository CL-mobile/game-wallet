import * as ActionTypes from '@actions/ActionTypes'

const initialState = {
    dataBalance:[],isLoadingB:true,Profile:[],isFail:false,dataBalanceDetail:[]
  }

export default function base(state = initialState, action){

  switch(action.type){
    case ActionTypes.GET_HOME_BALANCE_INFO_PENDING:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:true,
        message:"",
        isLoadingB:true,
        isFail:false,
      }
    }
    case ActionTypes.GET_HOME_BALANCE_INFO_FAIL:
    {
      return {
        ...state,...state,
        type:action.type,
        isRequesting:false,
        message:action.message,
        isLoadingB:false,
        isFail:true

      }
    }
    case ActionTypes.GET_HOME_BALANCE_INFO_SUCCESS:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:"",
        dataBalance:action.dataBalance,
        isLoadingB:false,
        Profile:action.Profile,
        dataBalanceDetail:action.dataBalanceDetail,
        isFail:false
      }
    }
    case ActionTypes.CLEAR_DATA_BALANCE:
    {
      return {
        ...state,
        Profile:[],
        dataBalance:[]
      }
    }
    default:
      return state
  }
  
}