import * as ActionTypes from '@actions/ActionTypes'

const initialState = {
    datapass:[]
  }

export default function base(state = initialState, action){
  switch(action.type){
    case ActionTypes.POST_CHANGE_PROFILE_PENDING:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:true,
        message:"",
      }
    }
    case ActionTypes.POST_CHANGE_PROFILE_FAIL:
    {
      return {
        ...state,...state,
        type:action.type,
        isRequesting:false,
        message:action.message
      }
    }
    case ActionTypes.POST_CHANGE_PROFILE_SUCCESS:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:action.message,
      }
    }
    default:
      return state
  }
  
}
