import * as ActionTypes from '@actions/ActionTypes'

const initialState = {
  dataDeposit:[]
}

export default function base(state = initialState, action){
  switch(action.type){
    case ActionTypes.SUBMIT_DEPOSIT_PENDING:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:true,
        message:"",
      }
    }
    case ActionTypes.SUBMIT_DEPOSIT_FAIL:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:action.message
      }
    }
    case ActionTypes.SUBMIT_DEPOSIT_SUCCESS:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:action.dataDeposit.message,
        dataDeposit:action.dataDeposit,
      }
    }
    default:
      return state
  }
  
}
