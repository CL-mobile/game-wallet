import * as ActionTypes from '@actions/ActionTypes'

const initialState = {
    dataGame:[]
  }

export default function base(state = initialState, action){
  switch(action.type){
    case ActionTypes.GET_GAME_PENDING:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:true,
        message:"",
      }
    }
    case ActionTypes.GET_GAME_FAIL:
    {
      return {
        ...state,...state,
        type:action.type,
        isRequesting:false,
        message:action.message
      }
    }
    case ActionTypes.GET_GAME_SUCCESS:
    {

      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:"",
        dataGame:action.dataGame
      }
    }
    default:
      return state
  }
  
}
