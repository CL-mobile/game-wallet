import * as ActionTypes from '@actions/ActionTypes'

export default function base(state = {}, action){
  switch(action.type){
    case ActionTypes.SET_LANGUAGE:
    {
      return {
        ...state,
        lang:action.lang
      }
    }
    default:
      return state
  }
}
