import * as ActionTypes from '@actions/ActionTypes'

const initialState = {
    dataMenu:[],
    dataPopular:[]
  }

export default function base(state = initialState, action){
  switch(action.type){
    case ActionTypes.GET_MENU_PENDING:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:true,
        message:"",
      }
    }
    case ActionTypes.GET_MENU_FAIL:
    {
      return {
        ...state,...state,
        type:action.type,
        isRequesting:false,
        message:action.message
      }
    }
    case ActionTypes.GET_MENU_SUCCESS:
    {

      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:"",
        dataMenu:action.dataMenu,
        dataPopular:action.dataPopular
      }
    }
    case ActionTypes.SET_ACTIVE_MENU:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:"",
        dataMenu:action.dataMenu
      }
    }
    case ActionTypes.CLEAR_DATA_MENU:
    {
      return {
        ...state,
        dataMenu:[]
      }
    }
    default:
      return state
  }
  
}
