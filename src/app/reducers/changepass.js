import * as ActionTypes from '@actions/ActionTypes'

const initialState = {
    datapass:[]
  }

export default function base(state = initialState, action){
  switch(action.type){
    case ActionTypes.POST_CHANGE_PASS_PENDING:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:true,
        message:"",
      }
    }
    case ActionTypes.POST_CHANGE_PASS_FAIL:
    {
      return {
        ...state,...state,
        type:action.type,
        isRequesting:false,
        message:action.message
      }
    }
    case ActionTypes.POST_CHANGE_PASS_SUCCESS:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:action.message,
      }
    }
    default:
      return state
  }
  
}
