import * as ActionTypes from '@actions/ActionTypes'

export default function base(state = {}, action){
  switch(action.type){
    case ActionTypes.SET_PRODUCT_MESSAGE:
    {
      return {
        ...state,
        isProductMessage:action.isProductMessage
      }
    }
    default:
      return state
  }
}
