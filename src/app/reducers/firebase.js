import * as ActionTypes from '@actions/ActionTypes'

const initialState = {
    dataFirebase:[]
  }

export default function base(state = initialState, action){
  switch(action.type){
    case ActionTypes.GET_FIREBASE_PENDING:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:true,
        message:"",
      }
    }
    case ActionTypes.GET_FIREBASE_FAIL:
    {
      return {
        ...state,...state,
        type:action.type,
        isRequesting:false,
        message:action.message
      }
    }
    case ActionTypes.GET_FIREBASE_SUCCESS:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:"",
        dataFirebase:action.dataFirebase
      }
    }
    default:
      return state
  }
  
}
