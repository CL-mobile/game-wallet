import * as ActionTypes from '@actions/ActionTypes'

const initialState = {
    dataBanner:[],
  }

export default function base(state = initialState, action){
  switch(action.type){
    case ActionTypes.GET_PROMOTIONS_SLIDERS_PENDING:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:true,
        message:"",
      }
    }
    case ActionTypes.GET_PROMOTIONS_SLIDERS_FAIL:
    {
      return {
        ...state,...state,
        type:action.type,
        isRequesting:false,
        message:action.message
      }
    }
    case ActionTypes.GET_PROMOTIONS_SLIDERS_SUCCESS:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:"",
        dataBanner:action.dataBanner,
      }
    }
    case ActionTypes.CLEAR_DATA_BANNER:
    {
      return {
        ...state,
        dataBanner:[]
      }
    }
    default:
      return state
  }
  
}