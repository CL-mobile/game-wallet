import * as ActionTypes from '@actions/ActionTypes'

const initialState = {
    dataBank:[]
  }

export default function base(state = initialState, action){
  switch(action.type){
    case ActionTypes.GET_BANK_PENDING:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:true,
        message:"",
      }
    }
    case ActionTypes.GET_BANK_FAIL:
    {
      return {
        ...state,...state,
        type:action.type,
        isRequesting:false,
        message:action.message
      }
    }
    case ActionTypes.GET_BANK_SUCCESS:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:"",
        dataBank:action.dataBank
      }
    }
    default:
      return state
  }
  
}
