import * as ActionTypes from '@actions/ActionTypes'

const initialState = {
    dataRecentTransaction:[]
  }

export default function base(state = initialState, action){
  switch(action.type){
    case ActionTypes.GET_RECENT_TRANSACTION_PENDING:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:true,
        message:"",
      }
    }
    case ActionTypes.GET_RECENT_TRANSACTION_FAIL:
    {
      return {
        ...state,...state,
        type:action.type,
        isRequesting:false,
        message:action.message
      }
    }
    case ActionTypes.GET_RECENT_TRANSACTION_SUCCESS:
    {
      return {
        ...state,
        type:action.type,
        isRequesting:false,
        message:"",
        dataRecentTransaction:action.dataRecentTransaction
      }
    }
    default:
      return state
  }
  
}
