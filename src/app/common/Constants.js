import {Dimensions} from 'react-native'
import Icons from './Icons'



const Constants = {
  Screen:{
    Home : 'Home',
    Deals : 'Deals',
    Search : 'Search',
    Carts : 'Carts',
    MyProfile : 'MyProfile',
    MyWishList : 'MyWishList',
    Languages : 'Languages',
    MyAddress : 'MyAddress',
    Launch : 'Launch',
    Detail : 'Detail',
    ProductsByCategory : 'ProductsByCategory',
    SetLanguage : 'SetLanguage',
    SignIn : 'SignIn',
    SignUp : 'SignUp',
    Feedback : 'Feedback',
    ShippingInfo : 'ShippingInfo',
    ShippingAddress : 'ShippingAddress',
    PaymentInfo: 'PaymentInfo',
    AddAddress: 'AddAddress',
    MyOrders: 'MyOrders',
    ActiveCode:'ActiveCode',
    Balance : 'Balance',
    Account : 'Account',
    Register: 'Register',
    Login:'Login',
    ResetPassword:'ResetPassword',
    Transaction:'Transaction',
    Deposit:'Deposit',
    GameProduct:'GameProduct',
    TransactionHistory:'TransactionHistory',
    ForgotPass:'ForgotPass',
    DepositFirst:'DepositFirst',
    DepositSecond: 'DepositSecond',
    DepositThird: 'DepositThird',
    DepositConfirmStep:'DepositConfirmStep',
    WalletDetail: 'WalletDetail',
    WalletTransfer:"WalletTransfer",
    TransationHistory:"TransactionHistory",
    AccountSetting:"AccountSetting",
    ChangePassword:"ChangePassword",
    EditProfile:"EditProfile",
    Game:"Game",
    GameAll:"GameAll"
  },
  ScreenSize:Dimensions.get('window'),
  EventEmitterName:{
    OpenDrawer:'OpenDrawer',
    OpenPage:'OpenPage',
    onSearch:'onSearch',
    onAdd:'onAdd'
  },
  Languages : [
    {
      code:'en',
      text:'English',
      icon:Icons.EnFlag
    },
    {
      code:'ar',
      text:'Arabic',
      icon:Icons.ArFlag
    }
  ],
  FontFamily:'AvertaStd-Regular', //Bookerly-Regular
  Api:{
    Limit:20
  }
}

export default Constants
