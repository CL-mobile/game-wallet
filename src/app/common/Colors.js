const Colors = {
  AppColor:'#32f269',
  LeftMenu:'#b262d2',
  Gray:'rgba(178,178,178,1)',
  Blue:'rgba(70,120,172,1)',
  DarkGray:'rgba(64,64,64,1)',
  LightGray:'rgba(207,206,204,0.5)',
  LighterGray:'#f2f2f2',
  Red:'rgba(208,83,67,1)',
  Green:'rgba(55,203,123,1)',
  DarkBlue:'rgba(17,107,213,1)'
}

export default Colors
