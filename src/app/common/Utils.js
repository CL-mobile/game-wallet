import {Linking} from 'react-native'
import Config from './Config'
import moment from 'moment'

export const getCustomAttribute = (customAttributes,attribute)=>{
  var value = null
  if (typeof customAttributes != 'undefined' && customAttributes.length > 0) {
    customAttributes.forEach((item)=>{
      if (item.attribute_code == attribute) {
        value = item.value
        return
      }
    })
  }
  return value
}

export const convertToIDR = (amount)=>{
  var	reverse = amount.toString().split('').reverse().join(''),
	ribuan 	= reverse.match(/\d{1,3}/g);
	ribuan	= ribuan.join('.').split('').reverse().join('');
  return ribuan
}

export const convertFormatAmount = (amount, currency)=>{

  if(currency == "IDR"){
    
    var	reverse = amount.toString().split('').reverse().join(''),
    ribuan 	= reverse.match(/\d{1,3}/g);
    ribuan	= ribuan.join('.').split('').reverse().join('');
    return ribuan
    
  }else if(currency == "USD"){
    
    var number = amount;
    return (number + "").replace(".", ",");
    
  }
  
}

export const getDataAmount = (currency)=>{

  var data = []
  if(currency == "IDR"){
    data = [
      {key:'100000', label:"IDR 100.000"},
      {key:'250000', label:"IDR 250.000"},
      {key:'500000', label:"IDR 500.000"},
      {key:'750000', label:"IDR 750.000"},
      {key:'1000000', label:"IDR 1000.000"}
    ]
  }else if(currency == "USD"){
    data = [
      {key:'7.18', label:"$7,18"},
      {key:'17.96', label:"$17,96"},
      {key:'35.91', label:"$35,91"},
      {key:'53.87', label:"$53,87"},
      {key:'71.82', label:"71,82"}
    ]
  }

  return data
  
}

export const convertCurrency = (currency)=>{

  var label = ""

  if(currency == "IDR"){
    
    label = "IDR"
    
  }else if(currency == "USD"){
    
    label = "$"
    
  }
  
  return label
}

export const convertDate = (date, type)=>{
  let output;
  let dateTimeParts= date.split(/[/ :]/);
  dateTimeParts[1]--;
  const dateObject = new Date(...dateTimeParts);

  if(type == "date"){
    output = dateObject.getDate();
  }else if(type == "year"){
    output = dateObject.getFullYear();
  }else if(type == "days"){
    let days = ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"];
    output = days[dateObject.getDay()];
  }else if(type == "months"){
    let months = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
    output = months[dateObject.getMonth()];
  }
  
  return output;
}

export const isEmpty = (obj)=>{

  for(var key in obj) {
    if(obj.hasOwnProperty(key))
        return false;
  }
  return true;
}

export const getProductImageUrl = (item,attribute = "thumbnail")=>{
  if (item.images != undefined && item.images.length > 0) {
    return item.images[0].src
  }
  return "http://saveabandonedbabies.org/wp-content/uploads/2015/08/default.png"
}

export const getCategoryImageUrl = (item)=>{
  if (item.image != null) {
    return item.image.src
  }
  return "http://saveabandonedbabies.org/wp-content/uploads/2015/08/default.png"
}

export const openUrl = (url) => {
  Linking.canOpenURL(url).then(supported => {
    if (!supported) {
      alert('Can\'t handle url: ' + url);
    } else {
      return Linking.openURL(url);
    }
  }).catch(err => console.log('An error occurred', err));
}

export const getCurrentPrice = (product)=>{
  if (product.on_sale) {
    return product.sale_price
  }
  return product.price
}
