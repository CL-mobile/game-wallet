import {Dimensions} from 'react-native'
import Icons from './Icons'
//import PayPal from 'react-native-paypal-wrapper'

const Config = {
  dataProduct:[
    {
      id:1,
      category:'Sports',
      active:false,
      icon:Icons.Sports,
      subCategory:[
        {
          name:"Sports 1",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ]
        },
        {
          name:"Sports 2",
          product:[
            {
              title:"Angry Birds Epic",
              image:"https://vignette.wikia.nocookie.net/logopedia/images/3/3b/0x0ss-85_%281%29.png/revision/latest?cb=20160701171308"
            },
            {
              title:"Hago",
              image:"https://is3-ssl.mzstatic.com/image/thumb/Purple118/v4/c0/18/f1/c018f13c-3aae-61b0-5561-0804d9656d17/AppIcon-1x_U007emarketing-85-220-0-9.png/246x0w.jpg"
            },
            {
              title:"Plant of Zombies",
              image:"https://upload.wikimedia.org/wikipedia/en/thumb/2/2f/Plants_vs._Zombies_Heroes.png/220px-Plants_vs._Zombies_Heroes.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
          ]
        },
        {
          name:"Sports 3",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ],
        },
        {
          name:"Sports 4",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ]
        }
      ]
    },
    {
      id:2,
      category:'Casino',
      active:false,
      icon:Icons.Casino,
      subCategory:[
        {
          name:"Casino 1",
          product:[
            {
              title:"Angry Birds Epic",
              image:"https://vignette.wikia.nocookie.net/logopedia/images/3/3b/0x0ss-85_%281%29.png/revision/latest?cb=20160701171308"
            },
            {
              title:"Hago",
              image:"https://is3-ssl.mzstatic.com/image/thumb/Purple118/v4/c0/18/f1/c018f13c-3aae-61b0-5561-0804d9656d17/AppIcon-1x_U007emarketing-85-220-0-9.png/246x0w.jpg"
            },
            {
              title:"Plant of Zombies",
              image:"https://upload.wikimedia.org/wikipedia/en/thumb/2/2f/Plants_vs._Zombies_Heroes.png/220px-Plants_vs._Zombies_Heroes.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
          ]
        },
        {
          name:"Casino 2",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ]
        },
        {
          name:"Casino 3",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ],
        },
        {
          name:"Casino 4",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ]
        }
      ]
    },
    {
      id:3,
      category:'E-Games',
      icon:Icons.Egames,
      active:false,
      subCategory:[
        {
          name:"E-Games 1",
          product:[
            {
              title:"Angry Birds Epic",
              image:"https://vignette.wikia.nocookie.net/logopedia/images/3/3b/0x0ss-85_%281%29.png/revision/latest?cb=20160701171308"
            },
            {
              title:"Hago",
              image:"https://is3-ssl.mzstatic.com/image/thumb/Purple118/v4/c0/18/f1/c018f13c-3aae-61b0-5561-0804d9656d17/AppIcon-1x_U007emarketing-85-220-0-9.png/246x0w.jpg"
            },
            {
              title:"Plant of Zombies",
              image:"https://upload.wikimedia.org/wikipedia/en/thumb/2/2f/Plants_vs._Zombies_Heroes.png/220px-Plants_vs._Zombies_Heroes.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
          ]
        },
        {
          name:"E-Games 2",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ]
        },
        {
          name:"E-Games 3",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ],
        },
        {
          name:"E-Games 4",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ]
        }
      ]
    },
      {
      id:5,
      category:'Poker',
      active:false,
      icon:Icons.Poker,
      subCategory:[
        {
          name:"Poker 1",
          product:[
            {
              title:"Angry Birds Epic",
              image:"https://vignette.wikia.nocookie.net/logopedia/images/3/3b/0x0ss-85_%281%29.png/revision/latest?cb=20160701171308"
            },
            {
              title:"Hago",
              image:"https://is3-ssl.mzstatic.com/image/thumb/Purple118/v4/c0/18/f1/c018f13c-3aae-61b0-5561-0804d9656d17/AppIcon-1x_U007emarketing-85-220-0-9.png/246x0w.jpg"
            },
            {
              title:"Plant of Zombies",
              image:"https://upload.wikimedia.org/wikipedia/en/thumb/2/2f/Plants_vs._Zombies_Heroes.png/220px-Plants_vs._Zombies_Heroes.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
          ]
        },
        {
          name:"Poker 2",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ]
        },
        {
          name:"Poker 3",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ],
        },
        {
          name:"Poker 4",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ]
        }
      ]
    },
    {
      id:6,
      category:'Racing',
      active:false,
      icon:Icons.Racing,
      subCategory:[
        {
          name:"Racing 1",
          product:[
            {
              title:"Angry Birds Epic",
              image:"https://vignette.wikia.nocookie.net/logopedia/images/3/3b/0x0ss-85_%281%29.png/revision/latest?cb=20160701171308"
            },
            {
              title:"Hago",
              image:"https://is3-ssl.mzstatic.com/image/thumb/Purple118/v4/c0/18/f1/c018f13c-3aae-61b0-5561-0804d9656d17/AppIcon-1x_U007emarketing-85-220-0-9.png/246x0w.jpg"
            },
            {
              title:"Plant of Zombies",
              image:"https://upload.wikimedia.org/wikipedia/en/thumb/2/2f/Plants_vs._Zombies_Heroes.png/220px-Plants_vs._Zombies_Heroes.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
          ]
        }, {
          name:"Racing 4",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ]
        }
      ]
    },
    {
      id:7,
      active:false,
      category:'Lottery',
      icon:Icons.Lottery,
      subCategory:[
        {
          name:"Lottery 1",
          product:[
            {
              title:"Angry Birds Epic",
              image:"https://vignette.wikia.nocookie.net/logopedia/images/3/3b/0x0ss-85_%281%29.png/revision/latest?cb=20160701171308"
            },
            {
              title:"Hago",
              image:"https://is3-ssl.mzstatic.com/image/thumb/Purple118/v4/c0/18/f1/c018f13c-3aae-61b0-5561-0804d9656d17/AppIcon-1x_U007emarketing-85-220-0-9.png/246x0w.jpg"
            },
            {
              title:"Plant of Zombies",
              image:"https://upload.wikimedia.org/wikipedia/en/thumb/2/2f/Plants_vs._Zombies_Heroes.png/220px-Plants_vs._Zombies_Heroes.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
          ]
        },
        {
          name:"Lottery 2",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ]
        },
        {
          name:"Lottery 3",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ],
        },
        {
          name:"Lottery 4",
          product:[
            {
              title:"PUBG",
              image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
            },
            {
              title:"Fortnite",
              image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
            },
            {
              title:"Free Fire",
              image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
            },
            {
              title:"ROS",
              image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
            },
          ]
        }
      ]
    }
  ],
  WoocommerceConfig:{
    endpoint : 'https://hubay.net/wordpress',
    consumer_key : 'ck_9d13ee9988b0587d0de0009a6488867ee0f7c92b',
    consumer_secret : 'cs_35ae33d62b8a0a3418aa6ccf9e5296f1bf4a8424'
  },
  Promotions : [
    {
      image:'https://hubay.net/wordpress/wp-content/uploads/2018/07/slider3.jpg',
      categoryId: 89
    },
    {
      image:'https://hubay.net/wordpress/wp-content/uploads/2018/07/slider2.jpg',
      categoryId: 93
    },
    {
      image:'https://hubay.net/wordpress/wp-content/uploads/2018/07/slider1.jpg',
      categoryId: 126
    }
  ],
  Brands : [
    {
      url:'https://hubay.net/wordpress',
      image:'https://hubay.net/wordpress/img/brand3.gif'
    },
    {
      url:'https://hubay.net/wordpress',
      image:'https://hubay.net/wordpress/img/brand2.gif'
    },
    {
      url:'https://hubay.net/wordpress',
      image:'https://hubay.net/wordpress/img/Shop1.jpg'
    },
    {
      url:'https://hubay.net/wordpress',
      image:'https://hubay.net/wordpress/img/brand1.gif'
    },
  ],
  OneSignalAppId:'341a03aa-c95a-45b4-b2df-5be1d09583cc'
}

export default Config
