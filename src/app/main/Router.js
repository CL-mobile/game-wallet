import React from 'react'
import {Image} from 'react-native'
import {TabBarItem,BottomContainer} from '@components'
import {FluidNavigator} from 'react-navigation-fluid-transitions'

import {createStackNavigator,createBottomTabNavigator,createTabNavigator} from 'react-navigation'
import {Constants,Colors,Icons} from '@common'
import HomeScreen from './screens/HomeScreen'
import DealsScreen from './screens/DealsScreen'
import SearchScreen from './screens/SearchScreen'
import CartsScreen from './screens/CartsScreen'
import MyProfileScreen from './screens/MyProfileScreen'
import MyWishListScreen from './screens/MyWishListScreen'
import LanguagesScreen from './screens/LanguagesScreen'
import MyAddressScreen from './screens/MyAddressScreen'
import LaunchScreen from './screens/LaunchScreen'
import DetailScreen from './screens/DetailScreen'
import ProductsByCategoryScreen from './screens/ProductsByCategoryScreen'
import SetLanguageScreen from './screens/SetLanguageScreen'
import SignInScreen from './screens/SignInScreen'
import SignUpScreen from './screens/SignUpScreen'
import FeedbackScreen from './screens/FeedbackScreen'
import ShippingInfoScreen from './screens/ShippingInfoScreen'
import ShippingAddressScreen from './screens/ShippingAddressScreen'
import PaymentInfoScreen from './screens/PaymentInfoScreen'
import AddAddressScreen from './screens/AddAddressScreen'
import MyOrdersScreen from './screens/MyOrdersScreen'
import RegisterScreen from './screens/RegisterScreen'
import AccountScreen from './screens/AccountScreen'
import BalanceScreen from './screens/BalanceScreen'
import LoginScreen from './screens/LoginScreen'
import TransactionScreen from './screens/TransactionScreen'
import DepositScreen from './screens/DepositScreen'
import GameProductScreen from './screens/GameProductScreen'
import TransactionHistoryScreen from './screens/TransactionHistoryScreen'
import ForgotPassScreen from './screens/ForgotPassScreen'
import ActiveCodeScreen from './screens/ActiveCodeScreen'
import ResetPasswordScreen from './screens/ResetPasswordScreen'
//DEPOSIT
import FirstDeposit from "./screens/Deposit/FirstStepScreen";
import SecondDeposit from "./screens/Deposit/SecondStepScreen";
import ThirdDeposit from './screens/Deposit/ThirdStepScreen';
import ConfirmStep from './screens/Deposit/ConfirmStepScreen';

import WalletDetailScreen from "./screens/WalletDetailScreen";
import WalletTransferScreen from "./screens/WalletTransferScreen";
import AccountSettingScreen from "./screens/AccountSettingScreen";
import EditProfileScreen from "./screens/EditProfileScreen";
import ChangePasswordScreen from "./screens/ChangePasswordScreen";

import GameScreen from "./screens/GameScreen";
import GameAllScreen from "./screens/GameAllScreen";

const stackNavigatorConfiguration = {
  mode:'card',
  navigationOptions:{
    header:null,
    headerStyle:{backgroundColor:'#1c1c1c',borderBottomWidth:0},
    headerTintColor:'black',
    headerTitleStyle:{fontSize:16,fontFamily:Constants.FontFamily,fontWeight:'bold'}

  }
}

// ANIMATION

const DepositAnimation = {}
//DepositAnimation [Constants.Screen.Account] = {screen:AccountScreen}
DepositAnimation [ Constants.Screen.DepositFirst ] = {screen: FirstDeposit}
DepositAnimation [ Constants.Screen.DepositSecond ] = {screen: SecondDeposit}
DepositAnimation [ Constants.Screen.DepositThird ] = {screen: ThirdDeposit }
DepositAnimation [Constants.Screen.DepositConfirmStep]={screen:ConfirmStep}
const DepositStack = FluidNavigator(DepositAnimation)

//Balance Detail = {}
const BalanceDetail = {}
BalanceDetail [ Constants.Screen.WalletDetail ] = {screen : WalletDetailScreen}
BalanceDetail [Constants.Screen.DepositFirst] = {screen: DepositStack}
//BalanceDetail [ Constants.Screen.Account ] = {screen:AccountScreen}

const BalanceStack = FluidNavigator(BalanceDetail)

//Wallet Transfer
const WalletTransfer= {}
WalletTransfer [ Constants.Screen.WalletTransfer ] = {screen:WalletTransferScreen}

const WalletTransferStack = FluidNavigator(WalletTransfer)

//Transaction History
const TransactionHistory = {}
TransactionHistory [ Constants.Screen.TransactionHistory ] = {screen:TransactionHistoryScreen}

//Wallet Transfer
const AccountSetting= {}
AccountSetting [ Constants.Screen.AccountSetting ] = {screen:AccountSettingScreen}
AccountSetting [ Constants.Screen.EditProfile ] = {screen:EditProfileScreen}
AccountSetting [ Constants.Screen.ChangePassword ] = {screen:ChangePasswordScreen}
const AccountSettingStack = FluidNavigator(AccountSetting)

const TransactionHistoryStack = FluidNavigator(TransactionHistory)

const homeTabScreens = {}
homeTabScreens[Constants.Screen.Home] = {screen:HomeScreen}
homeTabScreens[Constants.Screen.GameProduct] = {screen:GameProductScreen}
homeTabScreens[Constants.Screen.GameAll] = {screen:GameAllScreen}
homeTabScreens[Constants.Screen.Login] ={screen:LoginScreen}
homeTabScreens[Constants.Screen.DepositConfirmStep]={screen:ConfirmStep}
const homeStack = createStackNavigator(homeTabScreens,stackNavigatorConfiguration)

const balanceTabScreens = {}
balanceTabScreens[Constants.Screen.Balance] = {screen:BalanceScreen}
const balanceStack = createStackNavigator(balanceTabScreens,stackNavigatorConfiguration)

const ShadowLogin={}
ShadowLogin[Constants.Screen.Login]={screen:LoginScreen}
ShadowLogin[Constants.Screen.Home] = {screen:homeStack}
const shadowStack=createStackNavigator(ShadowLogin,stackNavigatorConfiguration,{navigationOptions:{tabBarVisible:false}})
shadowStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible;
console.log(navigation.state.routes.length)
console.log('asep')
  if (navigation.state.routes.length === 1) {
    navigation.state.routes.map(route => {
      console.log(route.routeName)
      if (route.routeName === "Home") {
        tabBarVisible = true;
      } else {
        tabBarVisible = false;
        
      }
    });
  }

  return {
    tabBarVisible
  };
};

const accountTabScreens = {}
accountTabScreens[Constants.Screen.Account] = {screen:AccountScreen}
accountTabScreens[Constants.Screen.Login]={screen:shadowStack}
accountTabScreens[Constants.Screen.Transaction]={screen:TransactionScreen}
accountTabScreens[Constants.Screen.TransactionHistory]={screen:TransactionHistoryScreen}
const accountStack =  createStackNavigator(accountTabScreens,stackNavigatorConfiguration)

// This code let you hide the bottom app bar when "CustomHide" is rendering
accountStack.navigationOptions = ({ navigation }) => {
  let tabBarVisible;
console.log(navigation.state.routes.length)
console.log('asep')
  if (navigation.state.routes.length === 1) {
    navigation.state.routes.map(route => {
      console.log(route.routeName)
      if (route.routeName === "Login") {
        tabBarVisible = false;
      } else {
        tabBarVisible = true;
      }
    });
  }

  return {
    tabBarVisible
  };
};
const dealsTabScreens = {}
dealsTabScreens[Constants.Screen.Deals] = {screen:DealsScreen}
dealsTabScreens[Constants.Screen.ProductsByCategory] = {screen:ProductsByCategoryScreen}
const dealsStack = createStackNavigator(dealsTabScreens,stackNavigatorConfiguration)

const searchTabScreens = {}
searchTabScreens[Constants.Screen.Search] = {screen:SearchScreen}
searchTabScreens[Constants.Screen.Detail] = {screen:DetailScreen}
const searchStack = createStackNavigator(searchTabScreens,stackNavigatorConfiguration)

const cartsTabScreens = {}
cartsTabScreens[Constants.Screen.Carts] = {screen:CartsScreen}
cartsTabScreens[Constants.Screen.ShippingAddress] = {screen:ShippingAddressScreen}
cartsTabScreens[Constants.Screen.ShippingInfo] = {screen:ShippingInfoScreen}
cartsTabScreens[Constants.Screen.PaymentInfo] = {screen:PaymentInfoScreen}
const cartsStack = createStackNavigator(cartsTabScreens,stackNavigatorConfiguration)

const profileTabScreens = {}
profileTabScreens[Constants.Screen.MyProfile] = {screen:MyProfileScreen}
profileTabScreens[Constants.Screen.MyWishList] = {screen:MyWishListScreen}
profileTabScreens[Constants.Screen.Languages] = {screen:LanguagesScreen}
profileTabScreens[Constants.Screen.MyAddress] = {screen:MyAddressScreen}
profileTabScreens[Constants.Screen.Feedback] = {screen:FeedbackScreen}
profileTabScreens[Constants.Screen.AddAddress] = {screen:AddAddressScreen}
profileTabScreens[Constants.Screen.MyOrders] = {screen:MyOrdersScreen}
const profileStack = createStackNavigator(profileTabScreens,stackNavigatorConfiguration)

const tabScreens = {}
tabScreens[Constants.Screen.Home] = {screen:homeStack}
tabScreens[Constants.Screen.Balance] = {screen:balanceStack}
tabScreens[Constants.Screen.Search] = {screen:searchStack}
tabScreens[Constants.Screen.Carts] = {screen:cartsStack}
tabScreens[Constants.Screen.Account] = {screen:accountStack}

const screens = {}
screens[Constants.Screen.Launch] = {screen:LaunchScreen}
screens[Constants.Screen.SignIn] = {screen:SignInScreen}
screens[Constants.Screen.SignUp] = {screen:SignUpScreen}
screens[Constants.Screen.SetLanguage] = {screen:SetLanguageScreen}

const BottomTabNaviagtor = {}
BottomTabNaviagtor[Constants.Screen.Home] = { screen: homeStack}
BottomTabNaviagtor[Constants.Screen.Search] = { screen: searchStack }
BottomTabNaviagtor[Constants.Screen.Account] = { screen: accountStack }

const BottomTabStack = createTabNavigator(BottomTabNaviagtor, {
  navigationOptions: ({ navigation }) => ({
    tabBarIcon: ({ tintColor }) => {
        const { routeName } = navigation.state
        var icon = null
        switch (routeName) {
            case Constants.Screen.Home:
                icon = Icons.Home2
                break;
            case Constants.Screen.Search:
                icon = Icons.Games
                break;
            case Constants.Screen.Account:
                icon = Icons.Account
                break;
            default:
                return null
        }
        return <TabBarItem icon = { icon }
        tintColor = { tintColor }
        routeName = { routeName }
        />
    }
  }),
  tabBarPosition: 'bottom',
  swipeEnabled: false,
  tabBarComponent:BottomContainer,
  tabBarOptions:{
    showIcon: true,
    showLabel: false,
    activeTintColor:Colors.AppColor,
    inactiveTintColor: 'white',
    style:{
      backgroundColor:'transparent',
      borderTopColor:'white',
      borderTopWidth:0.3
    },
    indicatorStyle: {
      backgroundColor: 'transparent'
    },
  }
})

const LoginTabScreen ={}
LoginTabScreen[Constants.Screen.Login] ={screen:LoginScreen}
LoginTabScreen[Constants.Screen.Account]={screen:accountStack}
LoginTabScreen[Constants.Screen.Register]={screen:RegisterScreen}
LoginTabScreen[Constants.Screen.Game]={screen:GameScreen}
LoginTabScreen[Constants.Screen.ForgotPass]={screen:ForgotPassScreen}
LoginTabScreen[Constants.Screen.Home] = {screen:BottomTabStack}
LoginTabScreen[Constants.Screen.DepositFirst]  = {screen:DepositStack}
LoginTabScreen[Constants.Screen.WalletDetail] = {screen:BalanceStack}
LoginTabScreen [ Constants.Screen.WalletTransfer ] = {screen:WalletTransferStack}
LoginTabScreen [Constants.Screen.TransactionHistory] = {screen:TransactionHistoryStack}
LoginTabScreen [ Constants.Screen.AccountSetting ] = {screen:AccountSettingStack}
LoginTabScreen[Constants.Screen.ActiveCode]={screen:ActiveCodeScreen}
LoginTabScreen[Constants.Screen.ResetPassword]={screen:ResetPasswordScreen}
const mainTab = createBottomTabNavigator(tabScreens,{
  navigationOptions:({navigation}) => ({
    tabBarIcon: ({tintColor}) => {
      const {routeName} = navigation.state
      var icon = null
      switch (routeName) {
        case Constants.Screen.Home:
        icon = Icons.Home2
        break;
        case Constants.Screen.Search:
        icon = Icons.Search
        break;
        case Constants.Screen.Account:
        icon = Icons.Account
        break;
        default:
          return null
      }

      return <TabBarItem icon={icon} tintColor={tintColor} routeName={routeName}/>
    }
  }),
  tabBarOptions:{
    showLabel:false,
    activeTintColor:Colors.AppColor,
    inactiveTintColor: 'white',
    style:{
      backgroundColor:'#1c1c1c',
      height:60,
      borderTopColor:'white',
      borderTopWidth:0.3
    }
  }
})



export default createStackNavigator({
  ...LoginTabScreen,
  default:{
    screen:BottomTabStack
  }
},{
  headerMode:'none',
  navigationOptions:{
    gesturesEnabled:false
  }
})
