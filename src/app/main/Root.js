import React from 'react'
import {Provider} from 'react-redux'
import {AsyncStorage} from 'react-native';
import {createStore, applyMiddleware, compose} from 'redux'
import thunkMiddleware from 'redux-thunk'

import reducers from '../reducers'
import App from './App'

import {persistStore} from 'redux-persist'
import {PersistGate} from 'redux-persist/es/integration/react'


import firebase from 'react-native-firebase';

const middleware = [thunkMiddleware]
const store = compose(applyMiddleware(...middleware))(createStore)(reducers)

let persistor = persistStore(store)

class Root extends React.Component {
  
  async checkPermission(){

    firebase.messaging().hasPermission()
      .then(enabled => {
        if(enabled){
          this.getToken();
        }else{
          this.requestPermission();
        }
      })
  }

  async requestPermission(){
  
    firebase.messaging().requestPermission()
      .then(() => {
        this.getToken();
      })
      .catch(error => {
        console.log('permission rejected')
      })
  }

  async getToken(){
    let fcmToken = await AsyncStorage.getItem('fcmToken')
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  componentDidMount = async () => {
    this.checkPermission()
  }

  render() {
    return (
      <Provider store={store}>
        <PersistGate persistor={persistor}>

          <App />

        </PersistGate>
      </Provider>
    )
  }
}

export default Root
