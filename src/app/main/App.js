import React from 'react'
import {StatusBar,I18nManager} from 'react-native'
import Router from './Router'
import {Languages,Constants,Global} from '@common'
import I18n from 'react-native-i18n'
import Drawer from 'react-native-drawer'
import {LeftMenu} from '@components'
class App extends React.Component {
  constructor(props){
    super(props)
    this.setupI18n()
    StatusBar.setBarStyle('light-content',true)

    this.state = {
      show:false
    }
  }

  render(){
    return (
      
          <Router />
    )
  }

  componentDidMount(){
    this.dawerOpenListener = Global.EventEmitter.addListener(Constants.EventEmitterName.OpenDrawer,this.openDawer)
  }

  componentWillUnmount(){
    this.dawerOpenListener.remove()
  }

  openDawer = ()=>{
    this.setState({show:true})
  }

  onClose = ()=>{
    this.setState({show:false})
  }

  onOpenPage = (item)=>{
    this.setState({show:false},()=>{
      Global.EventEmitter.emit(Constants.EventEmitterName.OpenPage,item)
    })
  }

  setupI18n = ()=>{
    I18n.fallbacks = true
    I18n.translations = Languages
    global.__ = I18n
  }
}

const drawerStyles = {
  drawer:{shadowColor:'#000',shadowOpacity:0.8,shadowRadius:3},
  main:{paddingLeft:3}
}
export default App
