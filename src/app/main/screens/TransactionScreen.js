import React from 'react'
import {Transaction} from '@pages'
import {NavTitle,TabBarItem} from '@components'
import {Icons} from '@common'

class TransactionScreen extends React.Component {
  render(){
    const {navigation} = this.props
    return <Transaction navigation={navigation} goBack={()=>navigation.goBack()}/>
  }
}

export default TransactionScreen
