import React from 'react'
import {Register} from '@pages'
import {NavButton,NavTitle,TabBarItem} from '@components'
import {Icons} from '@common'

class RegisterScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
    header:null,
  })

  render(){
    const {navigation} = this.props
    return <Register navigation={navigation} />
  }
}

export default RegisterScreen;