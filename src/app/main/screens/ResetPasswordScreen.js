import React from 'react'
import {ResetPassword} from '@pages'
import {NavButton,NavTitle,TabBarItem} from '@components'
import {Icons,Constants} from '@common'

class ResetPasswordScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
    header:null,
  })

  render(){
    const {navigation} = this.props
    return <ResetPassword navigation={navigation} 
        showHome={()=>navigation.navigate(Constants.Screen.Home)}
        showRegister={()=>navigation.navigate(Constants.Screen.Register)}
        showLogin={()=>navigation.navigate(Constants.Screen.Login)}
    />
  }
}

export default ResetPasswordScreen;