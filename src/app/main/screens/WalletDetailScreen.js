import React,{Component} from 'react'
import {View} from 'react-native'
import {WalletDetail} from '@pages'
import {Constants} from '@common'

class WalletDetailScreen extends Component{
    static navigationOptions = ({navigation}) => ({

    })
    render(){
        const {navigation} = this.props
        return (<WalletDetail
            goBack={()=>navigation.navigate(Constants.Screen.Account)}
            transfer={(index)=>navigation.navigate(Constants.Screen.WalletTransfer, {index:index})}
            deposit={()=>navigation.navigate(Constants.Screen.DepositFirst)}
        />)
    }
}

export default WalletDetailScreen