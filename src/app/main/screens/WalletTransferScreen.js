import React, {Component} from 'react'
import {WalletTransfer} from '@pages'
import {Constants} from '@common'
class WalletTransferScreen extends Component{
    static navigationOptions = ({navigation}) => ({

    })

    render(){
        const {navigation} = this.props
        return(
        <WalletTransfer
        navigation={navigation} 
        goBack={()=>navigation.navigate(Constants.Screen.Account)}/>)
    }
}

export default WalletTransferScreen