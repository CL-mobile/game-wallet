import React from 'react'
import {MyProfile} from '@pages'
import {NavButton,NavTitle,TabBarItem} from '@components'
import {Icons,Constants, Global} from '@common'

class MyProfileScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
    headerTitle: <NavTitle />,
    headerLeft: <NavButton icon={Icons.Drawer} style={{marginLeft:10}} onPress={()=>Global.EventEmitter.emit(Constants.EventEmitterName.OpenDrawer)}/>,

  })

  render(){
    const {navigation} = this.props
    return <MyProfile
            navigation={navigation}
            showWishList={()=>navigation.navigate(Constants.Screen.MyWishList)}
            showLanguages={()=>navigation.navigate(Constants.Screen.Languages)}
            showMyAddress={()=>navigation.navigate(Constants.Screen.MyAddress)}
            showFeedback={()=>navigation.navigate(Constants.Screen.Feedback)}
            showMyOrders={()=>navigation.navigate(Constants.Screen.MyOrders)}/>
  }
}

export default MyProfileScreen
