import React from 'react'
import {Deposit} from '@pages'
import {NavTitle,TabBarItem} from '@components'
import {Icons} from '@common'

class DepositScreen extends React.Component {
  render(){
    const {navigation} = this.props
    return <Deposit navigation={navigation} goBack={()=>navigation.goBack()}/>
  }
}

export default DepositScreen
