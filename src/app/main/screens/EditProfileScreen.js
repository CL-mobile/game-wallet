import React from 'react'
import {EditProfile} from '@pages'
import {NavTitle,TabBarItem} from '@components'
import {Icons} from '@common'

class EditProfileScreen extends React.Component {
  render(){
    const {navigation} = this.props
    return <EditProfile navigation={navigation} goBack={()=>navigation.goBack()}/>
  }
}

export default EditProfileScreen
