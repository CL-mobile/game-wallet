import React from 'react'
import {GameAll} from '@pages'
import {NavButton,NavTitle,TabBarItem} from '@components'
import {Icons,Constants} from '@common'

class GameAllScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
    
  })

  render(){
    const {navigation} = this.props
    return <GameAll navigation={navigation} goBack={()=>navigation.goBack()} 
      showGame={(data)=>navigation.navigate(Constants.Screen.Game,{data})}
    />
  }
}

export default GameAllScreen
