import React, {Component} from 'react'
import {View, Text} from 'react-native'
import {DepositSecondStep} from '@pages'
import {Icons,Constants} from '@common'

class SecondDeposit extends Component{

    render(){
        const {navigation} = this.props
        return(<DepositSecondStep
            navigation={navigation}
            goBack={()=>navigation.goBack()}
            nextStep={(data)=>navigation.navigate(Constants.Screen.DepositThird, {data})}
        />);
    }

}

export default SecondDeposit;