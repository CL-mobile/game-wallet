import React, {Component} from 'react'
import {View, Text} from 'react-native'
import {Icons,Constants} from '@common'

import {DepositThirdStep} from "@pages";

class ThirdDeposit extends Component{

    render(){
        const {navigation} = this.props
        
        return(<DepositThirdStep navigation={navigation} 
        goBack={()=> navigation.goBack()}
        nextStep={()=>navigation.navigate(Constants.Screen.DepositConfirmStep)}
        />);
    }

}

export default ThirdDeposit;