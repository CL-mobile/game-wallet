import React, {Component} from 'react'
import {View, Text} from 'react-native'
import {Icons,Constants} from '@common'

import {DepositFirstStep} from "@pages";

class FirstDeposit extends Component{

    render(){
        const {navigation} = this.props
        return(<DepositFirstStep
            navigation={navigation}
            goBack={()=>navigation.navigate(Constants.Screen.Account)}
            nextStep={(amount)=>navigation.navigate(Constants.Screen.DepositSecond, {amount})}
        />);
    }

}

export default FirstDeposit;