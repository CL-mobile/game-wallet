import React, {Component} from 'react'
import {View, Text} from 'react-native'
import {Icons,Constants} from '@common'

import {DepositConfirmStep} from "@pages";

class ConfirmStep extends Component{

    render(){
        const {navigation} = this.props
        return(<DepositConfirmStep
            navigation={navigation}
            goBack={()=>navigation.navigate(Constants.Screen.DepositThird)}
            
        />);
    }

}

export default ConfirmStep;