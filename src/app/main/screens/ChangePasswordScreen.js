import React from 'react'
import {ChangePassword} from '@pages'
import {NavTitle,TabBarItem} from '@components'
import {Icons} from '@common'

class ChangePasswordScreen extends React.Component {
  render(){
    const {navigation} = this.props
    return <ChangePassword navigation={navigation} goBack={()=>navigation.goBack()}/>
  }
}

export default ChangePasswordScreen
