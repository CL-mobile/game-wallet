import React from 'react'
import {GameProduct} from '@pages'
import {NavButton,NavTitle,TabBarItem} from '@components'
import {Icons} from '@common'

class GameProductScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
    
  })

  render(){
    const {navigation} = this.props
    return <GameProduct navigation={navigation} goBack={()=>navigation.goBack()} />
  }
}

export default GameProductScreen
