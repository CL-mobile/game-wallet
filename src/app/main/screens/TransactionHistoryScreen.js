import React from 'react'
import {TransactionHistory} from '@pages'
import {NavTitle,TabBarItem} from '@components'
import {Icons} from '@common'

class TransactionHistoryScreen extends React.Component {
  render(){
    const {navigation} = this.props
    return <TransactionHistory navigation={navigation} goBack={()=>navigation.goBack()}/>
  }
}

export default TransactionHistoryScreen