import React from 'react'
import {Account} from '@pages'
import {NavTitle,TabBarItem} from '@components'
import {Icons,Constants} from '@common'

class AccountScreen extends React.Component {
  render(){
    const {navigation} = this.props
    return <Account navigation={navigation} 
    goBack={()=>navigation.goBack()}
    showDeposit={()=>navigation.navigate(Constants.Screen.DepositFirst)}
    showTransaction={()=>navigation.navigate(Constants.Screen.Transaction)}
    showTransactionHistory={()=>navigation.navigate(Constants.Screen.TransactionHistory)}
    balanceDetail = {()=> navigation.navigate(Constants.Screen.WalletDetail)}
    transferPage = {()=> navigation.navigate(Constants.Screen.WalletTransfer)}
    accountSetting = {()=> navigation.navigate(Constants.Screen.AccountSetting)}
    />
  }
}

export default AccountScreen
