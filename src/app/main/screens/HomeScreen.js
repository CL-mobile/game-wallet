import React from 'react'
import {Home} from '@pages'
import {View} from 'react-native'
import {NavButton,NavTitle,TabBarItem} from '@components'
import {Icons,Constants,Global} from '@common'

class HomeScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
    title:'Your Account',
    headerLeft: <NavButton icon={Icons.Drawer} style={{marginLeft:10}} />,

  })

  render(){
    const {navigation} = this.props
      //return <View style={{flex:1}}></View>
    return <Home
            navigation={navigation}
            showGameProduct={(data)=>navigation.navigate(Constants.Screen.GameProduct,{data})}
            showGame={(data)=>navigation.navigate(Constants.Screen.Game,{data})}
            showGameAll={(data)=>navigation.navigate(Constants.Screen.GameAll,{data})}
            balanceDetail = {()=> navigation.navigate(Constants.Screen.WalletDetail)}
            />
  }

}

export default HomeScreen
