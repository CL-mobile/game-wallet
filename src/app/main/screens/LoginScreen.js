import React from 'react'
import {Login} from '@pages'
import {NavButton,NavTitle,TabBarItem} from '@components'
import {Icons,Constants} from '@common'

class LoginScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
    header:null,
  })

  render(){
    const {navigation} = this.props
    return <Login navigation={navigation} 
        showHome={()=>navigation.navigate(Constants.Screen.Home)}
        showRegister={()=>navigation.navigate(Constants.Screen.Register)}
        showForgot={()=>navigation.navigate(Constants.Screen.ForgotPass)}
    />
  }
}

export default LoginScreen;