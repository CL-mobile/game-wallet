import React from 'react'
import {AccountSetting} from '@pages'
import {NavTitle,TabBarItem} from '@components'
import {Icons,Constants} from '@common'

class AccountSettingScreen extends React.Component {
  render(){
    const {navigation} = this.props
    return <AccountSetting navigation={navigation} 
    goBack={()=>navigation.navigate(Constants.Screen.Account)}
    editProfile = {()=> navigation.navigate(Constants.Screen.EditProfile)}
    changePassword = {()=> navigation.navigate(Constants.Screen.ChangePassword)}
    />
  }
}

export default AccountSettingScreen
