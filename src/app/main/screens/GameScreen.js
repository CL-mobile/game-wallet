import React from 'react'
import {Game} from '@pages'
import {NavButton,NavTitle,TabBarItem} from '@components'
import {Icons} from '@common'

class GameScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
    
  })

  render(){
    const {navigation} = this.props
    return <Game navigation={navigation} goBack={()=>navigation.goBack()} />
  }
}

export default GameScreen
