import React from 'react'
import {ActiveCode} from '@pages'
import {NavButton,NavTitle,TabBarItem} from '@components'
import {Icons,Constants} from '@common'

class ActiveCodeScreen extends React.Component {
  static navigationOptions = ({navigation}) => ({
    header:null,
  })

  render(){
    const {navigation} = this.props
    return <ActiveCode navigation={navigation} 
        showHome={()=>navigation.navigate(Constants.Screen.Home)}
        showRegister={()=>navigation.navigate(Constants.Screen.Register)}
        showReset={(Vc)=>navigation.navigate(Constants.Screen.ResetPassword,Vc)}
    />
  }
}

export default ActiveCodeScreen;