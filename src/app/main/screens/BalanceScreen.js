import React from 'react'
import {Balance} from '@pages'
import {NavButton,NavTitle,TabBarItem} from '@components'
import {Icons} from '@common'

class BalanceScreen extends React.Component {

  render(){
    const {navigation} = this.props
    return <Balance navigation={navigation} goBack={()=>navigation.goBack()}/>
  }
}

export default BalanceScreen
