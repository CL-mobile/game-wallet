import React from 'react'
import {ForgotPass} from '@pages'
import {NavButton,NavTitle,TabBarItem} from '@components'
import {Icons,Constants} from '@common'

class ForgotPassScreen extends React.Component {

  render(){
    const {navigation} = this.props
    return <ForgotPass navigation={navigation} goBack={()=>navigation.goBack()}
      goToInputCode={(datas)=>navigation.navigate(Constants.Screen.ActiveCode,datas)}
    />
  }
}

export default ForgotPassScreen