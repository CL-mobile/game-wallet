import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'

export const changeprofile = (params,token)=>{
  console.log(token)
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.POST_CHANGE_PROFILE_PENDING})
    Services.changeprofile(params,token)
    .then((data)=>{
        
      dispatch({type:ActionTypes.POST_CHANGE_PROFILE_SUCCESS,message:data.message})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.POST_CHANGE_PROFILE_FAIL,message:errMsg})
    })
  }
}