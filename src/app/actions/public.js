import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'
import firebase from 'react-native-firebase';

export const requestAll = (token)=>{
  
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.GET_PROMOTIONS_SLIDERS_PENDING})
    Services.getPromotionSliders(token)
    .then((data)=>{
      dispatch({type:ActionTypes.GET_PROMOTIONS_SLIDERS_SUCCESS,dataBanner:data })
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.GET_PROMOTIONS_SLIDERS_FAIL,message:errMsg})
    })

    setTimeout(() => {
        dispatch({type:ActionTypes.GET_HOME_BALANCE_INFO_PENDING})
        Services.getHomeBalanceInfo(token)
        .then((data)=>{
        let dataprofile=data.data;
        dispatch({type:ActionTypes.GET_HOME_BALANCE_INFO_SUCCESS,dataBalance:data,Profile:dataprofile })
        })
        .catch((errMsg)=>{
        dispatch({type:ActionTypes.GET_HOME_BALANCE_INFO_FAIL,message:errMsg})
        })

        dispatch({type:ActionTypes.GET_MENU_PENDING})
        Services.getMenu(token)
        .then((data)=>{

            if(data.length != 0){
                let menu_list = data.menu_list.map((x, i) => {
                    x.active = false
                    return x
                })
                dispatch({type:ActionTypes.GET_MENU_SUCCESS,dataMenu:menu_list, dataPopular:data.popular_game})
            } 
        
        })
        .catch((errMsg)=>{
        dispatch({type:ActionTypes.GET_MENU_FAIL,message:errMsg})
        })
    }, 1000)
    
  }
}

export const getDataFirebase = (id)=>{
  
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.GET_FIREBASE_PENDING})
    firebase.database().goOnline();
    firebase.database().ref('users').child(id).on('value', (data) => {
      dispatch({type:ActionTypes.GET_FIREBASE_SUCCESS,dataFirebase:data.toJSON() })
    },(error) => {
      dispatch({type:ActionTypes.GET_FIREBASE_FAIL,message:error.message})
    })
    
  }

  
}