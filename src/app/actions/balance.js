import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'
import firebase from 'react-native-firebase';

export const getHomeBalanceInfo = (token)=>{

  return (dispatch,getState) => {
    dispatch({type:ActionTypes.GET_HOME_BALANCE_INFO_PENDING})
    Services.getHomeBalanceInfo(token)
    .then((data)=>{

      let dataprofile=data.data;
      let dataBalanceDetail = data.balance_type.map((x, i) => {
          
          x.total_balance = Object.values(data.balance)[i]

          //x.sub_menu = []
          return x
      })
      dispatch({type:ActionTypes.GET_HOME_BALANCE_INFO_SUCCESS,dataBalance:data,Profile:dataprofile, dataBalanceDetail:dataBalanceDetail })
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.GET_HOME_BALANCE_INFO_FAIL,message:errMsg})
    })
  }
}