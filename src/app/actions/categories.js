import * as ActionTypes from './ActionTypes'
import * as Services from '@services'

export const getCategories = ()=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.GET_CATEGORIES_PENDING})

    Services.getCategories()
    .then((categories)=>{
      dispatch({type:ActionTypes.GET_CATEGORIES_SUCCESS,categories})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.GET_CATEGORIES_FAIL,message:errMsg})
    })
  }
}
