import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'

export const getBank = (token)=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.GET_BANK_PENDING})
    Services.getBank(token)
    .then((data)=>{
        
        
      dispatch({type:ActionTypes.GET_BANK_SUCCESS,dataBank:data})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.GET_BANK_FAIL,message:errMsg})
    })
  }
}