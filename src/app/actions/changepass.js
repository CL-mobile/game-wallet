import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'

export const changepassword = (params,token)=>{
  console.log(token)
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.POST_CHANGE_PASS_PENDING})
    Services.changepassword(params,token)
    .then((data)=>{
      dispatch({type:ActionTypes.POST_CHANGE_PASS_SUCCESS,message:data})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.POST_CHANGE_PASS_FAIL,message:errMsg})
    })
  }
}