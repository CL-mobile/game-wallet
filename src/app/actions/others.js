import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'

export const setProductMessage = (isProductMessage)=>{
  return {type:ActionTypes.SET_PRODUCT_MESSAGE,isProductMessage}
}
