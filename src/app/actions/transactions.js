import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'

export const getRecentTransaction = (token)=>{

  return (dispatch,getState) => {
    dispatch({type:ActionTypes.GET_RECENT_TRANSACTION_PENDING})
    Services.getRecentTransaction(token)
    .then((data)=>{
      dispatch({type:ActionTypes.GET_RECENT_TRANSACTION_SUCCESS,dataRecentTransaction:data})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.GET_RECENT_TRANSACTION_FAIL,message:errMsg})
    })
  }
}