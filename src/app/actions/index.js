import * as AuthActions from './auth'
import * as CategoriesActions from './categories'
import * as ProductsActions from './products'
import * as CartsActions from './carts'
import * as SettingsActions from './settings'
import * as OthersActions from './others'
import * as MenusActions from './menus'
import * as BannerActions from './banner'
import * as BalanceInfoActions from './balance'
import * as BankActions from './bank'
import * as DepositActions from './deposit'
import * as PublicActions from './public'
import * as TransactionsActions from './transactions'
import * as TranferActions from './tranfer'
import * as ChangePassActions from './changepass'
import * as ChangeProfileActions from './changeprofile'
import * as GameActions from './game'

export const ActionCreators = Object.assign({},
  AuthActions,
  CategoriesActions,
  ProductsActions,
  CartsActions,
  SettingsActions,
  OthersActions,
  MenusActions,
  BannerActions,
  BalanceInfoActions,
  BankActions,
  DepositActions,
  PublicActions,
  TransactionsActions,
  TranferActions,
  ChangePassActions,
  ChangeProfileActions,
  GameActions
)
