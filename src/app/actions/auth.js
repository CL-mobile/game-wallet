import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'

export const signUpCustomer = (firstname,lastname,email,password)=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.SIGN_UP_PENDING})
    Services.signUp(firstname,lastname,email,password)
    .then((response)=>{
      dispatch({type:ActionTypes.SIGN_UP_SUCCESS})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.SIGN_UP_FAIL,message:errMsg})
    })
  }
}

export const signInCustomer = (email,password)=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.SIGN_IN_PENDING})
    Services.signIn(email,password)
    .then((token)=>{
      global.userToken = token
      dispatch({type:ActionTypes.SIGN_IN_SUCCESS,email,userToken:token})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.SIGN_IN_FAIL,message:errMsg})
    })
  }
}

export const getCustomerInfo = (userToken)=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.GET_CUSTOMER_INFO_PENDING})
    if (typeof userToken != 'undefined' && userToken != null && userToken.length > 0) {
      global.userToken = userToken
    }
    Services.getCustomerInfo()
    .then((customerInfo)=>{
      dispatch({type:ActionTypes.GET_CUSTOMER_INFO_SUCCESS,customerInfo})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.GET_CUSTOMER_INFO_FAIL,message:errMsg})
    })
  }
}

export const signOut = ()=>{
  return {type:ActionTypes.SIGN_OUT}
}

export const setMyAddress = (myAddress)=>{
  return {type:ActionTypes.SET_MY_ADDRESS,myAddress}
}

export const login = (email,password,tokenID)=>{
  
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.LOGIN_PENDING})
    Services.login(email,password,tokenID)
    .then((data)=>{
      dispatch({type:ActionTypes.LOGIN_SUCCESS,dataUser:data})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.LOGIN_FAIL,message:errMsg})
    })
  }
}
export const logout = ()=>{
  return {type:ActionTypes.LOGOUT}
}
export const forgotPass = (email)=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.FORGOTPASSWORD_PENDING})
    Services.forgotPass(email)
    .then((data)=>{
      dispatch({type:ActionTypes.FORGOTPASSWORD_SUCCESS,message:data})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.FORGOTPASSWORD_FAIL,message:errMsg})
    })
  }
}
export const inputVer = (email,verification_code)=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.INPUTCODE_PENDING})
    // console.log(code)
    Services.inputVer(email,verification_code)
    .then((data)=>{
      dispatch({type:ActionTypes.INPUTCODE_SUCCESS,data:data})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.INPUTCODE_FAIL,message:errMsg})
    })
  }
}
export const resetpassword = (userid,password,confirm)=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.RESETPASSWORD_PENDING})
    // console.log(code)
    console.log(userid+password+confirm)
    Services.resetpassword(userid,password,confirm)
    .then((data)=>{
      dispatch({type:ActionTypes.RESETPASSWORD_SUCCESS,message:data})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.RESETPASSWORD_FAIL,message:errMsg})
    })
  }
}