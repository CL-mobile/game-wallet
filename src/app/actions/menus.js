import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'

export const getMenu = (token)=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.GET_MENU_PENDING})
    Services.getMenu(token)
    .then((data)=>{
        let menu_list = data.menu_list.map((x, i) => {
            x.active = false
            //x.sub_menu = []
            return x
        })

      dispatch({type:ActionTypes.GET_MENU_SUCCESS,dataMenu:menu_list, dataPopular:data.popular_game})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.GET_MENU_FAIL,message:errMsg})
    })
  }
}

export const set_active_menu = (active, dataOld, item)=>{

    if(active == false){
        let newData = [...dataOld]

        newData.map((item, index) => {
            newData[index] = {...newData[index], active:false}
        })

        let newIndex = newData.findIndex(val => val.name === item.name)

        newData[newIndex] = {...newData[newIndex], active:true}

        return {type:ActionTypes.SET_ACTIVE_MENU,dataMenu:newData}
    }else{
        let newData = [...dataOld]

        newData.map((item, index) => {
            newData[index] = {...newData[index], active:false}
        })

        let newIndex = newData.findIndex(val => val.name === item.name)

        newData[newIndex] = {...newData[newIndex], active:false}
        return {type:ActionTypes.SET_ACTIVE_MENU,dataMenu:newData}
    }
    
    
  }