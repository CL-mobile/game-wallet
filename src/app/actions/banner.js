import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'

export const getPromotionSliders = (token)=>{

  return (dispatch,getState) => {
    dispatch({type:ActionTypes.GET_PROMOTIONS_SLIDERS_PENDING})
    Services.getPromotionSliders(token)
    .then((data)=>{
      dispatch({type:ActionTypes.GET_PROMOTIONS_SLIDERS_SUCCESS,dataBanner:data })
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.GET_PROMOTIONS_SLIDERS_FAIL,message:errMsg})
    })
  }
}

export const clearDataBanner = ()=>{
  return {type:ActionTypes.CLEAR_DATA_BANNER}
}