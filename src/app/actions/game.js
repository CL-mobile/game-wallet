import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'

export const getGame = (gameId, token)=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.GET_GAME_PENDING})
    Services.getGame(gameId, token)
    .then((data)=>{

      dispatch({type:ActionTypes.GET_GAME_SUCCESS,dataGame:data})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.GET_GAME_FAIL,message:errMsg})
    })
  }
}