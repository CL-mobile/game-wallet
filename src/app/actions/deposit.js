import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'

export const submitDeposit = (params,token)=>{
  
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.SUBMIT_DEPOSIT_PENDING})
    Services.submitDeposit(params,token)
    .then((data)=>{
      dispatch({type:ActionTypes.SUBMIT_DEPOSIT_SUCCESS,dataDeposit:data})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.SUBMIT_DEPOSIT_FAIL,message:errMsg})
    })
  }
}