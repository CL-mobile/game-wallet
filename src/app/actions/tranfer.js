import * as ActionTypes from './ActionTypes'
import * as Services from '@services'
import {AsyncStorage} from 'react-native'

export const postTranfer = (params,token)=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.POST_TRANFER_PENDING})
    Services.postTranfer(params,token)
    .then((message)=>{
      dispatch({type:ActionTypes.POST_TRANFER_SUCCESS,message:message})
    })
    .catch((errMsg)=>{
      dispatch({type:ActionTypes.POST_TRANFER_FAIL,message:errMsg})
    })
  }
}

export const setIsOpenModalTransfer = (isOpen)=>{
  return (dispatch,getState) => {
    dispatch({type:ActionTypes.SET_MODAL_TRANFER,isOpen:isOpen})
  }
}