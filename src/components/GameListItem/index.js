import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, Image, Linking } from 'react-native';
import { Container, Header, Content, Card, CardItem, Text, Body } from 'native-base';
import LinearGradient from 'react-native-linear-gradient'


class GameListItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  showGame = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (!supported) {
        console.log('Can\'t handle url: ' + url);
      } else {
        return Linking.openURL(url);
      }
    }).catch(err => console.error('An error occurred', err));
  }

  render() {
    let {item} = this.props
    return (
        <TouchableOpacity onPress={() => this.showGame(item.link_url)} style={{flex:1, alignItems:'center', justifyContent: 'center', borderRadius:5, marginRight:10}}>
            <Image
            source={{uri:item.cover_image}}
            style={{height:170, width:130, borderRadius:5}}
            resizeMode='cover'
            />
            <View style={{height:170,width:130, borderRadius:6, position: 'absolute',left: 0, top: 0 }} >
            <LinearGradient colors={[ 'transparent','rgba(0,0,0,0.5)', 'rgba(0,0,0,.9)' ]} style={{height:170, width:130, borderRadius:5, justifyContent: 'flex-end'}}>
                <Text style={{fontSize:16, fontWeight:'400',padding:10, color:'white'}} >{item.name}</Text>
            </LinearGradient>
            </View>
        </TouchableOpacity>
    );
  }
}

export default GameListItem;
