import React, {Component} from 'react'
import {View, Text} from 'react-native'
import { Container, Header, Item, Input, Icon, Button} from 'native-base';
import styles from './style'

export default class SearchHeaders extends Component{

    constructor(props){
        super(props)
    }

    render() {
        return (
            <View style={styles.containerSearch}>
                <Text style={styles.textHeader}> Search </Text>
                <Header searchBar rounded transparent style={{paddingTop:0, height:40 }}>
                    <Item style={{margin:0 }}>
                        <Icon name="ios-search" style={{tintColor:'rgba(0,0,0,.6)'}} />
                        <Input placeholder="Search" style={styles.searchInput}/>
                    </Item>
                    <Button transparent>
                        <Text>Search</Text>
                    </Button>
                </Header>
            </View>
        );
    }

}