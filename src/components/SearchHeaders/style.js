import {StyleSheet} from 'react-native'
import {robotoWeights, iOSColors} from 'react-native-typography'

export default StyleSheet.create({
    containerSearch:{
       paddingTop:  20,

    },
    textHeader:{
        ...robotoWeights.bold,
        color:iOSColors.lightGray,
        fontSize:26,
        marginHorizontal: 10,
        marginBottom:5
    },
    searchInput:{
        ...robotoWeights.light,
        backgroundColor:'rgba(255,255,255,.4)',
        fontSize: 14
    }
})