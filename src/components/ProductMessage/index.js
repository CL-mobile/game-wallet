import React, { Component } from 'react';
import { View, TouchableOpacity, ScrollView, Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Text, Body } from 'native-base';

import {GameListItem} from '@components'

import {Config,Constants, Icons, Colors} from '@common'

import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'

const dataPopular = [
  {
    title:"Fortnite",
    image:"https://is2-ssl.mzstatic.com/image/thumb/Purple128/v4/dd/24/12/dd2412d9-02ca-7658-48e8-258c63aed123/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/246x0w.jpg"
  },
  {
    title:"Free Fire",
    image:"https://cdn-www.bluestacks.com/bs-images/YRT3BaR3BNhNv-ZVGQ7EFP-hXvp8Z7KXZdPtR-OlBzOfvsBgcT_XS9JSkddIceI1DMc.png"
  },
  {
    title:"PUBG",
    image:"https://lh3.googleusercontent.com/0Uv9JJj6QBz425dwlASC944eiNKelUWl3qpIf8faV-Cuq2vuKeLVi5XZR-l_sPSh2xAh"
  },
  {
    title:"ROS",
    image:"https://rulesofsurvival.gcube.id/wp-content/uploads/sites/8/2018/02/gcube-rules-of-survival-icon.png"
  },
  
]

class ProductMessage extends Component {

  showHideComponent = () => {
    if(this.props.isProductMessage == false){
      this.props.setProductMessage(true)
      
    }else{
      this.props.setProductMessage(false)
      
    }
  }
  
  render() {
    let {title, isProductMessage, subCategory} = this.props
    
    return (
        <View>

          <View style={{marginTop:10, marginHorizontal:2}}>
            <View style={{flexDirection:'row', justifyContent:'space-between', alignItems:'center'}}>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                <Text style={{fontWeight:'500', color:'black', fontFamily:Constants.FontFamily, marginBottom:5}}>{title}</Text>
              </View>

            </View>


            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              {
                subCategory.map(val =>
                <TouchableOpacity onPress={() => this.showGameProduct(val, subCategory) }>
                  <Card style={{flex:1, alignItems:'center', borderRadius:5, marginRight:10}}>
                      <Text style={{fontSize:16, fontWeight:'500', color:'black', padding:20}}>{val.sub_name}</Text>
                  </Card>
                </TouchableOpacity>
                )
              }
            </ScrollView>

          </View>

        </View>
    );
  }

  showGameProduct = (val, subCategory) => {
    
    let data = {
      gameProduct:val,
      subCategory:subCategory
    }
    if(__DEV__)
    console.log(data);
    
    this.props.showGameProduct(data)
    
  }
}

function mapStateToProps({othersReducers}){
  return {
    isProductMessage:othersReducers.isProductMessage
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators(ActionCreators,dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(ProductMessage)
