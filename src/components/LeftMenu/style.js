import {StyleSheet} from 'react-native'
import {Colors,Constants} from '@common'

export default StyleSheet.create({
  container:{
    flex:1,
  },
  icon:{
    width:150,
    height:100,
    marginTop:25,
    resizeMode:'contain',
    tintColor:'white'
  },
  topView:{
    height:120,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:Colors.LeftMenu,


  },
  btmView:{
    backgroundColor:'white',
    flex:1,
    opacity:0.8
  },
  separator:{
    height:0,
    backgroundColor:'white'
  }
})
