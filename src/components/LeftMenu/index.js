import React from 'react'
import {
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  Text
} from 'react-native'
import styles from './style'
import {Icons,Constants,Colors} from '@common'

import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'

import {LeftMenuItem} from '@components'
class LeftMenu extends React.Component {
  render(){
    let {categories,onOpenPage} = this.props
    return (
      <View style={styles.container}>
        <View style={styles.topView} >
          <Image source={Icons.Logo} style={styles.icon}/>
        </View>

        <View style={{backgroundColor:'#f3f3f3'}}>
        <Text style={{marginLeft:10, marginTop:10, marginBottom:10, color:'#999999', fontWeight: 'bold'}}> CATEGORIES </Text>
        </View>

        <View style={styles.btmView} >
          <FlatList
            contentContainerStyle={styles.list}
            keyExtractor={(item,index)=>`${index}`}
            data={categories}
            renderItem={({item,index})=><LeftMenuItem item={item} onPress={onOpenPage}/>}
            ItemSeparatorComponent={()=><View style={styles.separator}/>}
          />
        </View>
      </View>
    )
  }
}

LeftMenu.defaultProps = {
  categories:[],
}

function mapStateToProps({categoriesReducers}){
  return {
    categories:categoriesReducers.categories,
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators(ActionCreators,dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(LeftMenu)
