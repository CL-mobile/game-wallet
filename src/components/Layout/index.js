import React from 'react'

import {
    View,
    Dimensions,
    Image,
    SafeAreaView,
    StatusBar,
    TouchableOpacity,
    FlatList,
    Button,
    ScrollView,
    LayoutAnimation,
    ActivityIndicator
} from 'react-native'

import LinearGradient from 'react-native-linear-gradient'
import styles from "./style";
import {SharedElement, TranslateYAndOpacity} from 'react-native-motion'


class Layout extends React.Component{
    constructor(props){
        super(props);
    }

    render(){
        const {animated} = this.props;
        if(animated){
            return this.renderAnimation()
        }else{
            return this.staticUI()
        }
    }

    renderAnimation(){
        const {children, id, sourceId} = this.props;
        return (
            <SafeAreaView style={styles.container}>
                <LinearGradient colors={['#130b44','#2b188e']} style={{flex:1}}>
                    <View style={{flex:1,backgroundColor:'rgba(0,0,0,.7)', paddingTop: StatusBar.currentHeight}}>
                        <StatusBar
                            translucent={true}
                            backgroundColor={'transparent'}
                        />
                        {children}
                    </View>

                </LinearGradient>
            </SafeAreaView>)
    }

    staticUI(){
        const {children, id, sourceId} = this.props;
        return (<SafeAreaView style={styles.container}>
                <LinearGradient colors={['#130b44','#2b188e']} style={{flex:1}}>
                    <View style={{flex:1,backgroundColor:'rgba(0,0,0,.7)', paddingTop: StatusBar.currentHeight}}>
                        <StatusBar
                            translucent={true}
                            backgroundColor={'transparent'}
                        />
                        {children}
                    </View>

                </LinearGradient>
            </SafeAreaView> )
    }

}

Layout.defaultProps = {
    animated : false
}

export default Layout;