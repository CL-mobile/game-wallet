import React from 'react'
import {
  View,
  Image,
  TouchableOpacity,
    Dimensions
} from 'react-native'
import styles from './style'
import {Icons,Constants} from '@common'

import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'
import {iOSColors} from 'react-native-typography'

import {Text} from '@components'

class TabBarItem extends React.Component {
  render(){
    let {icon,tintColor,routeName,carts,index,jumpToIndex} = this.props
      const width = (Dimensions.get('window').width/3)-2;
    return (
      <View>
        <TouchableOpacity activeOpacity={.5} onPress={()=>{
            jumpToIndex(index);
        }}  style={{width:width, alignItems: 'center', padding:10 }} >
          <Image source={icon} style={[styles.icon,{tintColor}]}/>
        </TouchableOpacity>
      </View>
    )
  }
}

TabBarItem.defaultProps = {
  carts:[]
}

function mapStateToProps({cartsReducers}){
  return {
    carts:cartsReducers.carts,
    reload:cartsReducers.reload
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators(ActionCreators,dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(TabBarItem)
