import React, {Component} from 'react'
import {Image, TouchableOpacity} from 'react-native'
import {Icons} from'@common'

class NavBack extends Component{

    constructor(props){
        super(props)
    }

    render(){
        const {onPress, icon} = this.props
        return(
            <TouchableOpacity onPress={onPress} style={{paddingVertical:10}}>
                <Image
                    source={icon}
                    style={{height:18, width:18, tintColor:'white'}}
                />
            </TouchableOpacity>
        )
    }
}

export default NavBack

NavBack.defaultProps = {
    icon: Icons.ArrowLeft
}