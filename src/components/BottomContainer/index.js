import React from 'react'
import {
    View,
    Image,
    TouchableOpacity
} from 'react-native'

import LinearGradient from "react-native-linear-gradient";
import {SafeAreaView} from 'react-navigation';
import {Icons} from '@common'
import {iOSColors} from "react-native-typography";
import {TabBarItem} from '@components'

class BottomContainer extends React.Component {
    render(){
        const {
            navigation,
            jumpToIndex,
        } = this.props;
        const {
            routes, routerName
        } = navigation.state;
        return (
            <SafeAreaView style={{zIndex: 10}}>
                <LinearGradient colors={['#2b188e','#130b44','rgba(0,0,0,.9)']} style={{height: 44}}>
                    <View style={{backgroundColor:'rgba(0,0,0,.7)', height:44,shadowColor: iOSColors.blue,
                        shadowOffset: {
                            width: 3,
                            height: -500,
                        },
                        shadowOpacity: 1,
                        shadowRadius: 7.16,
                        elevation: 20,flexDirection: 'row',
                        justifyContent: 'space-around',
                        alignItems: 'center',
                        alignContent: 'center',}}>
                    {routes && routes.map((route, index) => {
                        const focused = index === navigation.state.index;
                        const tabKey = route.key;
                        let icon = null
                        switch (index) {
                            case 0:
                                icon = Icons.Home2
                                break;
                            case 1:
                                icon = Icons.Search
                                break;
                            case 2:
                                icon = Icons.Account
                                break;
                            default:
                                return null
                        }
                        return <TabBarItem icon={icon} key={index} tintColor={focused? iOSColors.tealBlue:iOSColors.white} routeName={route.routeName} jumpToIndex={jumpToIndex} index={index} />;
                    })}
                    </View>
                </LinearGradient>
            </SafeAreaView>
        )
    }
}

export default BottomContainer

