import {StyleSheet} from 'react-native'

export default StyleSheet.create({
  container:{
    backgroundColor:'#cccccc',
    height:200,
    alignItems:'center',
    justifyContent:'center'
  },
  background:{
    flex:1,
    width:null,
    height:null
  },
  absolute:{
    position:'absolute',
    top:0,
    left:0,
    right:0,
    bottom:0,
  },
  avatar:{
    width:100,
    height:100,
    borderRadius:50,
    borderWidth:0.5,
    borderColor:'white'
  },
  name:{
    fontSize:16,
    color:'white',
    textAlign:'center',
    fontWeight:'bold',
    marginTop:10
  }
})
