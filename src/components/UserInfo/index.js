import React from 'react'
import {
  View,
  Image,
  TouchableOpacity,
  ImageBackground,
  AsyncStorage,
  Platform,
  findNodeHandle
} from 'react-native'
import styles from './style'
import {Icons} from '@common'
import { BlurView, VibrancyView } from 'react-native-blur';

import {connect} from 'react-redux'
import {ActionCreators} from '@actions'
import {bindActionCreators} from 'redux'
import * as ActionTypes from '@actions/ActionTypes'

import {Text} from '@components'

class UserInfo extends React.Component {
  state = { viewRef: null }
  imageLoaded() {
    this.setState({ viewRef: findNodeHandle(this.backgroundImage) });
  }
  render(){
    let avatar = "https://media-mmdb.nationalgeographic.com/static-media/images/css_images/nationalGeographic_default_avatar.jpg"
    return (
      <View style={styles.container}>
        <Image source={{uri:avatar}} style={styles.absolute} ref={(img) => { this.backgroundImage = img; }} onLoadEnd={this.imageLoaded.bind(this)} />

        <BlurView
          style={styles.absolute}
          viewRef={this.state.viewRef}
          blurType="dark"
          blurAmount={10}
        />

        <Image source={{uri:avatar}} style={styles.avatar}/>
        <Text style={styles.name}>{this.props.email}</Text>

      </View>
    )
  }

}

function mapStateToProps({authReducers}){
  return {
    email:authReducers.email,
    message:authReducers.message,
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators(ActionCreators,dispatch)
}

export default connect(mapStateToProps,mapDispatchToProps)(UserInfo)
