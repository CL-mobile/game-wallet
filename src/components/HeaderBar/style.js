import {StyleSheet, Dimensions} from 'react-native'
import {Config,Constants, Icons, Colors} from '@common'
import {robotoWeights, iOSColors} from 'react-native-typography'

export default StyleSheet.create({
    container: {
        flex: 1
    },
    headerContainer:{
        flexDirection:'row', justifyContent:'space-between', paddingHorizontal:16, backgroundColor:'transparent' , alignItems:'center'
    },
    headerMenu:{
        height:25, width:25, tintColor:'white'
    },
    headerTitle:{
        ...robotoWeights.condensed,
        borderWidth:.5,
        borderRadius:50,
        paddingHorizontal:8,
        paddingVertical:3,
        borderColor:iOSColors.lightGray,
        fontSize:8,
        color:iOSColors.lightGray,
        alignSelf: 'center'
    },
    headerNotif:{
        height:25, width:21
    },
    subHeaderText:{
        ...robotoWeights.light,
        fontSize:15,
        marginTop:5,
        color:'#FFF',
        alignSelf: 'center'
    },
    subHeaderText1:{
        ...robotoWeights.light,
        fontSize:15,
        marginTop:5,
        color:'#FFF'
    }
})