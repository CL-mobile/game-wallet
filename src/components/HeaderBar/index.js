import React from 'react'

import {
    View
} from 'react-native'

import LinearGradient from 'react-native-linear-gradient'
import styles from "./style";
import {Config,Constants, Icons, Colors} from '@common'

class HeaderBar extends React.Component{

    render(){
        const { left, center, right} = this.props;
        return(<View style={styles.headerContainer}>
                {left}
                {center}
                {right}
            </View>)
    }

}

export default HeaderBar