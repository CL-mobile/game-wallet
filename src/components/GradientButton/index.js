import React from 'react'
import {
    View,
    Image,
    TouchableOpacity,
    TextInput,
    ActivityIndicator
} from 'react-native'
import styles from './style'
import {Colors} from '@common'
import {Text} from '@components'
import LinearGradient from "react-native-linear-gradient";

class GradientButton extends React.Component {
    static defaultProps = {
        disabled:false,
        loading:false
    }
    render(){
        let {title,loading, disabled,onPress,style,colors} = this.props

        return (
            <TouchableOpacity  disabled={loading || disabled} onPress={onPress} activeOpacity={0.75}>
                <LinearGradient colors={colors} start={{ x: 0, y: -1}} end={{ x: 1, y: 3 }}   style={[styles.linearGradient,style]}>
                    {!loading && <Text style={styles.buttonText}>{title}</Text>}
                    {loading && <ActivityIndicator color="white"/>}
                </LinearGradient>
            </TouchableOpacity>
        )
    }
}

export default GradientButton

GradientButton.defaultProps = {
    colors:['#0A7F6D','#027599', '#096991', '#0F4C82']
}
