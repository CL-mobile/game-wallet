import {Dimensions, StyleSheet} from 'react-native'
import {Colors} from '@common'
import {iOSColors, robotoWeights} from "react-native-typography";

export default StyleSheet.create({
    button:{
        flexDirection:'row',
        flex:1,
        height:50,
        borderRadius:50,
        backgroundColor:Colors.AppColor,
        alignItems:'center',
        justifyContent:'center'
    },
    title:{
        fontSize:16,
        color:'white',
        alignSelf: 'center',
        alignItems: 'center'
    },
    linearGradient: {
        width:Dimensions.get('window').width-60,
        alignSelf: 'center',
        alignItems:'center',
        justifyContent: 'center',
        paddingLeft: 15,
        paddingRight: 15,
        height:45,
        paddingVertical:20,
        borderRadius: 50,
        shadowColor: iOSColors.blue,
        shadowOffset: {
            width: 3,
            height: -10,
        },
        shadowOpacity: 1,
        shadowRadius: 7.16,
        elevation: 2,
    },
    buttonText: {
        ...robotoWeights.light,
        fontSize: 16,
        textAlign: 'center',
        color: iOSColors.lightGray2 ,
        backgroundColor: 'transparent',
    },
})
